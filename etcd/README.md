# etcd

```yaml
all:
  hosts:
    stage01:
      ansible_host: 10.10.10.1
    stage02:
      ansible_host: 10.10.10.1
    stage03:
      ansible_host: 10.10.10.1

etcd:
  hosts:
    stage01:
      etcd_cookbook_prefix: etcd01
      etcd_env:
        ETCD_INITIAL_CLUSTER_STATE: existing
      etcd_docker_networks:
        - name: etcd-net
          ipv4_address: 172.18.0.2
    stage02:
      etcd_cookbook_prefix: etcd02
      etcd_docker_networks:
        - name: etcd-net
          ipv4_address: 172.18.0.3
    stage03:
      etcd_cookbook_prefix: etcd03
      etcd_docker_networks:
        - name: etcd-net
          ipv4_address: 172.18.0.4
  vars:
    etcd_docker_etc_hosts:
      stage01: 172.18.0.2
      stage02: 172.18.0.3
      stage03: 172.18.0.4
```
