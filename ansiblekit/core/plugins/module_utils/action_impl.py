from __future__ import annotations

from typing import Callable
from typing import Dict
from typing import Optional

import ansible.plugins.action  # type: ignore

from .action_base import Action, LinuxUtils, WindowsUtils
from .action_base import ActionResult
from .action_base import Executor
from .action_base import ExecutorException
from .action_base import ExecutorExceptions
from .action_base import StatResult
from .errors import ExpectedException
from .errors import UnexpectedException
from .unverified_dict import UnverifiedDict

class AnsibleAction(ansible.plugins.action.ActionBase):
    def run(self, tmp: Optional[object] = None, task_vars: Optional[object] = None) -> ActionResult:
        super(AnsibleAction, self).run(tmp, task_vars) # type: ignore

        try:
            args = UnverifiedDict(self._task.args) # type: ignore
            vars = UnverifiedDict(task_vars)
            executor = ExecutorImpl(self, tmp, task_vars)

            if (self._connection.transport == "winrm"): # type: ignore
                action = self.create_windows_action(args, vars, WindowsUtilsImpl(executor))
            else:
                action = self.create_linux_action(args, vars, LinuxUtilsImpl(executor))

            args.assert_empty()

            return action.run()
        except ExecutorException as e:
            return e.context
        except ExpectedException as e:
            return ActionResult.failed(e)

    def create_linux_action(self, args: UnverifiedDict, vars: UnverifiedDict, utils: LinuxUtilsImpl) -> Action:
        raise UnexpectedException("Unsupported action")

    def create_windows_action(self, args: UnverifiedDict, vars: UnverifiedDict, utils: WindowsUtilsImpl) -> Action:
        raise UnexpectedException("Unsupported action")

DEFAULT_TEMPLATE_OPTIONS: Dict[str, object] = {
    "lstrip_blocks": True,
    "trim_blocks": True
}

class ExecutorImpl(Executor):
    action: AnsibleAction
    tmp: Optional[object]
    task_vars: Optional[object]

    def __init__(self, action: AnsibleAction, tmp: Optional[object], task_vars: Optional[object]) -> None:
        self.action = action
        self.tmp = tmp
        self.task_vars = task_vars

    def execute_module(self, name: str, args: Dict[str, object]) -> ActionResult:
        execute_fn: Callable[..., Dict[str, object]] = self.action._execute_module # type: ignore

        result = ActionResult(
            execute_fn(
                module_name=name,
                module_args=args,
                tmp=self.tmp,
                task_vars=self.task_vars
            )
        )

        return self.process_result(result)

    def execute_action(self, name: str, args: Dict[str, object]) -> ActionResult:
        ds: object = {
            "action": name,
            "args": args
        }
        variable_manager: object = self.action._task.get_variable_manager() # type: ignore
        loader: object = self.action._task.get_loader() # type: ignore
        task: object = self.action._task.copy().load_data(ds=ds, variable_manager=variable_manager, loader=loader) # type: ignore
        connection: object = self.action._connection # type: ignore
        play_context: object = self.action._play_context # type: ignore
        templar: object = self.action._templar # type: ignore
        shared_loader_obj: object = self.action._shared_loader_obj # type: ignore

        create_action_fn: Callable[..., AnsibleAction] = self.action._shared_loader_obj.action_loader.get # type: ignore

        action = create_action_fn(
            name=name,
            task=task,
            connection=connection,
            play_context=play_context,
            loader=loader,
            templar=templar,
            shared_loader_obj=shared_loader_obj
        )

        result = ActionResult(
            action.run(self.tmp, self.task_vars)
        )

        return self.process_result(result)

    def process_result(self, result: ActionResult) -> ActionResult:
        if (result.get("failed", False) == False):
            return result
        else:
            raise ExecutorException(result)

    def fetch(self, src: str, dest: str) -> ActionResult:
        return self.execute_action(
            name="ansible.builtin.fetch",
            args={
                "src": src,
                "dest": dest,
                "flat": True
            }
        )

    def stat(self, path: str) -> StatResult:
        result = self.execute_module(
            name="ansible.builtin.stat",
            args={
                "path": path
            }
        )

        stat = UnverifiedDict(
            UnverifiedDict(result).get_dict("stat")
        )

        if (stat.get_bool("exists") == False):
            raise ExecutorExceptions.FileNotFoundException(result)

        return StatResult(result, stat)

    def create_windows_utils(self) -> WindowsUtils:
        return WindowsUtilsImpl(self)

    def create_linux_utils(self) -> LinuxUtils:
        return LinuxUtilsImpl(self)

class WindowsUtilsImpl(WindowsUtils):
    executor: ExecutorImpl

    def __init__(self, executor: ExecutorImpl) -> None:
        self.executor = executor

class LinuxUtilsImpl(LinuxUtils):
    executor: ExecutorImpl

    def __init__(self, executor: ExecutorImpl) -> None:
        self.executor = executor

    def mkdir(self, path: str, owner: str, group: str, mode: str) -> ActionResult:
        return self.executor.execute_module(
            name="ansible.builtin.file",
            args={
                "path": path,
                "state": "directory",
                "owner": owner,
                "group": group,
                "mode": mode
            }
        )

    def copy(self, src: str, dest: str, owner: str, group: str, mode: str) -> ActionResult:
        return self.executor.execute_action(
            name="ansible.builtin.copy",
            args={
                "src": src,
                "dest": dest,
                "owner": owner,
                "group": group,
                "mode": mode
            }
        )

    def template(self, src: str, dest: str, owner: str, group: str, mode: str) -> ActionResult:
        return self.executor.execute_action(
            name="ansible.builtin.template",
            args={
                **DEFAULT_TEMPLATE_OPTIONS,
                **{
                    "src": src,
                    "dest": dest,
                    "owner": owner,
                    "group": group,
                    "mode": mode
                }
            }
        )

    def stat(self, path: str) -> StatResult:
        return self.executor.stat(path)

    def fetch(self, src: str, dest: str) -> ActionResult:
        try:
            return self.executor.fetch(src, dest)
        except ExecutorException as e:
            error_msg = "file not found: " + src
            if (e.context.msg() == error_msg):
                raise ExecutorExceptions.FileNotFoundException(e.context)
            else:
                raise e
