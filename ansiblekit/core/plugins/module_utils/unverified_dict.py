from __future__ import annotations

from typing import Callable
from typing import Dict
from typing import List
from typing import Optional
from typing import cast

from .errors import ExpectedException

class UnverifiedDict:
    class KeyNotFound(ExpectedException):
        pass

    origin: Dict[str, object]

    def __init__(self, origin: Optional[object], allow_none: bool = False):
        if (origin is None):
            if allow_none:
                self.origin = {}
            else:
                raise ExpectedException("None instead of dict")
        elif isinstance(origin, dict):
            self.origin = origin.copy()
        else:
            raise ExpectedException("[%s] instead of dict" % type(origin).__name__)

    def pop(self, key: str, default: Optional[Callable[[], object]] = None) -> object:
        value = self.origin.pop(key, None)

        if (value is not None):
            return value

        if (default is not None):
            return default()

        raise UnverifiedDict.KeyNotFound("[%s] not found" % key)

    def get(self, key: str, default: Optional[Callable[[], object]] = None) -> object:
        value = self.origin.get(key, None)

        if (value is not None):
            return value

        if (default is not None):
            return default()

        raise UnverifiedDict.KeyNotFound("[%s] not found" % key)

    def pop_dict(self, key: str, default: Optional[Callable[[], Dict[str, object]]] = None) -> Dict[str, object]:
        value = self.pop(key, default)

        if isinstance(value, dict):
            return cast(Dict[str, object], value)

        raise ExpectedException.wrap(
            "[%s]" % key,
            ExpectedException("[%s] instead of dict" % type(value).__name__)
        )

    def get_dict(self, key: str, default: Optional[Callable[[], Dict[str, object]]] = None) -> Dict[str, object]:
        value = self.get(key, default)

        if isinstance(value, dict):
            return cast(Dict[str, object], value)

        raise ExpectedException.wrap(
            "[%s]" % key,
            ExpectedException("[%s] instead of dict" % type(value).__name__)
        )

    def pop_list(self, key: str, default: Optional[Callable[[], List[object]]] = None) -> List[object]:
        value = self.pop(key, default)

        if isinstance(value, list):
            return cast(List[object], value)

        raise ExpectedException.wrap(
            "[%s]" % key,
            ExpectedException("[%s] instead of list" % type(value).__name__)
        )

    def pop_text(self, key: str, default: Optional[Callable[[], str]] = None) -> str:
        value = self.pop(key, default)

        if isinstance(value, str):
            return value

        raise ExpectedException.wrap(
            "[%s]" % key,
            ExpectedException("[%s] instead of text" % type(value).__name__)
        )

    def get_text(self, key: str, default: Optional[Callable[[], str]] = None) -> str:
        value = self.get(key, default)

        if isinstance(value, str):
            return value

        raise ExpectedException.wrap(
            "[%s]" % key,
            ExpectedException("[%s] instead of text" % type(value).__name__)
        )

    def get_bool(self, key: str, default: Optional[Callable[[], bool]] = None) -> bool:
        value = self.get(key, default)

        if isinstance(value, bool):
            return value

        raise ExpectedException.wrap(
            "[%s]" % key,
            ExpectedException("[%s] instead of bool" % type(value).__name__)
        )

    def assert_empty(self) -> None:
        if (len(self.origin) > 0):
            raise ExpectedException("Unsupported keys %s" % set(self.origin.keys()))
