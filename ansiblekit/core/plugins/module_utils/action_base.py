from __future__ import annotations

from typing import Dict

from .errors import ExpectedException
from .unverified_dict import UnverifiedDict

class ActionResult(Dict[str, object]):
    @staticmethod
    def failed(e: ExpectedException) -> ActionResult:
        return ActionResult({
            "failed": True,
            "msg": str(e)
        })

    @staticmethod
    def success(changed: bool, ansible_facts: Dict[str, object] = {}) -> ActionResult:
        return ActionResult({
            "changed": changed,
            "ansible_facts": ansible_facts
        })

    def __init__(self, origin: Dict[str, object]) -> None:
        super().__init__(origin)

    def changed(self) -> bool:
        return (self.get("changed", False) == True)

    def msg(self) -> object:
        return self.get("msg")

class Action:
    def run(self) -> ActionResult: ...

class ExecutorException(ExpectedException):
    context: ActionResult

    def __init__(self, context: ActionResult) -> None:
        super().__init__("Execute failed")

        self.context = ActionResult({
            **context,
            **{"failed": True}
        })

class ExecutorExceptions:
    class FileNotFoundException(ExecutorException):
        pass

class StatResult(ActionResult):
    origin: ActionResult
    stat: UnverifiedDict

    def __init__(self, origin: ActionResult, stat: UnverifiedDict) -> None:
        super(ActionResult, self).__init__(origin)

        self.stat = stat

class Executor:
    def execute_module(self, name: str, args: Dict[str, object]) -> ActionResult: ...
    def execute_action(self, name: str, args: Dict[str, object]) -> ActionResult: ...
    def fetch(self, src: str, dest: str) -> ActionResult: ...
    def stat(self, path: str) -> StatResult: ...
    def create_windows_utils(self) -> WindowsUtils: ...
    def create_linux_utils(self) -> LinuxUtils: ...

class WindowsUtils:
    pass

class LinuxUtils:
    def mkdir(self, path: str, owner: str, group: str, mode: str) -> ActionResult: ...
    def copy(self, src: str, dest: str, owner: str, group: str, mode: str) -> ActionResult: ...
    def template(self, src: str, dest: str, owner: str, group: str, mode: str) -> ActionResult: ...
    def stat(self, path: str) -> StatResult: ...
    def fetch(self, src: str, dest: str) -> ActionResult: ...
