from __future__ import annotations

class ExpectedException(Exception):
    @staticmethod
    def wrap(text: str, origin: Exception) -> ExpectedException:
        return ExpectedException("%s:\n  %s" % (text, str(origin).replace("\n", "\n  ")))

    def __init__(self, text: str) -> None:
        super().__init__(text)

class UnexpectedException(Exception):
    pass
