from ....core.plugins.module_utils.action_base import ActionResult
from ....core.plugins.module_utils.action_base import LinuxUtils
from ....core.plugins.module_utils.action_base import WindowsUtils

class OSFamily:
    def pull(self, name: str, path: str) -> ActionResult: ...
    def push(self, name: str, path: str) -> ActionResult: ...

class Windows(OSFamily):
    def __init__(self, utils: WindowsUtils) -> None:
        pass

class Linux(OSFamily):
    utils: LinuxUtils

    def __init__(self, utils: LinuxUtils) -> None:
        self.utils = utils

    def pull(self, name: str, path: str) -> ActionResult:
        return self.utils.fetch(
            src="/var/lib/cookbook/" + name + ".json",
            dest=path
        )

    def push(self, name: str, path: str) -> ActionResult:
        self.utils.mkdir(
            path="/var/lib/cookbook/",
            owner="root",
            group="root",
            mode="0755"
        )

        return self.utils.copy(
            src=path,
            dest="/var/lib/cookbook/" + name + ".json",
            owner="root",
            group="root",
            mode="0600"
        )
