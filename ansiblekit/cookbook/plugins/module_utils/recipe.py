from __future__ import annotations

import json
import os

from typing import Dict
from typing import cast

from ....cookbook.plugins.module_utils.cookbook import Cookbook
from ....cookbook.plugins.module_utils.osfamily import OSFamily
from ....core.plugins.module_utils.action_base import ActionResult
from ....core.plugins.module_utils.errors import ExpectedException
from ....core.plugins.module_utils.unverified_dict import UnverifiedDict

class Recipe:
    class NotOpenedException(ExpectedException):
        def __init__(self) -> None:
            super().__init__("Recipe not opened")

    @staticmethod
    def open(cookbook: Cookbook, vars: UnverifiedDict) -> Recipe:
        try:
            name = vars.get_text("_ansiblekit_recipe")
        except ExpectedException:
            raise Recipe.NotOpenedException()

        return Recipe(
            cookbook=cookbook,
            name=name
        )

    @staticmethod
    def pull(osfamily: OSFamily, cookbook: Cookbook, name: str) -> Recipe:
        recipe = Recipe(cookbook, name)

        try:
            os.makedirs(recipe.dir_path())
        except FileExistsError:
            raise ExpectedException("Not use the same recipe twice [%s]" % recipe.name)

        osfamily.pull(recipe.name, recipe.previous_path())
        recipe.write(recipe.current_path())

        return recipe

    @staticmethod
    def new(cookbook: Cookbook, name: str) -> Recipe:
        recipe = Recipe(cookbook, name)

        recipe.write(recipe.previous_path())
        recipe.write(recipe.current_path())

        return recipe

    cookbook: Cookbook
    name: str

    def __init__(self, cookbook: Cookbook, name: str) -> None:
        self.cookbook = cookbook
        self.name = name

    def dir_path(self) -> str:
        return self.cookbook.path + self.name + "/"

    def previous_path(self) -> str:
        return self.dir_path() + "previous.json"

    def current_path(self) -> str:
        return self.dir_path() + "current.json"

    def ref(self) -> Dict[str, object]:
        return {
            "_ansiblekit_recipe": self.name
        }

    def push(self, osfamily: OSFamily) -> ActionResult:
        result = osfamily.push(self.name, self.current_path())

        return ActionResult.success(
            changed=result.changed(),
            ansible_facts={
                "_ansiblekit_recipe": None
            }
        )

    def update(self, patch: Dict[str, object]) -> bool:
        previous = self.read(self.previous_path())
        current = self.read(self.current_path())

        changed = False
        for k, v in patch.items():
            current[k] = v
            if changed:
                continue
            changed = (previous.get(k) != v)

        self.write(self.current_path(), current)

        return changed

    def read(self, path: str) -> Dict[str, object]:
        with open(path, "r") as f:
            try:
                unverified = json.load(f)
            except json.JSONDecodeError:
                raise ExpectedException("Invalid recipe format")

            if isinstance(unverified, dict):
                return cast(Dict[str, object], unverified)

            raise ExpectedException("Invalid recipe format")

    def write(self, path: str, body: Dict[str, object] = {}) -> None:
        with open(path, "w") as f:
            f.write(json.dumps(body))
            f.write("\n")
