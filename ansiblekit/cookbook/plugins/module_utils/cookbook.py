from __future__ import annotations

import tempfile

from typing import Dict

from ....core.plugins.module_utils.errors import ExpectedException
from ....core.plugins.module_utils.unverified_dict import UnverifiedDict

class Cookbook:
    class NotOpenedException(ExpectedException):
        def __init__(self) -> None:
            super().__init__("Cookbook not opened")

    @staticmethod
    def create() -> Cookbook:
        return Cookbook(
            tempfile.mkdtemp(prefix="ansible-cookbook-") + "/"
        )

    @staticmethod
    def open(vars: UnverifiedDict) -> Cookbook:
        try:
            return Cookbook(
                vars.get_text("_ansiblekit_cookbook"),
            )
        except ExpectedException:
            raise Cookbook.NotOpenedException()

    path: str

    def __init__(self, path: str) -> None:
        self.path = path

    def ref(self) -> Dict[str, object]:
        return {
            "_ansiblekit_cookbook": self.path
        }
