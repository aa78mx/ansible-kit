from ....core.plugins.module_utils.action_base import Action
from ....core.plugins.module_utils.action_base import ActionResult
from ....core.plugins.module_utils.action_base import LinuxUtils
from ....core.plugins.module_utils.action_base import WindowsUtils
from ....core.plugins.module_utils.action_impl import AnsibleAction
from ....core.plugins.module_utils.unverified_dict import UnverifiedDict
from ..module_utils.cookbook import Cookbook
from ..module_utils.osfamily import Linux
from ..module_utils.osfamily import OSFamily
from ..module_utils.osfamily import Windows
from ..module_utils.recipe import Recipe

class CloseAction(Action):
    osfamily: OSFamily
    recipe: Recipe

    def __init__(self, osfamily: OSFamily, args: UnverifiedDict, vars: UnverifiedDict) -> None:
        self.osfamily = osfamily
        self.recipe = Recipe.open(
            cookbook=Cookbook.open(vars),
            vars=vars
        )

    def run(self) -> ActionResult:
        return self.recipe.push(self.osfamily)

class ActionModule(AnsibleAction):
    def create_linux_action(self, args: UnverifiedDict, vars: UnverifiedDict, utils: LinuxUtils) -> Action:
        return CloseAction(Linux(utils), args, vars)

    def create_windows_action(self, args: UnverifiedDict, vars: UnverifiedDict, utils: WindowsUtils) -> Action:
        return CloseAction(Windows(utils), args, vars)
