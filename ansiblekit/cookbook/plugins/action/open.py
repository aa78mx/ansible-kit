import re

from ....core.plugins.module_utils.action_base import Action
from ....core.plugins.module_utils.action_base import ActionResult
from ....core.plugins.module_utils.action_base import ExecutorExceptions
from ....core.plugins.module_utils.action_base import LinuxUtils
from ....core.plugins.module_utils.action_base import WindowsUtils
from ....core.plugins.module_utils.action_impl import AnsibleAction
from ....core.plugins.module_utils.errors import ExpectedException
from ....core.plugins.module_utils.unverified_dict import UnverifiedDict
from ..module_utils.cookbook import Cookbook
from ..module_utils.osfamily import Linux
from ..module_utils.osfamily import OSFamily
from ..module_utils.osfamily import Windows
from ..module_utils.recipe import Recipe

class OpenAction(Action):
    osfamily: OSFamily
    name: str
    cookbook: Cookbook

    def __init__(self, osfamily: OSFamily, args: UnverifiedDict, vars: UnverifiedDict) -> None:
        self.osfamily = osfamily

        self.name = args.pop_text("recipe")
        if (not re.match(r"^[a-z][a-z0-9\-]*\.[0-9a-f]{8}\-(?:[0-9a-f]{4}\-){3}[0-9a-f]{12}$", self.name)):
            raise ExpectedException("Invalid recipe name [%s]" % self.name)

        try:
            self.cookbook = Cookbook.open(vars)
        except Cookbook.NotOpenedException:
            self.cookbook = Cookbook.create()

        try:
            opened_recipe = Recipe.open(self.cookbook, vars)

            if (opened_recipe.name == self.name):
                raise ExpectedException("Recipe already opened [%s]" % self.name)

            raise ExpectedException("Only one recipe can use at every moment in time [%s], [%s]" % (opened_recipe.name, self.name))
        except Recipe.NotOpenedException:
            pass

    def run(self) -> ActionResult:
        try:
            recipe = Recipe.pull(self.osfamily, self.cookbook, self.name)
        except ExecutorExceptions.FileNotFoundException:
            recipe = Recipe.new(self.cookbook, self.name)

        return ActionResult.success(
            changed=False,
            ansible_facts={
                **self.cookbook.ref(),
                **recipe.ref()
            }
        )

class ActionModule(AnsibleAction):
    def create_linux_action(self, args: UnverifiedDict, vars: UnverifiedDict, utils: LinuxUtils) -> Action:
        return OpenAction(Linux(utils), args, vars)

    def create_windows_action(self, args: UnverifiedDict, vars: UnverifiedDict, utils: WindowsUtils) -> Action:
        return OpenAction(Windows(utils), args, vars)
