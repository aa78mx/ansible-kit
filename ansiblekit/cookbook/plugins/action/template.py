from ansible_collections.ansiblekit.cookbook.plugins.action.stat import LinuxStatAction
from ansible_collections.ansiblekit.cookbook.plugins.module_utils.cookbook import Cookbook
from ansible_collections.ansiblekit.cookbook.plugins.module_utils.recipe import Recipe
from ansible_collections.ansiblekit.core.plugins.module_utils.action import Action
from ansible_collections.ansiblekit.core.plugins.module_utils.action import ActionResult
from ansible_collections.ansiblekit.core.plugins.module_utils.action import AnsibleAction
from ansible_collections.ansiblekit.core.plugins.module_utils.action import LinuxUtils
from ansible_collections.ansiblekit.core.plugins.module_utils.errors import ExpectedException
from ansible_collections.ansiblekit.core.plugins.module_utils.unverified_dict import UnverifiedDict

class LinuxTemplateAction(Action):
    utils: LinuxUtils
    src: str
    dest: str
    owner: str
    group: str
    mode: str
    recipe: Recipe

    def __init__(self, args: UnverifiedDict, vars: UnverifiedDict, utils: LinuxUtils) -> None:
        self.utils = utils
        self.src = args.pop_text("src")
        self.dest = args.pop_text("dest")
        self.owner = args.pop_text("owner", lambda: "root")
        self.group = args.pop_text("group", lambda: "root")
        self.mode = args.pop_text("mode", lambda: "0600")
        self.recipe = Recipe.open(
            cookbook=Cookbook.open(vars),
            vars=vars
        )

    def run(self) -> ActionResult:
        result = self.utils.template(
            src=self.src,
            dest=self.dest,
            owner=self.owner,
            group=self.group,
            mode=self.mode
        )

        try:
            dest = UnverifiedDict(result).get_text("dest")
        except ExpectedException as e:
            raise ExpectedException.wrap("Unexpected template result", e)

        stat_action = LinuxStatAction(
            utils=self.utils,
            path=dest,
            owner=self.owner,
            group=self.group,
            mode=self.mode,
            recipe=self.recipe
        )

        return stat_action.run()

class ActionModule(AnsibleAction):
    def create_linux_action(self, args: UnverifiedDict, vars: UnverifiedDict, utils: LinuxUtils) -> Action:
        return LinuxTemplateAction(args, vars, utils)
