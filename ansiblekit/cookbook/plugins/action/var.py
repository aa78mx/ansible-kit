from __future__ import annotations

from ansible_collections.ansiblekit.cookbook.plugins.module_utils.cookbook import Cookbook
from ansible_collections.ansiblekit.cookbook.plugins.module_utils.recipe import Recipe
from ansible_collections.ansiblekit.core.plugins.module_utils.action import Action
from ansible_collections.ansiblekit.core.plugins.module_utils.action import ActionResult
from ansible_collections.ansiblekit.core.plugins.module_utils.action import AnsibleAction
from ansible_collections.ansiblekit.core.plugins.module_utils.action import LinuxUtils
from ansible_collections.ansiblekit.core.plugins.module_utils.unverified_dict import UnverifiedDict

class VarAction(Action):
    @staticmethod
    def create(args: UnverifiedDict, vars: UnverifiedDict) -> VarAction:
        return VarAction(
            key=args.pop_text("key"),
            value=args.pop_text("value"),
            recipe=Recipe.open(
                cookbook=Cookbook.open(vars),
                vars=vars
            )
        )

    key: str
    value: str
    recipe: Recipe

    def __init__(self, key: str, value: str, recipe: Recipe) -> None:
        self.key = key
        self.value = value
        self.recipe = recipe

    def run(self) -> ActionResult:
        changed = self.recipe.update({
            self.key: self.value
        })

        return ActionResult.success(
            changed=changed
        )

class ActionModule(AnsibleAction):
    def create_linux_action(self, args: UnverifiedDict, vars: UnverifiedDict, utils: LinuxUtils) -> Action:
        return VarAction.create(args, vars)
