from __future__ import annotations

from ansible_collections.ansiblekit.cookbook.plugins.action.var import VarAction
from ansible_collections.ansiblekit.cookbook.plugins.module_utils.cookbook import Cookbook
from ansible_collections.ansiblekit.cookbook.plugins.module_utils.recipe import Recipe
from ansible_collections.ansiblekit.core.plugins.module_utils.action import Action
from ansible_collections.ansiblekit.core.plugins.module_utils.action import ActionResult
from ansible_collections.ansiblekit.core.plugins.module_utils.action import AnsibleAction
from ansible_collections.ansiblekit.core.plugins.module_utils.action import LinuxUtils
from ansible_collections.ansiblekit.core.plugins.module_utils.errors import ExpectedException
from ansible_collections.ansiblekit.core.plugins.module_utils.unverified_dict import UnverifiedDict

class LinuxStatAction(Action):
    @staticmethod
    def create(args: UnverifiedDict, vars: UnverifiedDict, utils: LinuxUtils) -> LinuxStatAction:
        return LinuxStatAction(
            utils=utils,
            path=args.pop_text("path"),
            owner=args.pop_text("owner", lambda: "root"),
            group=args.pop_text("group", lambda: "root"),
            mode=args.pop_text("mode", lambda: "0600"),
            recipe=Recipe.open(
                cookbook=Cookbook.open(vars),
                vars=vars
            )
        )

    utils: LinuxUtils
    path: str
    owner: str
    group: str
    mode: str
    recipe: Recipe

    def __init__(self, utils: LinuxUtils, path: str, owner: str, group: str, mode: str, recipe: Recipe) -> None:
        self.utils = utils
        self.path = path
        self.owner = owner
        self.group = group
        self.mode = mode
        self.recipe = recipe

    def run(self) -> ActionResult:
        result = self.utils.stat(
            path=self.path
        )

        try:
            owner = result.stat.get_text("pw_name")
            group = result.stat.get_text("gr_name")
            mode = result.stat.get_text("mode")
            checksum = result.stat.get_text("checksum")
            path = result.stat.get_text("path")
        except ExpectedException as e:
            raise ExpectedException.wrap("Unexpected stat result", e)

        if (owner != self.owner):
            raise ExpectedException("Invalid owner: [%s] instead of [%s]" % (owner, self.owner))

        if (group != self.group):
            raise ExpectedException("Invalid group: [%s] instead of [%s]" % (group, self.group))

        if (mode != self.mode):
            raise ExpectedException("Invalid mode: [%s] instead of [%s]" % (mode, self.mode))

        register_file = VarAction(
            key="file://" + path,
            value=checksum,
            recipe=self.recipe
        )

        return register_file.run()

class ActionModule(AnsibleAction):
    def create_linux_action(self, args: UnverifiedDict, vars: UnverifiedDict, utils: LinuxUtils) -> Action:
        return LinuxStatAction.create(args, vars, utils)
