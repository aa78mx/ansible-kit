from generic.unexpected_error import UnexpectedError

def validate_arg(arg: object, types: list, name: str) -> None:
    for type in types:
        if (type is None):
            if (arg is None):
                return
        elif isinstance(arg, type):
            return

    types_as_text = set(map(lambda allowed_type: allowed_type.__name__, types))

    raise UnexpectedError("[%s] is [%s] instead of allowed %s" % (name, arg.__class__.__name__, types_as_text))

def validate_list_arg(arg: object, types: list, name: str) -> None:
    validate_arg(arg, [list], name)

    for item_no, item in enumerate(arg):
        validate_arg(item, types, "%s[%d]" % (name, item_no))

def validate_dict_arg(arg: object, types: list, name: str) -> None:
    validate_arg(arg, [dict], name)

    for key, value in arg.items():
        validate_arg(value, types, "%s[%s]" % (name, key))
