import posixpath

def normalize_dir_path(path: str) -> str:
    if (not isinstance(path, str)):
        raise Exception("invalid type for path var")

    if ("\\" in path):
        raise Exception("unsupported character '\\' detected [%s]" % (path))

    normalized_path = path.replace(posixpath.sep + posixpath.sep, posixpath.sep)
    normalized_path = posixpath.normpath(path)

    if (not path.endswith(posixpath.sep)):
        raise Exception("file path instead of dir path [%s]" % (path))

    if (normalized_path != path[:-1]):
        raise Exception("invalid dir path [%s], [%s]" % (path, normalized_path + posixpath.sep))

    return normalized_path

def validate_file_path(path: str) -> str:
    if (not isinstance(path, str)):
        raise Exception("invalid type for path var")

    if ("\\" in path):
        raise Exception("unsupported character '\\' detected [%s]" % (path))

    normalized_path = path.replace(posixpath.sep + posixpath.sep, posixpath.sep)
    normalized_path = posixpath.normpath(path)

    if (path.endswith(posixpath.sep)):
        raise Exception("dir path instead of file path %s [%s]" % (path))

    if (normalized_path != path):
        raise Exception("invalid file path [%s], [%s]" % (path, normalized_path + posixpath.sep))

    return normalized_path

def validate_dir_path(path: str) -> str:
    return normalize_dir_path(path) + posixpath.sep
