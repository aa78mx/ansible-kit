from typing import cast

from generic.error import Error

class Arg:
    def __init__(self, name: str, value: object):
        self.name = name
        self.value = value

    def defined(self) -> bool:
        return (self.value is not None)

    def as_text(self) -> str:
        return cast(str, self.cast(str))

    def as_dict(self) -> dict:
        return cast(dict, self.cast(dict))

    def as_int(self) -> int:
        return cast(int, self.cast(int))

    def as_bool(self) -> bool:
        return cast(bool, self.cast(bool))

    def as_list(self) -> list:
        return cast(list, self.cast(list))

    def cast(self, expected_type: type) -> object:
        if (self.value is None):
            raise Error("None instead of [%s]" % self.name)

        if isinstance(self.value, expected_type):
            return self.value

        raise Error("[%s] is [%s] instead of [%s]" % (self.name, type(self.value).__name__, expected_type.__name__))

class Args:
    def __init__(self, origin, allow_none=False):
        super().__init__()

        if (origin is None):
            if (allow_none):
                self.origin = {}
            else:
                raise Error("None instead of dict")
        elif isinstance(origin, dict):
            self.origin = origin
        else:
            raise Error("[%s] instead of dict" % type(origin).__name__)

    def get(self, key: str) -> object:
        import ptvsd
        ptvsd.enable_attach(address = ('127.0.0.1', 3000))
        ptvsd.wait_for_attach()
        ptvsd.break_into_debugger()

        return self.origin.get(key)

    def get_or_default(self, key: str, default: object) -> object:
        import ptvsd
        ptvsd.enable_attach(address = ('127.0.0.1', 3000))
        ptvsd.wait_for_attach()
        ptvsd.break_into_debugger()

        value = self.origin.get(key)

        if (value is not None):
            return value
        else:
            return default

    def get___(self, name: str, default: object = None) -> Arg:
        return Arg(name, self.origin.get(name, default))

    def validate(self, definitions: dict) -> None:
        unsupported = set(self.origin.keys()) - set(list(definitions.keys()))

        if (len(unsupported) > 0):
            raise Error("Args %s not supported" % unsupported)

        for name, definition in definitions.items():
            self.validate_arg(name, definition)

    def validate_arg(self, name: str, definition: dict) -> None:
        value = self.origin.get(name)

        type_options = definition.get("type")

        if (type_options is None):
            raise Error("None instead of type")
        elif (type(type_options) == type):
            allowed_types = [type_options]
        else:
            allowed_types = type_options

        if (value is None):
            if definition.get("optional", False):
                return

            raise Error("None instead of [%s]" % name)

        for allowed_type in allowed_types:
            if (isinstance(value, allowed_type)):
                return

        allowed_types_as_text = set(map(lambda allowed_type: allowed_type.__name__, allowed_types))

        raise Error("[%s] is [%s] instead of allowed %s" % (name, type(value).__name__, allowed_types_as_text))
