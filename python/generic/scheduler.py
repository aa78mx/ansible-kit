from generic.report import Report

class Scheduler:
    def __init__(self):
        self.tasks = []

    def add(self, task: lambda: Report) -> None:
        self.tasks.append(task)

        return self

    def run(self) -> [Report]:
        reports = []

        for task in self.tasks:
            reports.append(task())

        return reports
