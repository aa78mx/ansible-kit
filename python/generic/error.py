class Error(Exception):
    def __init__(self, msg, details=None):
        if (details is None):
            super().__init__(msg)
        else:
            details = str(details).replace("->\n  ", "->\n    ").rstrip()

            super().__init__("%s ->\n  %s" % (msg, details))
