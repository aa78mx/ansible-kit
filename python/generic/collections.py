from generic.error import Error
from python.utils.validators import validate_arg

class Set(set):
    def register(self, item_id):
        if (item_id in self):
            raise Error("duplicated key detected [%s]" % item_id)
        else:
            self.add(item_id)

class Map(dict):
    def register(self, item_id, factory, unique=True):
        validate_arg(item_id, [str], "item_id")
        validate_arg(unique, [bool], "unique")

        item = self.get(item_id)

        if (item is None):
            item = factory(item_id)
            self[item_id] = item
        elif (unique):
            raise Error("duplicated key detected [%s]" % item_id)

        return item

    def sorted_values(self):
        sorted_pairs = sorted(self.items(), key=lambda pair: pair[0])

        return list(map(lambda pair: pair[1], sorted_pairs))

    def find(self, item_id, item_name):
        validate_arg(item_id, [str], item_name + "_id")

        result = self.get(item_id)

        if (result is None):
            raise Error("%s [%s] is not defined" % (item_name, item_id))

        return result
