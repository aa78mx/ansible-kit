class Report:
    def __init__(self):
        super().__init__()

        self.errors = []

    def error(self, error: str) -> None:
        self.errors.append(error)

    def has_errors(self) -> bool:
        return len(self.errors) != 0

    def show(self) -> str:
        return "\n".join(self.errors)
