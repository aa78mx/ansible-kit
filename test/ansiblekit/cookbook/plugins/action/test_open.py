import random
import string
import unittest

from typing import Dict
from typing import cast

from ansiblekit.cookbook.plugins.action.close import CloseAction
from ansiblekit.cookbook.plugins.action.open import OpenAction
from ansiblekit.cookbook.plugins.module_utils.osfamily import OSFamily
from ansiblekit.core.plugins.module_utils.action_base import ActionResult
from ansiblekit.core.plugins.module_utils.errors import ExpectedException
from ansiblekit.core.plugins.module_utils.unverified_dict import UnverifiedDict

class FakeOSFamily(OSFamily):
    def pull(self, name: str, path: str) -> ActionResult:
        return ActionResult.success(False)

    def push(self, name: str, path: str) -> ActionResult:
        return ActionResult.success(False)

class ActionTest(unittest.TestCase):
    def test_raise_exception_on_bad_recipe_name(self) -> None:
        name = "".join(random.choice(string.ascii_letters) for _ in range(10))
        osfamily = FakeOSFamily()
        vars: Dict[str, object] = {}
        args: object = {
            "recipe": name
        }

        with self.assertRaises(ExpectedException) as context:
            OpenAction(osfamily, UnverifiedDict(args), UnverifiedDict(vars))

        self.assertEqual(
            first="Invalid recipe name [%s]" % name,
            second=str(context.exception)
        )

    def test_raise_exception_if_recipe_already_opened(self) -> None:
        name = "foo.cc147fe5-067a-4f2d-982d-abc4b2cba074"
        osfamily = FakeOSFamily()
        vars: Dict[str, object] = {}
        args: object = {
            "recipe": name
        }

        open_result = OpenAction(osfamily, UnverifiedDict(args), UnverifiedDict(vars)).run()
        vars.update(cast(Dict[str, object], open_result.get("ansible_facts")))

        with self.assertRaises(ExpectedException) as context:
            OpenAction(osfamily, UnverifiedDict(args), UnverifiedDict(vars))

        self.assertEqual(
            first="Recipe already opened [%s]" % name,
            second=str(context.exception)
        )

    def test_raise_exception_if_recipe_reuse(self) -> None:
        name = "bar.a3056810-7cc1-4091-8492-d1364dd75301"

        osfamily = FakeOSFamily()
        vars: Dict[str, object] = {}
        args: object = {
            "recipe": name
        }

        open_result = OpenAction(osfamily, UnverifiedDict(args), UnverifiedDict(vars)).run()
        vars.update(cast(Dict[str, object], open_result.get("ansible_facts")))
        close_result = CloseAction(osfamily, UnverifiedDict(args), UnverifiedDict(vars)).run()
        vars.update(cast(Dict[str, object], close_result.get("ansible_facts")))

        with self.assertRaises(ExpectedException) as context:
            OpenAction(osfamily, UnverifiedDict(args), UnverifiedDict(vars)).run()

        self.assertEqual(
            first="Not use the same recipe twice [%s]" % name,
            second=str(context.exception)
        )

    def test_raise_exception_on_try_to_use_two_recipe_at_the_same_time(self) -> None:
        first = "baz.9af8de1a-e850-4dd9-845e-f19863219660"
        second = "etc.d1210306-e0d3-40f4-b0e3-e179be488669"

        osfamily = FakeOSFamily()
        vars: Dict[str, object] = {}
        first_args: object = {
            "recipe": first
        }
        second_args: object = {
            "recipe": second
        }

        open_first = OpenAction(osfamily, UnverifiedDict(first_args), UnverifiedDict(vars)).run()
        vars.update(cast(Dict[str, object], open_first.get("ansible_facts")))

        with self.assertRaises(ExpectedException) as context:
            OpenAction(osfamily, UnverifiedDict(second_args), UnverifiedDict(vars)).run()

        self.assertEqual(
            first="Only one recipe can use at every moment in time [%s], [%s]" % (first, second),
            second=str(context.exception)
        )

if (__name__ == '__main__'):
        unittest.main()
