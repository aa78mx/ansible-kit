# controller

```yml
- hosts: localhost
  gather_facts: false
  become: no
  tasks:
    - name: install | preinstall controller
      include_role:
        name: controller/preinstall
  vars:
    ansible_connection: local
```
