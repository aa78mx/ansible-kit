# cookbook

## 1.1. Добавляем cookbook в зависимости роли *meta/main.yml*

```yml
---
dependencies:
  - cookbook
```

## 2.1. open, save and close cookbook

**Внимание!** В каждый момент времени для отдельного узла может быть открыта только одна книга

**Внимание!** Использовать дважды одну книгу запрещено

```yml
tasks:
  - name: open cookbook
    cookbook:
      cookbook_state: open # required
      cookbook_name: temp.b9458a7a-a4aa-44ff-b966-5352514c4ee3 # required

  - name: only save cookbook
    cookbook:
      cookbook_state: save # required

  - name: save and close cookbook
    cookbook:
      cookbook_state: close # required
```

## 2.2. cookbook_copy

```yml
# Linux
tasks:
  - cookbook_copy:
      src: "{{src}}" # required
      dest: "{{dest}}" # required
      owner: "{{owner}}" # default root
      group: "{{group}}" # default root
      mode: "{{mode}}" # default 0600
```

## 2.3. cookbook_template

```yml
# Linux
tasks:
  - cookbook_template:
      src: "{{src}}" # required
      dest: "{{dest}}" # required
      owner: "{{owner}}" # default root
      group: "{{group}}" # default root
      mode: "{{mode}}" # default 0600
```

## 2.4. systemd_env

```yml
tasks:
  - systemd_env:
      service: "{{service}}" # required
      envfile: "{{envfile}}" # required
      envs: "{{envs}}" # required
```

## 2.5. cookbook_stat

```yml
tasks:
  - cookbook_stat:
      path: "{{path}}" # required
      owner: "{{owner}}" # default root
      group: "{{group}}" # default root
      mode: "{{mode}}" # default 0600
```

## 2.6. cookbook_var

```yml
tasks:
  - cookbook_var:
      k1: v1
      k2: v2
      ...
      kN: vN
```

## 2.7. deploy_files

```yml
# Linux
tasks:
  - deploy_files:
      owner: "{{owner}}" # default root
      group: "{{group}}" # default root
      root_dir: /etc/
      child_dirs:
        "./dir01/": # host directory
          copy_files:
            - files/foo/bar/baz # dest = /etc/dir01/baz
          stat_files:
            - bar # dest = /etc/dir01/bar
          template_files:
            - foo.j2 # dest = /etc/dir01/foo
        "./dir02/":
          copy_files:
            - files/foo/bar/baz # dest = /etc/dir02/baz
          stat_files:
            - bar # dest = /etc/dir02/bar
          template_files:
            - foo.j2 # dest = /etc/dir02/foo
```
