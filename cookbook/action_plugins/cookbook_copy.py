import os
import sys

sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), "../../")))

from ansible.plugins.action import ActionBase

from python.utils.args import Args
from python.utils.path import validate_file_path

class Base:
    def __init__(self, args):
        self.src = validate_file_path(args.get("src"))
        self.dest = validate_file_path(args.get("dest"))

    def run(self, execute_action):
        copy_result = execute_action(self.copy_options())

        if (copy_result.get("failed")):
            return copy_result

        return execute_action(self.stat_options())

class Windows(Base):
    def __init__(self, args):
        args.validate({
            "src": {"type": str},
            "dest": {"type": str}
        })

        super(Windows, self).__init__(args)

    def copy_options(self):
        return {
            "action": "win_copy",
            "args": {
                "src": self.src,
                "dest": self.dest
            }
        }

    def stat_options(self):
        return {
            "action": "cookbook_stat",
            "args": {
                "path": self.dest
            }
        }

class Linux(Base):
    def __init__(self, args):
        args.validate({
            "src": {"type": str},
            "dest": {"type": str},
            "owner": {"type": str, "optional": True},
            "group": {"type": str, "optional": True},
            "mode": {"type": str, "optional": True}
        })

        super(Linux, self).__init__(args)

        self.owner = args.get_or_default("owner", "root")
        self.group = args.get_or_default("group", "root")
        self.mode = args.get_or_default("mode", "0600")

    def copy_options(self):
        return {
            "action": "copy",
            "args": {
                "src": self.src,
                "dest": self.dest,
                "owner": self.owner,
                "group": self.group,
                "mode": self.mode
            }
        }

    def stat_options(self):
        return {
            "action": "cookbook_stat",
            "args": {
                "path": self.dest,
                "owner": self.owner,
                "group": self.group,
                "mode": self.mode
            }
        }

class ActionModule(ActionBase):
    def execute_action(self, args):
        task = self._task.copy().load_data(
            ds=args,
            variable_manager=self._task.get_variable_manager(),
            loader=self._task.get_loader()
        )

        action = self._shared_loader_obj.action_loader.get(
            args.get("action"),
            task=task,
            connection=self._connection,
            play_context=self._play_context,
            loader=self._loader,
            templar=self._templar,
            shared_loader_obj=self._shared_loader_obj
        )

        return action.run(self.tmp, self.task_vars)

    def run(self, tmp=None, task_vars=None):
        super(ActionModule, self).run(tmp, task_vars)

        self.tmp = tmp
        self.task_vars = task_vars

        args = Args(self._task.args)

        if (self._connection.transport == "winrm"):
            return Windows(args).run(self.execute_action)
        else:
            return Linux(args).run(self.execute_action)
