import os
import posixpath
import sys

sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), "../../")))

from ansible.plugins.action import ActionBase

from python.utils.args import Args
from python.utils.path import normalize_dir_path

class Base:
    def __init__(self, args):
        self.root_dir = normalize_dir_path(args.get("root_dir"))
        self.child_dirs = args.get("child_dirs")

    def run(self, execute_module, execute_action):
        result = {
            "changed": False,
            "ansible_facts": {},
            "status": {}
        }

        for child_dir_path, child_dir_args in self.child_dirs.items():
            child_dir_path = self.path_join(child_dir_path)

            child_dir_args = Args(child_dir_args)
            child_dir_args.validate({
                "template_files": {"type": list, "optional": True},
                "copy_files": {"type": list, "optional": True},
                "stat_files": {"type": list, "optional": True}
            })

            dir_create_result = execute_module(self.dir_create_options(child_dir_path))
            if dir_create_result.get("failed"):
                return dir_create_result

            for kind in ["copy_files", "template_files", "stat_files"]:
                action_args_factory = getattr(self, "%s_args" % kind)
                for path in child_dir_args.get_or_default(kind, []):
                    action_args = action_args_factory(child_dir_path, path)

                    action_result = execute_action(action_args)

                    if action_result.get("failed"):
                        return action_result
                    else:
                        if action_result.get("changed"):
                            result["changed"] = True
                        result.get("ansible_facts").update(action_result.get("ansible_facts"))
                        result.get("status").update(action_result.get("status"))

        result["_ansible_verbose_always"] = result.get("changed")

        return result

    def path_join(self, path):
        if (path.startswith("./")):
            path = path[2:]

        return normalize_dir_path(posixpath.join(self.root_dir, path))

class Windows(Base):
    def __init__(self, args):
        args.validate({
            "root_dir": {"type": str},
            "child_dirs": {"type": dict}
        })

        super(Windows, self).__init__(args)

    def dir_create_options(self, path):
        return {
            "name": "win_file",
            "args": {
                "path": path,
                "state": "directory"
            }
        }

    def copy_files_args(self, dest, src):
        return {
            "action": "cookbook_copy",
            "args": {
                "src": src,
                "dest": posixpath.join(dest, posixpath.basename(src))
            }
        }

    def template_files_args(self, dest, src):
        return {
            "action": "cookbook_template",
            "args": {
                "src": src,
                "dest": posixpath.join(dest, posixpath.basename(src).replace(".j2", ""))
            }
        }

    def stat_files_args(self, dest, name):
        return {
            "action": "cookbook_stat",
            "args": {
                "path": posixpath.join(dest, name)
            }
        }

class Linux(Base):
    def __init__(self, args):
        args.validate({
            "root_dir": {"type": str},
            "child_dirs": {"type": dict},
            "owner": {"type": str, "optional": True},
            "group": {"type": str, "optional": True}
        })

        super(Linux, self).__init__(args)

        self.owner = args.get_or_default("owner", "root")
        self.group = args.get_or_default("group", "root")

    def dir_create_options(self, path):
        return {
            "name": "file",
            "args": {
                "path": path,
                "state": "directory",
                "owner": self.owner,
                "group": self.group,
                "mode": "0755"
            }
        }

    def copy_files_args(self, dest, src):
        return {
            "action": "cookbook_copy",
            "args": {
                "src": src,
                "dest": posixpath.join(dest, posixpath.basename(src)),
                "owner": self.owner,
                "group": self.group
            }
        }

    def template_files_args(self, dest, src):
        return {
            "action": "cookbook_template",
            "args": {
                "src": src,
                "dest": posixpath.join(dest, posixpath.basename(src).replace(".j2", "")),
                "owner": self.owner,
                "group": self.group
            }
        }

    def stat_files_args(self, dest, name):
        return {
            "action": "cookbook_stat",
            "args": {
                "path": posixpath.join(dest, name),
                "owner": self.owner,
                "group": self.group
            }
        }

class ActionModule(ActionBase):
    def execute_module(self, options):
        return self._execute_module(
            module_name=options.get("name"),
            module_args=options.get("args"),
            task_vars=self.task_vars,
            tmp=self.tmp
        )

    def execute_action(self, args):
        task = self._task.copy().load_data(
            ds=args,
            variable_manager=self._task.get_variable_manager(),
            loader=self._task.get_loader()
        )

        action = self._shared_loader_obj.action_loader.get(
            args.get("action"),
            task=task,
            connection=self._connection,
            play_context=self._play_context,
            loader=self._loader,
            templar=self._templar,
            shared_loader_obj=self._shared_loader_obj
        )

        return action.run(self.tmp, self.task_vars)

    def run(self, tmp=None, task_vars=None):
        super(ActionModule, self).run(tmp, task_vars)

        self.tmp = tmp
        self.task_vars = task_vars

        args = Args(self._task.args)

        if (self._connection.transport == "winrm"):
            return Windows(args).run(self.execute_module, self.execute_action)
        else:
            return Linux(args).run(self.execute_module, self.execute_action)
