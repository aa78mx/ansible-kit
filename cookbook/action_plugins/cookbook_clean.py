import os
import sys

sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), "../../")))

from ansible.plugins.action import ActionBase

from python.utils.args import Args

class Base:
    def __init__(self, task_args, task_vars):
        super().__init__()

        task_args.validate({})

        self.cookbook_old = task_vars.get("cookbook_old")
        self.cookbook_new = task_vars.get("cookbook_new", {})

        if (self.cookbook_new is None):
            raise AnsibleError("cookbook not opened")

    def run(self, execute_module):
        changed = False
        details = {}

        for key in self.cookbook_old.keys():
            if (key.startswith("file://") and (key not in self.cookbook_new)):
                changed = True

                rm_result = execute_module(self.rm_options(key[7:]))
                if rm_result.get("failed"):
                    return rm_result

                details[key] = "removed"

        return {
            "changed": changed,
            "details": details,
            "_ansible_verbose_always": changed
        }

class Windows(Base):
    pass

class Linux(Base):
    def rm_options(self, path):
        return {
            "name": "file",
            "args": {
                "path": path,
                "state": "absent"
            }
        }

class ActionModule(ActionBase):
    def execute_module(self, options):
        return self._execute_module(
            module_name=options.get("name"),
            module_args=options.get("args"),
            task_vars=self.task_vars,
            tmp=self.tmp
        )

    def run(self, tmp=None, task_vars=None):
        super(ActionModule, self).run(tmp, task_vars)

        self.tmp = tmp
        self.task_vars = task_vars

        args = Args(self._task.args)

        if (self._connection.transport == "winrm"):
            return Windows(args, task_vars).run(self.execute_module)
        else:
            return Linux(args, task_vars).run(self.execute_module)
