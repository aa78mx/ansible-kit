from ansible.plugins.action import ActionBase
from ansible.plugins.action import AnsibleError

class ActionModule(ActionBase):
    def run(self, tmp=None, task_vars=None):
        super(ActionModule, self).run(tmp, task_vars)

        cookbook_old = task_vars.get("cookbook_old")
        cookbook_new = task_vars.get("cookbook_new")

        if (cookbook_new is None):
            raise AnsibleError("cookbook not opened")

        result = {
            "changed": False,
            "ansible_facts": {
                "cookbook_new": cookbook_new
            },
            "status": {}
        }

        for key, value in self._task.args.items():
            key = self._templar.template(key)

            if (not isinstance(value, str)):
                raise AnsibleError("invalid var value [%s]" % key)

            cookbook_new[key] = value

            changed = (key not in cookbook_old) or (cookbook_old.get(key) != cookbook_new.get(key))

            result["status"][key] = changed

            if changed:
                result["changed"] = True

        result["_ansible_verbose_always"] = result.get("changed")

        return result
