import os
import sys

sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), "../../")))

from ansible.plugins.action import ActionBase

from python.utils.args import Args
from python.utils.path import validate_file_path

class Base:
    def __init__(self, args):
        self.path = validate_file_path(args.get("path"))

    def run(self, execute_module, execute_action):
        stat_result = self.stat(execute_module)

        if stat_result.get("failed"):
            return stat_result

        return execute_action({
            "action": "cookbook_var",
            "args": {
                "file://" + stat_result.get("path"): stat_result.get("hash")
            }
        })

class Windows(Base):
    def __init__(self, args):
        args.validate({
            "path": {"type": str}
        })

        super(Windows, self).__init__(args)

    def stat(self, execute_module):
        stat_result = execute_module(
            name="stat",
            args={
                "path": self.path
            }
        )
        if stat_result.get("failed"):
            return stat_result

        stat = stat_result.get("stat")

        return {
            "path": self.path,
            "hash": "%s--%s" % (stat.get("checksum"), stat.get("lastwritetime"))
        }

class Linux(Base):
    def __init__(self, args):
        args.validate({
            "path": {"type": str},
            "owner": {"type": str, "optional": True},
            "group": {"type": str, "optional": True},
            "mode": {"type": str, "optional": True}
        })

        super(Linux, self).__init__(args)

        self.owner = args.get_or_default("owner", "root")
        self.group = args.get_or_default("group", "root")
        self.mode = args.get_or_default("mode", "0600")

    def stat(self, execute_module):
        stat_result = execute_module(
            name="stat",
            args={
                "path": self.path
            }
        )
        if stat_result.get("failed"):
            return stat_result

        stat = stat_result.get("stat")
        if (not stat.get("exists")):
            return {
                "path": self.path,
                "hash": "null"
            }
        elif ((stat.get("pw_name") == self.owner) and (stat.get("gr_name") == self.group) and (stat.get("mode") == self.mode)):
            return {
                "path": self.path,
                "hash": "%s--%s" % (stat.get("checksum"), stat.get("ctime"))
            }
        else:
            file_result = execute_module(
                name="file",
                args={
                    "path": self.path,
                    "state": "file",
                    "owner": self.owner,
                    "group": self.group,
                    "mode": self.mode
                }
            )
            if file_result.get("failed"):
                return file_result

            return self.stat(execute_module)

class ActionModule(ActionBase):
    def execute_module(self, name, args):
        return self._execute_module(
            module_name=name,
            module_args=args,
            task_vars=self.task_vars,
            tmp=self.tmp
        )

    def execute_action(self, args):
        task = self._task.copy().load_data(
            ds=args,
            variable_manager=self._task.get_variable_manager(),
            loader=self._task.get_loader()
        )

        action = self._shared_loader_obj.action_loader.get(
            args.get("action"),
            task=task,
            connection=self._connection,
            play_context=self._play_context,
            loader=self._loader,
            templar=self._templar,
            shared_loader_obj=self._shared_loader_obj
        )

        return action.run(self.tmp, self.task_vars)

    def run(self, tmp=None, task_vars=None):
        super(ActionModule, self).run(tmp, task_vars)

        self.tmp = tmp
        self.task_vars = task_vars

        args = Args(self._task.args)

        if (self._connection.transport == "winrm"):
            return Windows(args).run(self.execute_module, self.execute_action)
        else:
            return Linux(args).run(self.execute_module, self.execute_action)
