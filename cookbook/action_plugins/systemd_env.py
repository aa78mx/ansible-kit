import os
import posixpath
import sys

sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), "../../")))

from ansible.plugins.action import ActionBase

from python.utils.args import Args

class ActionModule(ActionBase):
    def execute_module(self, name, args):
        return self._execute_module(
            module_name=name,
            module_args=args,
            task_vars=self.task_vars,
            tmp=self.tmp
        )

    def execute_action(self, args):
        task = self._task.copy().load_data(
            ds=args,
            variable_manager=self._task.get_variable_manager(),
            loader=self._task.get_loader()
        )

        action = self._shared_loader_obj.action_loader.get(
            args.get("action"),
            task=task,
            connection=self._connection,
            play_context=self._play_context,
            loader=self._loader,
            templar=self._templar,
            shared_loader_obj=self._shared_loader_obj
        )

        return action.run(self.tmp, self.task_vars)

    def run(self, tmp=None, task_vars=None):
        super(ActionModule, self).run(tmp, task_vars)

        self.tmp = tmp
        self.task_vars = task_vars

        args = Args(self._task.args)

        args.validate({
            "service": {"type": str},
            "envfile": {"type": str},
            "envs": {"type": dict}
        })

        service = args.get("service")
        envfile = args.get("envfile")
        self.task_vars["envs"] = args.get_or_default("envs", {})

        service_conf_dir = "/etc/systemd/system/%s.service.d" % service

        conf_dir_create_result = self.execute_module(
            name="file",
            args={
                "path": service_conf_dir,
                "state": "directory",
                "owner": "root",
                "group": "root",
                "mode": "0755"
            }
        )

        if conf_dir_create_result.get("failed"):
            return conf_dir_create_result

        return self.execute_action({
            "action": "cookbook_template",
            "args": {
                "src": "roles/cookbook/templates/systemd/service.conf.j2",
                "dest": posixpath.join(service_conf_dir, "%s.conf" % envfile),
                "owner": "root",
                "group": "root",
                "mode": "0644"
            }
        })
