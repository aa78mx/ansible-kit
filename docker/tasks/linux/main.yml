---
- name: "docker::linux.main | open cookbook"
  cookbook:
    cookbook_state: open
    cookbook_name: docker.65cbb89d-da93-499a-a860-6cf39f66b8b4

- name: "docker::linux.main | get nobody uid"
  user:
    name: nobody
    state: present
  register: _docker_nobody

- name: "docker::linux.main | get nogroup gid"
  group:
    name: nogroup
    state: present
  register: _docker_nogroup

- name: "docker::linux.main | add subuid"
  lineinfile:
    path: /etc/subuid
    line: "nobody:{{_docker_nobody.uid}}:65536"
    state: present
    regexp: nobody:\d+:\d+

- name: "docker::linux.main | add subgid"
  lineinfile:
    path: /etc/subgid
    line: "nogroup:{{_docker_nogroup.gid}}:65536"
    state: present
    regexp: nogroup:\d+:\d+

- name: "docker::linux.main | install python modules"
  pip:
    name:
      - "docker=={{_docker_py_version}}"
  environment: "{{proxy_env | nvl({})}}"

- name: "docker::linux.main | add docker repo key"
  apt_key:
    url: "https://download.docker.com/linux/{{ansible_distribution | lower}}/gpg"
    id: 9DC858229FC7DD38854AE2D88D81803C0EBFCD88
    state: present
  environment: "{{proxy_env | nvl({})}}"

- name: "docker::linux.main | add docker repo"
  apt_repository:
    repo: "deb [arch=amd64] https://download.docker.com/linux/{{ansible_distribution | lower}} {{ansible_distribution_release | lower}} stable"
    state: present
    filename: /etc/apt/sources.list.d/docker
    update_cache: true

- name: "docker::linux.main | create /etc/docker/daemon.json"
  deploy_files:
    owner: root
    group: root
    root_dir: /etc/docker/
    child_dirs:
      "./":
        template_files:
          - templates/daemon.json.j2
  vars:
    _docker_daemon_conf_default:
      # Boolean and numeric values (such as the value for max-file in the example above) must therefore be enclosed in quotes (")
      log-driver: "json-file"
      log-opts:
        max-size: "50m"
        max-file: "1"
        compress: "false"
      userns-remap: "nobody:nogroup"
  register: _docker_configs

- name: "docker::linux.main | identify files /etc/docker/"
  identify_files:
    path: /etc/docker/
    patterns:
      - /etc/docker/daemon.json
      - /etc/docker/key.json

- name: "docker::linux.main | set proxy for docker.service"
  systemd_env:
    service: docker
    envfile: http-proxy
    envs: "{{proxy_env | nvl({})}}"
  register: _docker_service_configs

- name: "docker::linux.main | identify files /etc/systemd/system/docker.service.d/"
  identify_files:
    path: /etc/systemd/system/docker.service.d/
    patterns:
      - /etc/systemd/system/docker.service.d/http-proxy.conf

- name: "docker::linux.main | allow to change the docker version"
  dpkg_selections:
    name: "{{item}}"
    selection: install
  changed_when: false
  with_items: &package_list
    - docker-ce
    - docker-ce-cli
    - containerd.io

- name: "docker::linux.main | install docker ({{_docker_version}})"
  apt:
    name:
      - "docker-ce={{_version}}"
      - "docker-ce-cli={{_version}}"
      - "containerd.io"
    state: present
  vars:
    _version: "5:{{_docker_version}}~3-0~{{ansible_distribution | lower}}-{{ansible_distribution_release | lower}}"
  environment: "{{proxy_env | nvl({})}}"

- name: "docker::linux.main | prevent to change the docker version"
  dpkg_selections:
    name: "{{item}}"
    selection: hold
  changed_when: false
  with_items: *package_list

- name: "docker::linux.main | enable docker.service"
  systemd:
    name: docker
    enabled: true
    daemon_reload: true
    state: "{{'restarted' if _docker_settings_changed else 'started'}}"
  vars:
    _docker_settings_changed: "{{_docker_configs.changed or _docker_service_configs.changed}}"

- name: "docker::linux.main | identify files /var/lib/docker/"
  identify_files:
    path: /var/lib/docker/
    patterns:
      - "/var/lib/docker/{{_docker_nobody.uid}}.{{_docker_nogroup.gid}}"

- name: "docker::linux.main | save and close cookbook"
  cookbook:
    cookbook_state: close
