# docker

```yml
tasks:
  - include_role:
      name: docker
    vars:
      docker_daemon_conf:
        dns:
          - 127.0.0.53
      proxy_env:
        http_proxy: http://proxy:8080
        https_proxy: http://proxy:8080
        no_proxy: 127.0.0.1
```
