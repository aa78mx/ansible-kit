import json
import re

from ansible import constants as C
from ansible.plugins.action import ActionBase
from ansible.plugins.action import AnsibleError

class Meta:
    def __init__(self):
        self.vhosts = {}
        self.exchanges = {}
        self.queues = {}
        self.bindings = {}

class Item:
    def __init__(self, state, hash):
        self.state = state
        self.hash = hash

    def status(self, display):
        if self.state == "added":
            display(msg="* add %s %s" % (self.__class__.__name__.lower(), self.hash), color=C.COLOR_OK)
        else:
            display(msg="* del %s %s" % (self.__class__.__name__.lower(), self.hash), color=C.COLOR_ERROR)

class Vhost(Item):
    def __init__(self, state, name):
        super(Vhost, self).__init__(state, ".vhost[%s]" % json.dumps(name))

        self.name = name

class Exchange(Item):
    def __init__(self, state, vhost, name, type):
        super(Exchange, self).__init__(state, ".vhost[%s].exchange[%s].type[%s]" % (json.dumps(vhost), json.dumps(name), json.dumps(type)))

        self.vhost = vhost
        self.name = name
        self.type = type

class Queue(Item):
    def __init__(self, state, vhost, name, dlx, dlk):
        super(Queue, self).__init__(state, ".vhost[%s].queue[%s].dlx[%s].dlk[%s]" % (json.dumps(vhost), json.dumps(name), json.dumps(dlx), json.dumps(dlk)))

        self.vhost = vhost
        self.name = name
        self.dlx = dlx
        self.dlk = dlk

class Binding(Item):
    def __init__(self, state, vhost, destination, source, routing_key):
        super(Binding, self).__init__(state, ".vhost[%s].destination[%s].source[%s].routingKey[%s]" % (json.dumps(vhost), json.dumps(destination), json.dumps(source), json.dumps(routing_key)))

        self.vhost = vhost
        self.destination = destination
        self.source = source
        self.routing_key = routing_key

class DiffGroup:
    def __init__(self, source, target):
        self.removed = source.copy()
        self.added = target.copy()

class Diff:
    def __init__(self, source: Meta, target: Meta):
        self.vhosts = self.compare(source.vhosts, target.vhosts)
        self.exchanges = self.compare(source.exchanges, target.exchanges)
        self.queues = self.compare(source.queues, target.queues)
        self.bindings = self.compare(source.bindings, target.bindings)

    def compare(self, source, target):
        diff = DiffGroup(source, target)

        return diff

    def exec(self, action):
        for items in [self.queues.removed]:
            for pair in sorted(items.items()):
                action(pair[1])
        for items in [self.queues.added]:
            for pair in sorted(items.items()):
                action(pair[1])

class ActionModule(ActionBase):
    @staticmethod
    def validate_args(args, required, optional={}):
        unsupportedArgs = set(args.keys())

        for argName in required.keys():
            if (args.get(argName) is None):
                raise AnsibleError("%s is required" % argName)

        for argName, argType in {**required, **optional}.items():
            argValue = args.get(argName)
            if (argValue is not None) and not isinstance(argValue, argType):
                raise AnsibleError("invalid arg %s" % argName)

        unsupportedArgs -= set(list(required.keys()))
        unsupportedArgs -= set(list(optional.keys()))

        if len(unsupportedArgs) > 0:
            raise AnsibleError("args %s not supported" % unsupportedArgs)

    def parse_raw_source(self, raw):
        meta = Meta()

        for newVhost in raw.get("vhosts"):
            newVhost = Vhost(
                state="removed",
                name=newVhost.get("name")
            )
            meta.vhosts[newVhost.hash] = newVhost

        for exchange in raw.get("exchanges"):
            durable = exchange.get("durable")
            autoDelete = exchange.get("auto_delete")

            if durable and not autoDelete:
                newExchange = Exchange(
                    state="removed",
                    vhost=exchange.get("vhost"),
                    name=exchange.get("name"),
                    type=exchange.get("type")
                )
                meta.exchanges[newExchange.hash] = newExchange

        persistentQueues = {}
        for queue in raw.get("queues"):
            durable = queue.get("durable")
            autoDelete = queue.get("auto_delete")

            if durable and not autoDelete:
                arguments = queue.get("arguments")
                newQueue = Queue(
                    state="removed",
                    vhost=queue.get("vhost"),
                    name=queue.get("name"),
                    dlx=arguments.get("x-dead-letter-exchange"),
                    dlk=arguments.get("x-dead-letter-routing-key")
                )
                persistentQueues[(newQueue.vhost, newQueue.name)] = {}
                meta.queues[newQueue.hash] = newQueue

        for binding in raw.get("bindings"):
            newBinding = Binding(
                state="removed",
                vhost=binding.get("vhost"),
                destination=binding.get("destination"),
                source=binding.get("source"),
                routing_key=binding.get("routing_key")
            )
            if (binding.get("destination_type") == "queue") and persistentQueues.get((newBinding.vhost, newBinding.destination)) is not None:
                meta.bindings[newBinding.hash] = newBinding

        return meta

    def run(self, tmp=None, task_vars=None):
        super(ActionModule, self).run(tmp, task_vars)

        ActionModule.validate_args(self._task.args, {
            "management_user": str,
            "management_url": str,
            "management_pass": str,
            "state": str
        })

        state = self._task.args.get("state")
        if state not in ["status"]:
            raise AnsibleError("unsupported state %s" % state)

        managementUrl = "%s/api/definitions" % re.sub("/+$", "", self._task.args.get("management_url"))
        fetchSourceResponse = self._execute_module(
            module_name="uri",
            module_args={
                "url": managementUrl,
                "method": "GET",
                "user": self._task.args.get("management_user"),
                "password": self._task.args.get("management_pass")
            },
            task_vars=task_vars,
            tmp=tmp
        )
        if fetchSourceResponse.get("failed"):
            return fetchSourceResponse

        source = self.parse_raw_source(fetchSourceResponse.get("json"))
        target = Meta()
        diff = Diff(source, target)

        if state == "status":
            diff.exec(lambda item: item.status(self._display.display))
        else:
            raise AnsibleError("unsupported state %s" % state)

        return {
            "failed": False
        }
