import json
import pathlib

from python.collections import Map
from python.error import Error
from python.utils.args import Args
from python.utils.path import normalize_dir_path
from python.utils.validators import validate_arg

from rabbitmq.databus.library.configurator.models.base import BackendCfg
from rabbitmq.databus.library.configurator.models.base import ConsumerNode
from rabbitmq.databus.library.configurator.models.base import ConsumerSystem
from rabbitmq.databus.library.configurator.models.base import Entity
from rabbitmq.databus.library.configurator.models.base import Environment
from rabbitmq.databus.library.configurator.models.base import FrontendCfg
from rabbitmq.databus.library.configurator.models.base import FrontendPath
from rabbitmq.databus.library.configurator.models.base import Meta
from rabbitmq.databus.library.configurator.models.base import Node
from rabbitmq.databus.library.configurator.models.base import NodeProfile
from rabbitmq.databus.library.configurator.models.base import Peer
from rabbitmq.databus.library.configurator.models.base import ProducerNode
from rabbitmq.databus.library.configurator.models.base import Product
from rabbitmq.databus.library.configurator.models.base import ProductKind
from rabbitmq.databus.library.configurator.models.base import RouteOptions
from rabbitmq.databus.library.configurator.models.base import Router
from rabbitmq.databus.library.configurator.models.base import System
from rabbitmq.databus.library.configurator.models.base import SystemProfile
from rabbitmq.databus.library.configurator.models.base import Vhost
from rabbitmq.databus.library.configurator.models.base import ZoneProfile
from rabbitmq.databus.library.configurator.models.consumers.db import DbConsumer
from rabbitmq.databus.library.configurator.models.consumers.external import ExternalConsumer
from rabbitmq.databus.library.configurator.models.consumers.http import HttpConsumer
from rabbitmq.databus.library.configurator.models.consumers.rabbitmq import RabbitmqConsumer
from rabbitmq.databus.library.configurator.models.inter_zone_router_cfg import InterZoneRouterCfg
from rabbitmq.databus.library.configurator.models.rabbitmq_cfg import RabbitmqCfg

class RouteArgs:
    def __init__(self, environment, producer_system, consumer_system):
        self.environment = environment
        self.producer_system = producer_system
        self.consumer_system = consumer_system

class Configurator:
    def __init__(self, meta):
        validate_arg(meta, [Meta, None], "meta")

        if (meta is None):
            self.meta = Meta()
        else:
            self.meta = meta

    def create(self, args, build_dir):
        validate_arg(args, [Args], "args")
        validate_arg(build_dir, [str], "build_dir")

        args.validate({
            "state": {"type": str},
            "credentials": {"type": dict}
        })

        build_dir = normalize_dir_path(build_dir)
        credentials = Map(args.get("credentials"))

        for environment_id, environment in self.meta.environments.items():
            environment_build_dir = "%s/environments/%s" % (build_dir, environment_id)

            self._create_router_cfg(environment_build_dir, environment.router_cfg, credentials)

            for zone_id, zone in environment.zone_profiles.items():
                zone_build_dir = "%s/zones/%s" % (environment_build_dir, zone_id)

                self._create_rabbitmq_cfg(zone_build_dir, zone.rabbitmq_cfg, credentials)
                self._create_frontend_cfg(zone_build_dir, zone.frontend_cfg, credentials)
                self._create_backend_cfg(zone_build_dir, zone.backend_cfg, credentials)

    def _create_router_cfg(self, build_dir, router_cfg, credentials):
        validate_arg(build_dir, [str], "build_dir")
        validate_arg(router_cfg, [InterZoneRouterCfg], "router_cfg")
        validate_arg(credentials, [Map], "credentials")

        for router_id, cfg in router_cfg.serialize(credentials).items():
            cfg_dir = "%s/databus-router/%s" % (build_dir, router_id)
            cfg_file = "%s/databus.yml" % cfg_dir

            pathlib.Path(cfg_dir).mkdir(parents=True, exist_ok=True)
            with open(cfg_file, "w+") as f:
                f.write(json.dumps(obj=cfg, indent=2, sort_keys=True))

    def _create_rabbitmq_cfg(self, build_dir, rabbitmq_cfg, credentials):
        validate_arg(build_dir, [str], "build_dir")
        validate_arg(rabbitmq_cfg, [RabbitmqCfg], "rabbitmq_cfg")
        validate_arg(credentials, [Map], "credentials")

        for peer_id, cfg in rabbitmq_cfg.serialize(credentials).items():
            cfg_dir = "%s/rabbitmq-server/%s" % (build_dir, peer_id)
            cfg_file = "%s/definitions.json" % cfg_dir

            pathlib.Path(cfg_dir).mkdir(parents=True, exist_ok=True)
            with open(cfg_file, "w+") as f:
                f.write(json.dumps(obj=cfg, indent=2, sort_keys=True))

    def _create_frontend_cfg(self, build_dir, frontend_cfg, credentials):
        validate_arg(build_dir, [str], "build_dir")
        validate_arg(frontend_cfg, [FrontendCfg], "frontend_cfg")
        validate_arg(credentials, [Map], "credentials")

        for peer_id, cfg in frontend_cfg.serialize(credentials).items():
            cfg_dir = "%s/databus-frontend/%s" % (build_dir, peer_id)
            cfg_file = "%s/databus.yml" % cfg_dir

            pathlib.Path(cfg_dir).mkdir(parents=True, exist_ok=True)
            with open(cfg_file, "w+") as f:
                f.write(json.dumps(obj=cfg, indent=2, sort_keys=True))

    def _create_backend_cfg(self, build_dir, backend_cfg, credentials):
        validate_arg(build_dir, [str], "build_dir")
        validate_arg(backend_cfg, [BackendCfg], "backend_cfg")
        validate_arg(credentials, [Map], "credentials")

        for peer_id, cfg in backend_cfg.serialize(credentials).items():
            cfg_dir = "%s/databus-backend/%s" % (build_dir, peer_id)
            cfg_file = "%s/databus.yml" % cfg_dir

            pathlib.Path(cfg_dir).mkdir(parents=True, exist_ok=True)
            with open(cfg_file, "w+") as f:
                f.write(json.dumps(obj=cfg, indent=2, sort_keys=True))

    def init(self, args, groups):
        validate_arg(args, [Args], "args")
        validate_arg(groups, [dict], "groups")

        args.validate({
            "state": {"type": str},
            "systems": {"type": dict},
            "nodes": {"type": dict},
            "tags": {"type": dict}
        })

        for system_id, system_args in args.get___("systems").as_dict().items():
            try:
                self._add_system_profile(
                    system_id=system_id,
                    system_args=Args(system_args)
                )
            except Error as e:
                raise Error("systems[%s]" % system_id, e)

        for node_id, node_args in args.get___("nodes").as_dict().items():
            try:
                self._add_node_profile(
                    node_id=node_id,
                    node_args=Args(node_args)
                )
            except Error as e:
                raise Error("nodes[%s]" % node_id, e)

        for tag_id, tag_nodes in args.get___("tags").as_dict().items():
            try:
                self._add_tag(
                    tag_id=tag_id,
                    tag_nodes=tag_nodes
                )
            except Error as e:
                raise Error("tags[%s]" % tag_id, e)

        self.meta.tags.register(
            item_id="all",
            factory=lambda tag_id: list(self.meta.node_profiles.keys())
        )

        for group_id, zone_args in groups.items():
            try:
                self._add_zone_profile(
                    zone_args=zone_args
                )
            except Error as e:
                raise Error("groups[%s]" % group_id, e)

        for environment in self.meta.environments.values():
            for zone_profile in environment.zone_profiles.values():
                for peer in zone_profile.peers.values():
                    for router in peer.routers.values():
                        for router_zone_id in router.zones:
                            try:
                                consumer_zone = environment.get_zone_profile(router_zone_id)

                                is_found = False
                                for consumer_peer in consumer_zone.peers.values():
                                    is_found = router.router_id in consumer_peer.routers
                                    if is_found:
                                        break

                                if (not is_found):
                                    raise Error("router not connected to [%s]" % router_zone_id)
                            except Error as e:
                                raise Error(
                                    msg="zones[%s] -> peers[%s] -> routers[%s]" % (
                                        zone_profile.zone_id, peer.peer_id, router.router_id
                                    ),
                                    details=e
                                )

    def _add_system_profile(self, system_id, system_args):
        validate_arg(system_id, [str], "system_id")
        validate_arg(system_args, [Args], "system_args")

        system_args.validate({
            "title": {"type": str}
        })

        if (system_id == "blackhole"):
            raise Error("blackhole is reserved system name")

        self.meta.system_profiles.register(
            item_id=system_id,
            factory=lambda system_id: SystemProfile(
                system_id=system_id,
                title=system_args.get___("title").as_text()
            )
        )

    def _add_node_profile(self, node_id, node_args):
        validate_arg(node_id, [str], "node_id")
        validate_arg(node_args, [Args], "node_args")

        node_args.validate({
            "title": {"type": str}
        })

        self.meta.node_profiles.register(
            item_id=node_id,
            factory=lambda node_id: NodeProfile(
                node_id=node_id,
                title=node_args.get___("title").as_text()
            )
        )

    def _add_tag(self, tag_id, tag_nodes):
        validate_arg(tag_id, [str], "tag_id")
        validate_arg(tag_nodes, [dict], "tag_nodes")

        self.meta.tags.register(
            item_id=tag_id,
            factory=lambda tag_id: sorted(list(map(
                lambda pair: self._create_tag_node(pair[0], pair[1]),
                tag_nodes.items()
            )))
        )

    def _create_tag_node(self, node_id, node_args):
        if (node_args is not None):
            raise Error("invalid node [%s] definition" % node_id)

        return self.meta.get_node_profile(node_id).node_id

    def _add_zone_profile(self, zone_args):
        validate_arg(zone_args, [Args], "zone_args")

        zone_args.validate({
            "environment": {"type": str},
            "zone": {"type": str},
            "advertise_uri": {"type": str},
            "username": {"type": str},
            "vhosts": {"type": dict},
            "peers": {"type": dict}
        })

        environment = self.meta.environments.register(
            item_id=zone_args.get___("environment").as_text(),
            unique=False,
            factory=lambda environment_id: Environment(
                environment_id=environment_id
            )
        )

        zone_profile = self.meta.zone_profiles.register(
            item_id=zone_args.get___("zone").as_text(),
            factory=lambda zone_id: ZoneProfile(
                zone_id=zone_id,
                environment_id=environment.environment_id,
                advertise_uri=zone_args.get___("advertise_uri").as_text(),
                username=zone_args.get___("username").as_text()
            )
        )

        environment.zone_profiles.register(
            item_id=zone_profile.zone_id,
            factory=lambda zone_id: zone_profile
        )

        for peer_id, peer_args in zone_args.get___("peers").as_dict().items():
            try:
                peer_args = Args(peer_args)
                peer_args.validate({
                    "uri": {"type": str},
                    "routers": {"type": dict, "optional": True}
                })

                peer = zone_profile.peers.register(
                    item_id=peer_id,
                    factory=lambda peer_id: Peer(
                        peer_id=peer_id,
                        uri=peer_args.get___("uri").as_text(),
                        username=zone_profile.username
                    )
                )

                self.meta.peer_set.register(peer.peer_id)
                self.meta.peer_uri_set.register(peer.uri)

                for router_id, router_args in peer_args.get___("routers", {}).as_dict().items():
                    try:
                        router_args = Args(router_args)
                        router_args.validate({
                            "zones": {"type": dict, "optional": True}
                        })

                        router = peer.routers.register(
                            item_id=router_id,
                            factory=lambda router_id: Router(router_id)
                        )

                        router_zones = router_args.get___("zones", {}).as_dict()
                        for router_zone_id, router_zone in router_zones.items():
                            try:
                                validate_arg(router_zone, [None], "router_zone")

                                router.zones.register(router_zone_id)
                            except Error as e:
                                raise Error("zones[%s]" % router_zone_id, e)
                    except Error as e:
                        raise Error("routers[%s]" % router_id, e)
            except Error as e:
                raise Error("peers[%s]" % peer_id, e)

        if (len(zone_profile.peers) == 0):
            raise Error("peers not defined")

        for vhost_id, vhost_args in zone_args.get___("vhosts").as_dict().items():
            try:
                self._add_vhost(
                    environment=environment,
                    zone_profile=zone_profile,
                    vhost_id=vhost_id,
                    vhost_args=Args(vhost_args)
                )
            except Error as e:
                raise Error("vhosts[%s]" % vhost_id, e)

    def _add_vhost(self, environment, zone_profile, vhost_id, vhost_args):
        validate_arg(environment, [Environment], "environment")
        validate_arg(zone_profile, [ZoneProfile], "zone_profile")
        validate_arg(vhost_id, [str], "vhost_id")
        validate_arg(vhost_args, [Args], "vhost_args")

        vhost_args.validate({
            "nodes": {"type": dict},
            "producers": {"type": dict, "optional": True},
            "consumers": {"type": dict, "optional": True},
            "frontend": {"type": dict, "optional": True}
        })

        vhost = Vhost(
            vhost_id=vhost_id,
            zone=zone_profile
        )

        environment.vhosts.register(
            item_id=vhost.vhost_id,
            factory=lambda vhost_id: vhost
        )

        zone_profile.vhosts.register(
            item_id=vhost.vhost_id,
            factory=lambda vhost_id: vhost
        )

        zone_profile.rabbitmq_cfg.add_user(zone_profile.username)
        zone_profile.rabbitmq_cfg.add_vhost(vhost.vhost_id)
        zone_profile.rabbitmq_cfg.add_permission(vhost.vhost_id, zone_profile.username)

        zone_profile.rabbitmq_cfg.add_policy(
            vhost=vhost.vhost_id,
            name="default",
            pattern="q_.*",
            lazy=True
        )

        try:
            frontend_arg = vhost_args.get___("frontend")
            if frontend_arg.defined():
                self._add_frontend(
                    zone_profile=zone_profile,
                    vhost=vhost,
                    frontend_args=Args(frontend_arg.as_dict())
                )
        except Error as e:
            raise Error("frontend", e)

        for producer_id, producer_args in vhost_args.get___("producers", {}).as_dict().items():
            try:
                validate_arg(producer_args, [None], "producer_args")

                vhost.producers.register(self.meta.get_system_profile(producer_id).system_id)
            except Error as e:
                raise Error("producers[%s]" % producer_id, e)

        for consumer_id, consumer_args in vhost_args.get___("consumers", {}).as_dict().items():
            try:
                self._add_consumer(
                    vhost=vhost,
                    consumer_id=self.meta.get_system_profile(consumer_id).system_id,
                    consumer_args=Args(consumer_args)
                )
            except Error as e:
                raise Error("consumers[%s]" % consumer_id, e)

        for node_id, node_args in vhost_args.get___("nodes", {}).as_dict().items():
            try:
                validate_arg(node_args, [None], "node_args")

                self._add_node(
                    environment=environment,
                    vhost=vhost,
                    node_id=self.meta.get_node_profile(node_id).node_id
                )
            except Error as e:
                raise Error("nodes[%s]" % node_id, e)

    def _add_frontend(self, zone_profile, vhost, frontend_args):
        validate_arg(zone_profile, [ZoneProfile], "zone_profile")
        validate_arg(vhost, [Vhost], "vhost")
        validate_arg(frontend_args, [Args], "frontend_args")

        frontend_args.validate({
            "path": {"type": str}
        })

        path = frontend_args.get___("path").as_text()

        if (path is None):
            return

        zone_profile.frontend_cfg.paths.register(
            item_id=path,
            factory=lambda path: FrontendPath(
                name="frontend->rmq:%s:%s" % (vhost.zone_id, vhost.vhost_id),
                vhost=vhost
            )
        )

    def _add_consumer(self, vhost, consumer_id, consumer_args):
        validate_arg(vhost, [Vhost], "vhost")
        validate_arg(consumer_id, [str], "consumer_id")
        validate_arg(consumer_args, [Args], "consumer_args")

        consumer_type = consumer_args.get___("type").as_text()
        if (not isinstance(consumer_type, str)):
            raise Error("[type] is required")

        if (consumer_type == ExternalConsumer.type):
            consumer_class = ExternalConsumer
        elif (consumer_type == RabbitmqConsumer.type):
            consumer_class = RabbitmqConsumer
        elif (consumer_type == DbConsumer.type):
            consumer_class = DbConsumer
        elif (consumer_type == HttpConsumer.type):
            consumer_class = HttpConsumer
        else:
            raise Error("unsupported consumer type [%s]" % consumer_type)

        vhost.consumers.register(
            item_id=consumer_id,
            factory=lambda consumer_id: consumer_class(
                vhost_id=vhost.vhost_id,
                system_id=consumer_id,
                args=consumer_args
            )
        )

    def _add_node(self, environment, vhost, node_id):
        validate_arg(environment, [Environment], "environment")
        validate_arg(vhost, [Vhost], "vhost")
        validate_arg(node_id, [str], "node_id")

        node = environment.nodes.register(
            item_id=node_id,
            unique=False,
            factory=lambda node_id: Node(node_id)
        )

        for system_id in vhost.producers:
            try:
                node.producer_systems.register(
                    item_id=system_id,
                    factory=lambda system_id: System(
                        system_id=system_id,
                        vhost=vhost
                    )
                )
            except Error as e:
                raise Error("producers[%s]" % system_id, e)

        for system_id in vhost.consumers.keys():
            try:
                node.consumer_systems.register(
                    item_id=system_id,
                    factory=lambda system_id: System(
                        system_id=system_id,
                        vhost=vhost
                    )
                )
            except Error as e:
                raise Error("consumers[%s]" % system_id, e)

    def add_entity(self, args):
        validate_arg(args, [Args], "args")

        args.validate({
            "state": {"type": str},
            "system": {"type": str},
            "name": {"type": str},
            "title": {"type": str, "optional": True},
            "wiki": {"type": str, "optional": True},
            "refs": {"type": list, "optional": True}
        })

        system_profile = self.meta.get_system_profile(args.get___("system").as_text())

        title_arg = args.get___("title")
        wiki_arg = args.get___("wiki")

        entity = Entity(
            name=args.get___("name").as_text(),
            system_id=system_profile.system_id,
            title=title_arg.as_text() if title_arg.defined() else None,
            wiki=wiki_arg.as_text() if wiki_arg.defined() else None,
            refs=args.get___("refs", []).as_list()
        )

        self.meta.entities.register(
            item_id=entity.entity_id,
            factory=lambda entity_id: entity
        )

        system_profile.entities.register(
            item_id=entity.entity_id,
            factory=lambda entity_id: entity
        )

    def add_route(self, args):
        validate_arg(args, [Args], "args")

        args.validate({
            "state": {"type": str},
            "producer": {"type": dict},
            "consumer": {"type": dict}
        })

        producer_args = Args(args.get___("producer").as_dict())
        producer_args.validate({
            "system": {"type": str},
            "name": {"type": str},
            "node": {"type": str},
            "routing_key": {"type": str, "optional": True}
        })

        consumer_args = Args(args.get___("consumer").as_dict())
        consumer_args.validate({
            "system": {"type": str},
            "node": {"type": str},
            "queue": {"type": str, "optional": True}
        })

        entity = self.meta.get_entity(
            system_id=producer_args.get___("system").as_text(),
            name=producer_args.get___("name").as_text()
        )

        producer_node_id = producer_args.get___("node").as_text()
        try:
            producer_node_profile = self.meta.get_node_profile(producer_node_id)
        except Error as e:
            raise Error("producer profile [%s] is not defined" % producer_node_id)

        consumer_node_id = consumer_args.get___("node").as_text()
        try:
            consumer_node_profile = self.meta.get_node_profile(consumer_node_id)
        except Error as e:
            raise Error("consumer profile [%s] is not defined" % consumer_node_id)

        consumer_system_id = consumer_args.get___("system").as_text()
        try:
            consumer_system_profile = self.meta.get_system_profile(consumer_system_id)
        except Error as e:
            raise Error("consumer system profile [%s] is not defined" % consumer_system_id)

        try:
            routes = []

            for environment in self.meta.environments.values():
                producer_node = environment.nodes.get(producer_node_profile.node_id)
                if (producer_node is None):
                    continue

                producer_system = producer_node.producer_systems.get(entity.system_id)
                if (producer_system is None):
                    continue

                consumer_node = environment.nodes.get(consumer_node_profile.node_id)
                if (consumer_node is None):
                    continue

                consumer_system = consumer_node.consumer_systems.get(
                    consumer_system_profile.system_id
                )
                if (consumer_system is None):
                    continue

                routes.append(RouteArgs(
                    environment=environment,
                    producer_system=producer_system,
                    consumer_system=consumer_system
                ))

            if (len(routes) > 0):
                product, product_kind = self._add_product(
                    entity=entity,
                    producer_node=producer_node_profile,
                    consumer_node=consumer_node_profile,
                    system=consumer_system_profile,
                    product_id=producer_args.get___("routing_key", entity.name).as_text(),
                    kind_id=consumer_args.get___("queue", entity.name).as_text()
                )

                for route in routes:
                    try:
                        self._add_route(RouteOptions(
                            environment=route.environment,
                            entity=entity,
                            product=product,
                            product_kind=product_kind,
                            producer_system=route.producer_system,
                            consumer_system=route.consumer_system
                        ))
                    except Error as e:
                        raise Error("environments[%s]" % route.environment.environment_id, e)
        except Error as e:
            raise Error(
                msg="routes[%s:%s->%s]" % (
                    producer_node_profile.node_id, entity.entity_id, consumer_node_profile.node_id
                ),
                details=e
            )

    def _add_product(self, entity, producer_node, consumer_node, system, product_id, kind_id):
        validate_arg(entity, [Entity], "entity")
        validate_arg(producer_node, [NodeProfile], "producer_node")
        validate_arg(consumer_node, [NodeProfile], "consumer_node")
        validate_arg(system, [SystemProfile], "system")
        validate_arg(product_id, [str], "product_id")
        validate_arg(kind_id, [str], "kind_id")

        producer_node = entity.producer_nodes.register(
            item_id=producer_node.node_id,
            unique=False,
            factory=lambda node_id: ProducerNode(node_id)
        )

        product = producer_node.products.register(
            item_id=product_id,
            unique=False,
            factory=lambda product_id: Product(
                entity=entity,
                product_id=product_id
            )
        )

        consumer_node = product.consumer_nodes.register(
            item_id=consumer_node.node_id,
            unique=False,
            factory=lambda node_id: ConsumerNode(node_id)
        )

        consumer_system = consumer_node.consumer_systems.register(
            item_id=system.system_id,
            unique=False,
            factory=lambda system_id: ConsumerSystem(system_id)
        )

        product_kind = consumer_system.product_kinds.register(
            item_id=kind_id,
            factory=lambda kind_id: ProductKind(
                kind_id=kind_id,
                consumer_system=consumer_system
            )
        )

        return (product, product_kind)

    def _add_route(self, route_options):
        validate_arg(route_options, [RouteOptions], "route_options")

        environment = route_options.environment

        consumer_zone = environment.get_zone_profile(route_options.consumer_system.zone_id)
        consumer_vhost = environment.get_vhost(route_options.consumer_system.vhost_id)

        self._add_rabbitmq_queue(
            cfg=consumer_zone.rabbitmq_cfg,
            system=route_options.consumer_system.system_id,
            vhost=route_options.consumer_system.vhost_id,
            exchange_name=route_options.entity.exchange_name,
            routing_key=route_options.product.routing_key,
            queue_name=route_options.product_kind.queue_name
        )

        consumer = consumer_vhost.consumers.get(route_options.consumer_system.system_id)
        try:
            consumer.create(consumer_zone.backend_cfg, route_options)
        except Error as e:
            zone_id = route_options.consumer_system.zone_id
            vhost_id = route_options.consumer_system.vhost_id
            system_id = route_options.consumer_system.system_id

            raise Error(
                msg="zones[%s] -> vhosts[%s] -> consumers[%s]" % (zone_id, vhost_id, system_id),
                details=e
            )

        if (route_options.producer_system.vhost_id != route_options.consumer_system.vhost_id):
            producer_zone = environment.get_zone_profile(route_options.producer_system.zone_id)
            producer_vhost = environment.get_vhost(route_options.producer_system.vhost_id)

            rmq_queue_name = "q_rmq_forward.%s.%s" % (
                consumer_zone.zone_id, route_options.consumer_system.vhost_id
            )

            self._add_rabbitmq_queue(
                cfg=producer_zone.rabbitmq_cfg,
                system="rmq",
                vhost=route_options.producer_system.vhost_id,
                exchange_name=route_options.entity.exchange_name,
                routing_key=route_options.product.routing_key,
                queue_name=rmq_queue_name
            )

            if (route_options.producer_system.zone_id == route_options.consumer_system.zone_id):
                producer_zone.router_cfg.add_route(
                    producer_vhost=producer_vhost,
                    queue_name=rmq_queue_name,
                    consumer_vhost=consumer_vhost
                )
            else:
                is_found = False

                for producer_peer in producer_zone.peers.values():
                    for router_id, router in producer_peer.routers.items():
                        if (consumer_zone.zone_id not in router.zones):
                            continue

                        for consumer_peer in consumer_zone.peers.values():
                            if (router_id not in consumer_peer.routers):
                                continue

                            is_found = True

                            environment.router_cfg.add_route(
                                router_id=router_id,
                                producer_peer=producer_peer,
                                producer_vhost=producer_vhost,
                                queue_name=rmq_queue_name,
                                consumer_peer=consumer_peer,
                                consumer_vhost=consumer_vhost
                            )

                if (not is_found):
                    raise Error(
                        "router [%s->%s] not found" % (producer_zone.zone_id, consumer_zone.zone_id)
                    )

    def _add_rabbitmq_queue(self, cfg, system, vhost, exchange_name, routing_key, queue_name):
        validate_arg(cfg, [RabbitmqCfg], "cfg")
        validate_arg(system, [str], "system")
        validate_arg(vhost, [str], "vhost")
        validate_arg(exchange_name, [str], "exchange_name")
        validate_arg(routing_key, [str], "routing_key")
        validate_arg(queue_name, [str], "queue_name")

        retry_exchange = "ex_%s_retry" % system
        retry_queue = "q_%s_retry" % system

        cfg.add_policy(
            vhost=vhost,
            name="%s_retry" % system,
            pattern=retry_queue,
            priority=1,
            lazy=True,
            ttl=3600000
        )

        cfg.add_exchange(
            vhost=vhost,
            name=retry_exchange,
            type="fanout"
        )

        cfg.add_queue(
            vhost=vhost,
            name=retry_queue,
            dlx="",
            dlk=None
        )

        cfg.add_binding(
            vhost=vhost,
            source=retry_exchange,
            destination=retry_queue,
            routing_key=""
        )

        cfg.add_exchange(
            vhost=vhost,
            name=exchange_name,
            type="direct"
        )

        cfg.add_queue(
            vhost=vhost,
            name=queue_name,
            dlx=retry_exchange,
            dlk=queue_name
        )

        cfg.add_binding(
            vhost=vhost,
            source=exchange_name,
            destination=queue_name,
            routing_key=routing_key
        )
