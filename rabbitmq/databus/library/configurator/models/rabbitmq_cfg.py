import base64
import hashlib
import json
import urllib.parse

from python.collections import Map
from python.utils.validators import validate_arg

class RabbitmqCfg:
    def __init__(self, peers):
        validate_arg(peers, [Map], "peers")

        self.peers = peers
        self.users = Map()
        self.permissions = Map()
        self.policies = Map()
        self.vhosts = Map()
        self.exchanges = Map()
        self.queues = Map()
        self.bindings = Map()

    def serialize(self, credentials, salt=31415926):
        validate_arg(credentials, [Map], "credentials")
        validate_arg(salt, [int], "salt")

        users = [self.serialize_user(u, credentials, salt) for u in self.users.sorted_values()]
        permissions = self.permissions.sorted_values()
        policies = self.policies.sorted_values()
        vhosts = self.vhosts.sorted_values()
        exchanges = self.exchanges.sorted_values()
        queues = self.queues.sorted_values()
        bindings = self.bindings.sorted_values()

        sorted_vhost_names = list(map(lambda vhost: vhost.get("name"), vhosts))
        sorted_peers = self.peers.sorted_values()

        return dict(map(
            lambda peer_id: (peer_id, {
                "users": users,
                "permissions": permissions,
                "policies": policies,
                "vhosts": vhosts,
                "exchanges": exchanges,
                "queues": queues,
                "bindings": bindings,
                "parameters": self.serialize_parameters(
                    sorted_vhost_names=sorted_vhost_names,
                    sorted_peers=sorted_peers,
                    peer_id=peer_id,
                    credentials=credentials
                )
            }),
            self.peers.keys()
        ))

    def serialize_user(self, user, credentials, salt):
        password = credentials.find(user.get("name"), "password")

        salt_as_bytes = salt.to_bytes(4, byteorder="big")
        password_as_bytes = bytes(password, "utf-8")
        password_hash = base64.b64encode(
            salt_as_bytes + hashlib.sha256(salt_as_bytes + password_as_bytes
        ).digest()).decode("utf-8")

        return {
            **user,
            **{
                "hashing_algorithm": "rabbit_password_hashing_sha256",
                "password_hash": password_hash
            }
        }

    def serialize_parameters(self, sorted_vhost_names, sorted_peers, peer_id, credentials):
        parameters = []

        for peer in sorted_peers:
            if (peer.peer_id == peer_id):
                continue

            password = credentials.find(peer.username, "password")

            parsed = urllib.parse.urlparse(peer.uri)
            auth = "%s:%s" % (urllib.parse.quote(peer.username), urllib.parse.quote(password))
            scheme = parsed.scheme
            hostname = parsed.hostname
            port = ":%d" % parsed.port if parsed.port is not None else ""

            vhost_names = sorted(list(map(lambda vhost: vhost.get("name"), self.vhosts.values())))
            for vhost_name in vhost_names:
                parameters.append({
                    "value": {
                        "ack-mode": "on-confirm",
                        "prefetch-count": 10,
                        "reconnect-delay": 60,
                        "trust-user-id": False,
                        "uri": "%s://%s@%s%s/%s" % (
                            scheme, auth, hostname, port, urllib.parse.quote(vhost_name)
                        )
                    },
                    "vhost": vhost_name,
                    "component": "federation-upstream",
                    "name": peer.peer_id
                })

        return parameters

    def add_user(self, username):
        validate_arg(username, [str], "username")

        self.users.register(
            item_id=".user[%s]" % (json.dumps(username)),
            unique=False,
            factory=lambda user_id: {
                "name": username,
                "tags": ""
            }
        )

    def add_vhost(self, name):
        validate_arg(name, [str], "name")

        self.vhosts.register(
            item_id=".vhost[%s]" % json.dumps(name),
            factory=lambda vhost_id: {
                "name": name
            }
        )

    def add_permission(self, vhost, username):
        validate_arg(vhost, [str], "vhost")
        validate_arg(username, [str], "username")

        self.permissions.register(
            item_id=".vhost[%s].user[%s]" % (json.dumps(vhost), json.dumps(username)),
            factory=lambda permission_id: {
                "configure": ".*",
                "read": ".*",
                "write": ".*",
                "user": username,
                "vhost": vhost
            }
        )

    def add_policy(self, vhost, name, pattern, priority=0, ttl=None, lazy=False):
        validate_arg(vhost, [str], "vhost")
        validate_arg(name, [str], "name")
        validate_arg(pattern, [str], "pattern")
        validate_arg(priority, [int], "priority")
        validate_arg(ttl, [int, None], "ttl")
        validate_arg(lazy, [bool], "lazy")

        policy = {
            "vhost": vhost,
            "name": name,
            "pattern": pattern,
            "apply-to": "queues",
            "definition": {
                "federation-upstream-set": "all"
            },
            "priority": priority
        }

        if (ttl is not None):
            policy["definition"]["message-ttl"] = ttl

        if (lazy):
            policy["definition"]["queue-mode"] = "lazy"

        self.policies.register(
            item_id=".vhost[%s].policy[%s]" % (json.dumps(vhost), json.dumps(name)),
            unique=False,
            factory=lambda policy_id: policy
        )

    def add_exchange(self, vhost, name, type):
        validate_arg(vhost, [str], "vhost")
        validate_arg(name, [str], "name")
        validate_arg(type, [str], "type")

        self.exchanges.register(
            item_id=".vhost[%s].exchange[%s]" % (json.dumps(vhost), json.dumps(name)),
            unique=False,
            factory=lambda exchange_id: {
                "vhost": vhost,
                "name": name,
                "type": type,
                "durable": True,
                "auto_delete": False,
                "internal": False,
                "arguments": {
                }
            }
        )

    def add_queue(self, vhost, name, dlx, dlk):
        validate_arg(vhost, [str], "vhost")
        validate_arg(name, [str], "name")
        validate_arg(dlx, [str], "dlx")
        validate_arg(dlk, [str, None], "dlk")

        queue = {
            "vhost": vhost,
            "name": name,
            "durable": True,
            "auto_delete": False,
            "arguments": {
            }
        }

        if (dlx is not None):
            queue.get("arguments")["x-dead-letter-exchange"] = dlx

        if (dlk is not None):
            queue.get("arguments")["x-dead-letter-routing-key"] = dlk

        self.queues.register(
            item_id=".vhost[%s].queue[%s]" % (json.dumps(vhost), json.dumps(name)),
            unique=False,
            factory=lambda queue_id: queue
        )

    def add_binding(self, vhost, source, destination, routing_key):
        validate_arg(vhost, [str], "vhost")
        validate_arg(source, [str], "source")
        validate_arg(destination, [str], "destination")
        validate_arg(routing_key, [str], "routing_key")

        binding_id = ".vhost[%s].source[%s].destination[%s].routing_key[%s]" % (
            json.dumps(vhost), json.dumps(destination), json.dumps(source), json.dumps(routing_key)
        )

        self.bindings.register(
            item_id=binding_id,
            unique=False,
            factory=lambda binding_id: {
                "vhost": vhost,
                "source": source,
                "destination": destination,
                "destination_type": "queue",
                "routing_key": routing_key,
                "arguments": {
                }
            }
        )
