import urllib.parse

from python.collections import Set
from python.collections import Map
from python.error import Error
from python.utils.validators import validate_arg
from python.utils.validators import validate_list_arg

from rabbitmq.databus.library.configurator.models.rabbitmq_cfg import RabbitmqCfg
from rabbitmq.databus.library.configurator.models.zone_router_cfg import ZoneRouterCfg
from rabbitmq.databus.library.configurator.models.inter_zone_router_cfg import InterZoneRouterCfg

class Meta:
    def __init__(self):
        self.system_profiles = Map()
        self.node_profiles = Map()
        self.zone_profiles = Map()
        self.environments = Map()
        self.entities = Map()

        self.tags = Map()

        self.peer_set = Set()
        self.peer_uri_set = Set()

    def get_system_profile(self, system_id):
        return self.system_profiles.find(system_id, "system")

    def get_node_profile(self, node_id):
        return self.node_profiles.find(node_id, "node")

    def get_entity(self, system_id, name):
        validate_arg(system_id, [str], "system_id")
        validate_arg(name, [str], "name")

        return self.entities.find("%s_%s" % (system_id, name), "entity")

class SystemProfile:
    def __init__(self, system_id, title):
        validate_arg(system_id, [str], "system_id")
        validate_arg(title, [str], "title")

        self.system_id = system_id
        self.title = title
        self.entities = Map() # Для документации

class NodeProfile:
    def __init__(self, node_id, title):
        validate_arg(node_id, [str], "node_id")
        validate_arg(title, [str], "title")

        self.node_id = node_id
        self.title = title

class Environment:
    def __init__(self, environment_id):
        validate_arg(environment_id, [str], "environment_id")

        self.environment_id = environment_id
        self.zone_profiles = Map()
        self.vhosts = Map()
        self.nodes = Map()
        self.router_cfg = InterZoneRouterCfg()

    def get_zone_profile(self, zone_id):
        return self.zone_profiles.find(zone_id, "zone")

    def get_vhost(self, vhost_id):
        return self.vhosts.find(vhost_id, "vhost")

class ZoneProfile:
    def __init__(self, zone_id, environment_id, advertise_uri, username):
        validate_arg(zone_id, [str], "zone_id")
        validate_arg(environment_id, [str], "environment_id")
        validate_arg(advertise_uri, [str], "advertise_uri")
        validate_arg(username, [str], "username")

        self.zone_id = zone_id
        self.environment_id = environment_id
        self.advertise_uri = advertise_uri # Для документации
        self.username = username
        self.peers = Map()
        self.vhosts = Map()
        self.rabbitmq_cfg = RabbitmqCfg(self.peers)
        self.frontend_cfg = FrontendCfg(self.peers)
        self.backend_cfg = BackendCfg(self.peers)
        self.router_cfg = ZoneRouterCfg(self.backend_cfg)

class Vhost:
    def __init__(self, vhost_id, zone):
        validate_arg(vhost_id, [str], "vhost_id")
        validate_arg(zone, [ZoneProfile], "zone")

        self.vhost_id = vhost_id
        self.zone_id = zone.zone_id
        self.environment_id = zone.environment_id
        self.producers = Set()
        self.consumers = Map()

class FrontendCfg:
    def __init__(self, peers):
        validate_arg(peers, [Map], "peers")

        self.peers = peers
        self.paths = Map()

    def serialize(self, credentials):
        return dict(map(
            lambda pair: (pair[0], self.serialize_peer(pair[1], credentials)),
            self.peers.items()
        ))

    def serialize_peer(self, peer, credentials):
        validate_arg(peer, [Peer], "peer")
        validate_arg(credentials, [Map], "credentials")

        paths = dict(map(
            lambda pair: (pair[0], pair[1].serialize(peer, credentials)),
            self.paths.items()
        ))

        return {
            "web": {
                "paths": paths
            }
        }

class FrontendPath:
    def __init__(self, name, vhost):
        validate_arg(name, [str], "name")
        validate_arg(vhost, [Vhost], "vhost")

        self.name = name
        self.vhost = vhost

    def serialize(self, peer, credentials):
        validate_arg(peer, [Peer], "peer")
        validate_arg(credentials, [Map], "credentials")

        password = credentials.get(peer.username)

        if (not isinstance(password, str)):
            raise Error("password is not defined [%s]" % peer.username)

        return {
            "name": self.name,
            "uri": peer.uri + "/" + self.vhost.vhost_id,
            "auth.username": peer.username,
            "auth.password": password
        }

class BackendCfg():
    def __init__(self, peers):
        validate_arg(peers, [Map], "peers")

        self.peers = peers
        self.backends = Map()

    def serialize(self, credentials):
        validate_arg(credentials, [Map], "credentials")

        return dict(map(
            lambda pair: (pair[0], self.serialize_peer(pair[1], credentials)),
            self.peers.items()
        ))

    def serialize_peer(self, peer, credentials):
        validate_arg(peer, [Peer], "peer")
        validate_arg(credentials, [Map], "credentials")

        return {
            "workers": dict([
                (pair[0], pair[1].serialize(peer, credentials)) for pair in self.backends.items()
            ])
        }

class Peer:
    def __init__(self, peer_id, uri, username):
        validate_arg(peer_id, [str], "peer_id")
        validate_arg(uri, [str], "uri")
        validate_arg(username, [str], "username")

        self.peer_id = peer_id
        self.uri = uri
        self.username = username
        self.routers = Map()

    def create_connection_uri(self, vhost_id):
        validate_arg(vhost_id, [str], "vhost_id")

        parsed = urllib.parse.urlparse(self.uri)

        return "%s://%s/%s" % (parsed.scheme, parsed.netloc, urllib.parse.quote(vhost_id))

class Router:
    def __init__(self, router_id):
        validate_arg(router_id, [str], "router_id")

        self.router_id = router_id
        self.zones = Set()

class Node:
    def __init__(self, node_id):
        validate_arg(node_id, [str], "node_id")

        self.node_id = node_id
        self.producer_systems = Map()
        self.consumer_systems = Map()

class System:
    def __init__(self, system_id, vhost):
        validate_arg(system_id, [str], "system_id")
        validate_arg(vhost, [Vhost], "vhost")

        self.system_id = system_id
        self.vhost_id = vhost.vhost_id
        self.zone_id = vhost.zone_id
        self.environment_id = vhost.environment_id

class RouteOptions:
    def __init__(
        self, environment, entity, product, product_kind, producer_system, consumer_system
    ):
        validate_arg(environment, [Environment], "environment")
        validate_arg(entity, [Entity], "entity")
        validate_arg(product, [Product], "product")
        validate_arg(product_kind, [ProductKind], "product_kind")
        validate_arg(producer_system, [System], "producer_system")
        validate_arg(consumer_system, [System], "consumer_system")

        self.environment = environment
        self.entity = entity
        self.product = product
        self.product_kind = product_kind
        self.producer_system = producer_system
        self.consumer_system = consumer_system

class Entity:
    def __init__(self, name, system_id, title, wiki, refs):
        validate_arg(name, [str], "name")
        validate_arg(system_id, [str], "system_id")
        validate_arg(title, [str, None], "title")
        validate_arg(wiki, [str, None], "wiki")
        validate_list_arg(refs, [str], "refs")

        self.entity_id = "%s_%s" % (system_id, name)
        self.name = name
        self.system_id = system_id
        self.title = title
        self.wiki = wiki
        self.refs = refs
        self.exchange_name = "ex_%s" % system_id
        self.producer_nodes = Map()

class ProducerNode:
    def __init__(self, node_id):
        validate_arg(node_id, [str], "node_id")

        self.node_id = node_id
        self.products = Map()

class Product:
    def __init__(self, entity, product_id):
        validate_arg(entity, [Entity], "entity")
        validate_arg(product_id, [str], "product_id")

        self.product_id = product_id
        self.routing_key = "rk_%s_%s" % (entity.system_id, product_id)

        self.consumer_nodes = Map()

class ConsumerNode:
    def __init__(self, node_id):
        validate_arg(node_id, [str], "node_id")

        self.node_id = node_id
        self.consumer_systems = Map()

class ConsumerSystem:
    def __init__(self, system_id):
        validate_arg(system_id, [str], "system_id")

        self.system_id = system_id
        self.product_kinds = Map()

class ProductKind:
    def __init__(self, kind_id, consumer_system):
        validate_arg(kind_id, [str], "kind_id")
        validate_arg(consumer_system, [ConsumerSystem], "consumer_system")

        self.kind_id = kind_id
        self.queue_name = "q_%s_%s" % (consumer_system.system_id, kind_id)

        self.backends = Map()
