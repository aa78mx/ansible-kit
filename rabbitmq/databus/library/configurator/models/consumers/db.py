from python.collections import Map
from python.utils.args import Args
from python.utils.validators import validate_arg

from rabbitmq.databus.library.configurator.models.backends.db_backend import DbBackend
from rabbitmq.databus.library.configurator.models.base import BackendCfg
from rabbitmq.databus.library.configurator.models.base import RouteOptions
from rabbitmq.databus.library.configurator.models.consumers.consumer import Consumer

class DbConsumer:
    type = "db"

    def __init__(self, vhost_id, system_id, args):
        validate_arg(vhost_id, [str], "vhost_id")
        validate_arg(system_id, [str], "system_id")
        validate_arg(args, [Args], "args")

        args.validate({
            "type": {"type": str},
            "driver": {"type": str},
            "uri": {"type": str},
            "username": {"type": str},
            "product_kinds": {"type": dict},
            "capacity": {"type": int, "optional": True}
        })

        self.driver = args.get___("driver").as_text()
        self.uri = args.get___("uri").as_text()
        self.username = args.get___("username").as_text()
        self.capacity = Consumer.read_capacity(args)

        product_kinds = Map()
        disabled = {}
        for kind_id, backend_options_args in args.get___("product_kinds").as_dict().items():
            backend_options_args = Args(backend_options_args)

            backend_options_args.validate({
                "enabled": {"type": bool, "optional": True},
                "method": {"type": str, "optional": True}
            })

            if backend_options_args.get___("enabled", True).as_bool():
                backend_options_args.validate({
                    "enabled": {"type": bool, "optional": True},
                    "method": {"type": str}
                })
            else:
                disabled[kind_id] = None

            backend_method_arg = backend_options_args.get___("method")
            product_kinds.register(
                item_id=kind_id,
                factory=lambda kind_id: DbBackendOptions(
                    method=backend_method_arg.as_text() if backend_method_arg.defined() else None
                )
            )

        self.proxy = Consumer(
            vhost_id=vhost_id,
            system_id=system_id,
            default_options=None,
            product_kinds=product_kinds,
            origin=self,
            disabled=disabled
        )

    def create(self, backend_cfg, route_options):
        self.proxy.create(backend_cfg, route_options)

    def create_backend(self, backend_cfg, route_options, options):
        validate_arg(backend_cfg, [BackendCfg], "backend_cfg")
        validate_arg(route_options, [RouteOptions], "route_options")
        validate_arg(options, [DbBackendOptions], "options")

        consumer_system = route_options.consumer_system

        backend = backend_cfg.backends.register(
            item_id="rmq:%s:%s->%s:%s" % (
                consumer_system.zone_id, consumer_system.vhost_id, consumer_system.system_id, consumer_system.vhost_id
            ),
            unique=False,
            factory=lambda backend_id: DbBackend(
                vhost_id=route_options.consumer_system.vhost_id,
                retry_prefix=route_options.consumer_system.system_id,
                capacity=self.capacity,
                driver=self.driver,
                uri=self.uri,
                username=self.username
            )
        )

        backend.add_queue(
            queue_name=route_options.product_kind.queue_name,
            method=options.method
        )

class DbBackendOptions:
    def __init__(self, method):
        validate_arg(method, [str, None], "method")

        self.method = method
