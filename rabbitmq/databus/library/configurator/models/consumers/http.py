import urllib.parse

from python.collections import Map
from python.error import Error
from python.utils.args import Args
from python.utils.validators import validate_arg

from rabbitmq.databus.library.configurator.models.backends.http_backend import HttpBackend
from rabbitmq.databus.library.configurator.models.backends.http_backend import NoneAuth
from rabbitmq.databus.library.configurator.models.backends.http_backend import NtlmAuth
from rabbitmq.databus.library.configurator.models.base import BackendCfg
from rabbitmq.databus.library.configurator.models.base import RouteOptions
from rabbitmq.databus.library.configurator.models.consumers.consumer import Consumer

class HttpConsumer:
    type = "http"

    def __init__(self, vhost_id, system_id, args):
        validate_arg(vhost_id, [str], "vhost_id")
        validate_arg(system_id, [str], "system_id")
        validate_arg(args, [Args], "args")

        args.validate({
            "type": {"type": str},
            "scheme": {"type": str},
            "target": {"type": str},
            "envelope": {"type": str},
            "auth": {"type": dict, "optional": True},
            "default": {"type": dict, "optional": True},
            "product_kinds": {"type": dict, "optional": True},
            "disabled": {"type": dict, "optional": True},
            "capacity": {"type": int, "optional": True}
        })

        self.scheme = self.read_scheme(args)
        self.target = self.read_target(args)

        auth_arg = args.get___("auth")
        if auth_arg.defined():
            self.auth = self.read_auth(Args(auth_arg.as_dict()))
        else:
            self.auth = NoneAuth()
        self.envelope = self.read_envelope(args)
        self.capacity = Consumer.read_capacity(args)

        default_backend_options = args.get___("default")
        if default_backend_options.defined():
            default_options = self.create_options(Args(default_backend_options.as_dict()))
        else:
            default_options = None

        product_kinds = Map()
        for kind_id, backend_options_args in args.get___("product_kinds", {}).as_dict().items():
            product_kinds.register(
                item_id=kind_id,
                factory=lambda kind_id: self.create_options(Args(backend_options_args))
            )

        self.proxy = Consumer(
            vhost_id=vhost_id,
            system_id=system_id,
            default_options=default_options,
            product_kinds=product_kinds,
            origin=self,
            disabled=args.get___("disabled", {}).as_dict()
        )

    def create(self, backend_cfg, route_options):
        self.proxy.create(backend_cfg, route_options)

    def create_options(self, args):
        validate_arg(args, [Args], "args")

        args.validate({
            "path": {"type": str}
        })

        return HttpBackendOptions(
            scheme=self.scheme,
            target=self.target,
            path=args.get___("path").as_text()
        )

    def create_backend(self, backend_cfg, route_options, options):
        validate_arg(backend_cfg, [BackendCfg], "backend_cfg")
        validate_arg(route_options, [RouteOptions], "route_options")
        validate_arg(options, [HttpBackendOptions], "options")

        consumer_system = route_options.consumer_system

        backend = backend_cfg.backends.register(
            item_id="rmq:%s:%s->%s:%s" % (
                consumer_system.zone_id, consumer_system.vhost_id, consumer_system.system_id, consumer_system.vhost_id
            ),
            unique=False,
            factory=lambda backend_id: HttpBackend(
                vhost_id=route_options.consumer_system.vhost_id,
                system_id=route_options.consumer_system.system_id,
                capacity=self.capacity,
                scheme=self.scheme,
                target=self.target,
                envelope=self.envelope,
                auth=self.auth
            )
        )

        backend.add_queue(
            queue_name=route_options.product_kind.queue_name,
            path=options.path
        )

    def read_scheme(self, args):
        scheme = args.get___("scheme").as_text()

        if (scheme not in ["http", "https"]):
            raise Error("invalid scheme [%s]. Allowed values [http, https]" % scheme)

        return scheme

    def read_target(self, args):
        unverified = args.get___("target").as_text()
        parsed = urllib.parse.urlparse("%s://%s" % (self.scheme, unverified))

        if (parsed.hostname is None):
            raise Error("invalid target [%s]. Host is required" % unverified)

        if (parsed.port is None):
            raise Error("invalid target [%s]. Port is required" % unverified)

        if (parsed.netloc != unverified):
            raise Error("invalid target [%s]" % unverified)

        return parsed.netloc

    def read_envelope(self, args):
        envelope = args.get___("envelope").as_text()

        if (envelope not in ["xml", "json"]):
            raise Error("invalid envelope [%s]. Allowed values [xml, json]" % envelope)

        return envelope

    def read_auth(self, args):
        validate_arg(args, [Args], "args")

        args.validate({
            "type": {"type": str},
            "username": {"type": str},
            "path": {"type": str}
        })

        auth_type = args.get___("type").as_text()
        if (auth_type == "ntlm"):
            args.validate({
                "type": {"type": str},
                "username": {"type": str},
                "path": {"type": str}
            })

            return NtlmAuth(
                username=args.get___("username").as_text(),
                path=args.get___("path").as_text()
            )
        else:
            raise Error("invalid auth type [%s]. Allowed values [ntlm]" % auth_type)

class HttpBackendOptions:
    def __init__(self, scheme, target, path):
        validate_arg(scheme, [str], "scheme")
        validate_arg(target, [str], "target")
        validate_arg(path, [str], "path")

        self.scheme = scheme
        self.target = target
        self.path = path
