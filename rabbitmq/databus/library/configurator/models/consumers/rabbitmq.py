from python.collections import Map
from python.utils.args import Args
from python.utils.validators import validate_arg

from rabbitmq.databus.library.configurator.models.backends.rabbitmq_backend import RabbitmqBackend
from rabbitmq.databus.library.configurator.models.base import BackendCfg
from rabbitmq.databus.library.configurator.models.base import RouteOptions
from rabbitmq.databus.library.configurator.models.consumers.consumer import Consumer

class RabbitmqConsumer:
    type = "rabbitmq"

    def __init__(self, vhost_id, system_id, args):
        validate_arg(vhost_id, [str], "vhost_id")
        validate_arg(system_id, [str], "system_id")
        validate_arg(args, [Args], "args")

        args.validate({
            "type": {"type": str},
            "username": {"type": str},
            "product_kinds": {"type": dict},
            "disabled": {"type": dict, "optional": True},
            "capacity": {"type": int, "optional": True}
        })

        self.username = args.get___("username").as_text()
        self.capacity = Consumer.read_capacity(args)

        product_kinds = Map()
        for kind_id, backend_options_args in args.get___("product_kinds").as_dict().items():
            backend_options_args = Args(backend_options_args)

            backend_options_args.validate({
                "uri": {"type": str}
            })

            product_kinds.register(
                item_id=kind_id,
                factory=lambda kind_id: RabbitmqBackendOptions(
                    uri=backend_options_args.get___("uri").as_text()
                )
            )

        self.proxy = Consumer(
            vhost_id=vhost_id,
            system_id=system_id,
            default_options=None,
            product_kinds=product_kinds,
            origin=self,
            disabled=args.get___("disabled", {}).as_dict()
        )

    def create(self, backend_cfg, route_options):
        self.proxy.create(backend_cfg, route_options)

    def create_backend(self, backend_cfg, route_options, options):
        validate_arg(backend_cfg, [BackendCfg], "backend_cfg")
        validate_arg(route_options, [RouteOptions], "route_options")
        validate_arg(options, [RabbitmqBackendOptions], "options")

        consumer_system = route_options.consumer_system

        backend_cfg.backends.register(
            item_id="rmq:%s:%s->%s:%s" % (
                route_options.consumer_system.zone_id,
                route_options.consumer_system.vhost_id,
                route_options.consumer_system.system_id,
                route_options.product_kind.queue_name.replace(
                    "q_%s_" % route_options.consumer_system.system_id, ""
                )
            ),
            unique=False,
            factory=lambda route_id: RabbitmqBackend(
                vhost_id=route_options.consumer_system.vhost_id,
                queue=route_options.product_kind.queue_name,
                retry_prefix=route_options.consumer_system.system_id,
                uri=options.uri,
                username=self.username
            )
        )

class RabbitmqBackendOptions:
    def __init__(self, uri):
        validate_arg(uri, [str], "uri")

        self.uri = uri
