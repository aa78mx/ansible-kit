from enum import Enum

from python.collections import Map
from python.collections import Set
from python.error import Error
from python.utils.args import Args
from python.utils.validators import validate_arg

from rabbitmq.databus.library.configurator.models.backends.blackhole_backend import BlackholeBackend
from rabbitmq.databus.library.configurator.models.base import BackendCfg
from rabbitmq.databus.library.configurator.models.base import RouteOptions

class State(Enum):
    ENABLED = "enabled"
    DISABLED = "disabled"

class Consumer:
    @staticmethod
    def read_capacity(args):
        validate_arg(args, [Args], "args")

        capacity = args.get___("capacity", 10).as_int()

        if (capacity <= 0):
            raise Error("invalid consumer capacity")

        return capacity

    def __init__(self, vhost_id, system_id, default_options, product_kinds, origin, disabled):
        validate_arg(vhost_id, [str], "vhost_id")
        validate_arg(system_id, [str], "system_id")
        validate_arg(product_kinds, [Map], "product_kinds")
        validate_arg(origin, [object], "origin")
        validate_arg(disabled, [dict], "disabled")

        self.vhost_id = vhost_id
        self.system_id = system_id
        self.default_options = default_options
        self.product_kinds = product_kinds
        self.origin = origin
        self.disabled = Set()

        for kind_id, disabled_args in disabled.items():
            disabled_args = Args(disabled_args, True)

            disabled_args.validate({
            })

            if ((self.default_options is None) and (kind_id not in self.product_kinds)):
                raise Error("unconfigured kind detected [%s]" % (kind_id))

            self.disabled.register(kind_id)

    def create(self, backend_cfg, route_options):
        validate_arg(backend_cfg, [BackendCfg], "backend_cfg")
        validate_arg(route_options, [RouteOptions], "route_options")

        if (route_options.product_kind.kind_id in self.disabled):
            state = State.DISABLED
        else:
            state = State.ENABLED

        options = self.product_kinds.get(route_options.product_kind.kind_id, self.default_options)

        if (options is not None):
            if (state == State.ENABLED):
                self.origin.create_backend(
                    backend_cfg=backend_cfg,
                    route_options=route_options,
                    options=options
                )
            else:
                self.create_blackhole_backend(
                    backend_cfg=backend_cfg,
                    route_options=route_options
                )

            route_options.product_kind.backends.register(
                item_id=route_options.consumer_system.environment_id,
                factory=lambda environment_id: Backend(state, self.origin, options)
            )
        else:
            raise Error("unconfigured kind detected [%s]" % (route_options.product_kind.kind_id))

    def create_blackhole_backend(self, backend_cfg, route_options):
        consumer_system = route_options.consumer_system

        backend = backend_cfg.backends.register(
            item_id="rmq:%s:%s->blackhole:%s" % (
                consumer_system.zone_id, consumer_system.vhost_id, consumer_system.vhost_id
            ),
            unique=False,
            factory=lambda backend_id: BlackholeBackend(
                vhost_id=route_options.consumer_system.vhost_id,
                retry_prefix=route_options.consumer_system.system_id,
                capacity=10
            )
        )

        backend.add_queue(route_options.product_kind.queue_name)

class Backend:
    def __init__(self, state, consumer, options):
        super().__init__()

        self.state = state
        self.consumer = consumer
        self.options = options
