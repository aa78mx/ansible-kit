from python.collections import Map
from python.utils.args import Args
from python.utils.validators import validate_arg

from rabbitmq.databus.library.configurator.models.base import BackendCfg
from rabbitmq.databus.library.configurator.models.base import RouteOptions
from rabbitmq.databus.library.configurator.models.consumers.consumer import Consumer

class ExternalConsumer:
    type = "external"

    def __init__(self, vhost_id, system_id, args):
        validate_arg(vhost_id, [str], "vhost_id")
        validate_arg(system_id, [str], "system_id")
        validate_arg(args, [Args], "args")

        args.validate({
            "type": {"type": str},
            "disabled": {"type": dict, "optional": True}
        })

        self.proxy = Consumer(
            vhost_id=vhost_id,
            system_id=system_id,
            default_options=ExternalBackendOptions(),
            product_kinds=Map(),
            origin=self,
            disabled=args.get___("disabled", {}).as_dict()
        )

    def create(self, backend_cfg, route_options):
        self.proxy.create(backend_cfg, route_options)

    def create_backend(self, backend_cfg, route_options, options):
        validate_arg(backend_cfg, [BackendCfg], "backend_cfg")
        validate_arg(route_options, [RouteOptions], "route_options")
        validate_arg(options, [ExternalBackendOptions], "options")

class ExternalBackendOptions:
    pass
