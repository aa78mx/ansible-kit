from python.collections import Map
from python.utils.validators import validate_arg

from rabbitmq.databus.library.configurator.models.backends.rabbitmq_backend import RabbitmqBackend

class InterZoneRouterCfg:
    def __init__(self):
        super().__init__()

        self.routers = Map()

    def add_route(
        self, router_id, producer_peer, producer_vhost, queue_name, consumer_peer, consumer_vhost
    ):
        from rabbitmq.databus.library.configurator.models.base import Peer
        from rabbitmq.databus.library.configurator.models.base import Vhost

        validate_arg(router_id, [str], "router_id")
        validate_arg(producer_peer, [Peer], "producer_peer")
        validate_arg(producer_vhost, [Vhost], "producer_vhost")
        validate_arg(queue_name, [str], "queue_name")
        validate_arg(consumer_peer, [Peer], "consumer_peer")
        validate_arg(consumer_vhost, [Vhost], "consumer_vhost")

        router = self.routers.register(
            item_id=router_id,
            unique=False,
            factory=lambda router_id: Router()
        )

        router.routes.register(
            item_id="rmq:%s:%s->rmq:%s:%s" % (
                producer_vhost.zone_id,
                producer_vhost.vhost_id,
                consumer_vhost.zone_id,
                consumer_vhost.vhost_id
            ),
            unique=False,
            factory=lambda router_id: Route(
                producer_peer=producer_peer,
                producer_vhost=producer_vhost,
                queue=queue_name,
                consumer_peer=consumer_peer,
                consumer_vhost=consumer_vhost
            )
        )

    def serialize(self, credentials):
        validate_arg(credentials, [Map], "credentials")

        return dict(map(
            lambda pair: (pair[0], pair[1].serialize(credentials)),
            self.routers.items()
        ))

class Router:
    def __init__(self):
        super().__init__()

        self.routes = Map()

    def serialize(self, credentials):
        return {
            "workers": dict(map(
                lambda pair: (pair[0], pair[1].serialize(credentials)),
                self.routes.items()
            ))
        }

class Route:
    def __init__(self, producer_peer, producer_vhost, queue, consumer_peer, consumer_vhost):
        super().__init__()

        self.producer_peer = producer_peer
        self.backend = RabbitmqBackend(
            vhost_id=producer_vhost.vhost_id,
            queue=queue,
            retry_prefix="rmq",
            uri=consumer_peer.create_connection_uri(consumer_vhost.vhost_id),
            username=consumer_peer.username
        )

    def serialize(self, credentials):
        return self.backend.serialize(self.producer_peer, credentials)
