from python.collections import Map
from python.utils.validators import validate_arg

from rabbitmq.databus.library.configurator.models.backends.rabbitmq_backend import RabbitmqBackend

class ZoneRouterCfg:
    def __init__(self, backend_cfg):
        from rabbitmq.databus.library.configurator.models.base import BackendCfg

        super().__init__()

        validate_arg(backend_cfg, [BackendCfg], "backend_cfg")

        self.backends = backend_cfg.backends

    def add_route(self, producer_vhost, queue_name, consumer_vhost):
        from rabbitmq.databus.library.configurator.models.base import Vhost

        validate_arg(producer_vhost, [Vhost], "producer_vhost")
        validate_arg(queue_name, [str], "queue_name")
        validate_arg(consumer_vhost, [Vhost], "consumer_vhost")

        self.backends.register(
            item_id="rmq:%s:%s->rmq:%s:%s" % (
                producer_vhost.zone_id,
                producer_vhost.vhost_id,
                consumer_vhost.zone_id,
                consumer_vhost.vhost_id
            ),
            unique=False,
            factory=lambda route_id: Route(
                producer_vhost=producer_vhost,
                queue=queue_name,
                consumer_vhost=consumer_vhost
            )
        )

class Route:
    def __init__(self, producer_vhost, queue, consumer_vhost):
        super().__init__()

        self.producer_vhost_id = producer_vhost.vhost_id
        self.queue = queue
        self.consumer_vhost_id = consumer_vhost.vhost_id

    def serialize(self, peer, credentials):
        from rabbitmq.databus.library.configurator.models.base import Peer

        validate_arg(peer, [Peer], "peer")
        validate_arg(credentials, [Map], "credentials")

        return RabbitmqBackend(
            vhost_id=self.producer_vhost_id,
            queue=self.queue,
            retry_prefix="rmq",
            uri=peer.create_connection_uri(self.consumer_vhost_id),
            username=peer.username
        ).serialize(peer, credentials)
