from python.collections import Map
from python.utils.validators import validate_arg

class BlackholeBackend:
    def __init__(self, vhost_id, retry_prefix, capacity):
        super().__init__()

        validate_arg(vhost_id, [str], "vhost_id")
        validate_arg(retry_prefix, [str], "retry_prefix")
        validate_arg(capacity, [int], "capacity")

        self.vhost_id = vhost_id
        self.retry_prefix = retry_prefix
        self.capacity = capacity
        self.queues = Map()

    def add_queue(self, queue_name):
        self.queues.register(
            item_id=queue_name,
            unique=False,
            factory=lambda queue_name: {}
        )

    def serialize(self, peer, credentials):
        from rabbitmq.databus.library.configurator.models.base import Peer

        validate_arg(peer, [Peer], "peer")
        validate_arg(credentials, [Map], "credentials")

        return {
            "type": "rabbitmq-blackhole",
            "capacity": self.capacity,
            "queues": self.queues,
            "retry.exchange": "ex_%s_retry" % self.retry_prefix,
            "retry.queue": "q_%s_retry" % self.retry_prefix,
            "input": {
                "auth": {
                    "password": credentials.find(peer.username, "password"),
                    "username": peer.username
                },
                "uri": peer.create_connection_uri(self.vhost_id)
            }
        }
