from python.collections import Map
from python.utils.validators import validate_arg

class RabbitmqBackend:
    def __init__(self, vhost_id, queue, retry_prefix, uri, username):
        super().__init__()

        validate_arg(vhost_id, [str], "vhost_id")
        validate_arg(queue, [str], "queue")
        validate_arg(retry_prefix, [str], "retry_prefix")
        validate_arg(uri, [str], "uri")
        validate_arg(username, [str], "username")

        self.vhost_id = vhost_id
        self.queue = queue
        self.retry_prefix = retry_prefix
        self.uri = uri
        self.username = username

    def serialize(self, peer, credentials):
        from rabbitmq.databus.library.configurator.models.base import Peer

        validate_arg(peer, [Peer], "peer")
        validate_arg(credentials, [Map], "credentials")

        return {
            "type": "rabbitmq-rabbitmq",
            "capacity": 1,
            "queue": self.queue,
            "retry.exchange": "ex_%s_retry" % self.retry_prefix,
            "retry.queue": "q_%s_retry" % self.retry_prefix,
            "output": {
                "auth": {
                    "password": credentials.find(self.username, "password"),
                    "username": self.username
                },
                "uri": self.uri
            },
            "input": {
                "auth": {
                    "password": credentials.find(peer.username, "password"),
                    "username": peer.username
                },
                "uri": peer.create_connection_uri(self.vhost_id)
            }
        }
