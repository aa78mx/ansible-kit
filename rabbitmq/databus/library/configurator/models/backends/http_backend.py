from python.collections import Map
from python.utils.validators import validate_arg

class NoneAuth:
    type = "none"

    def serialize(self, credentials):
        validate_arg(credentials, [Map], "credentials")

        return {}

class NtlmAuth:
    type = "ntlm"

    def __init__(self, username, path):
        validate_arg(username, [str], "username")
        validate_arg(path, [str], "path")

        self.username = username
        self.path = path

    def serialize(self, credentials):
        validate_arg(credentials, [Map], "credentials")

        return {
            "ntlm_auth": {
                "username": self.username,
                "password": credentials.find(self.username, "password"),
                "path": self.path
            }
        }

class HttpBackend:
    def __init__(self, vhost_id, system_id, capacity, scheme, target, envelope, auth):
        super().__init__()

        validate_arg(vhost_id, [str], "vhost_id")
        validate_arg(system_id, [str], "system_id")
        validate_arg(capacity, [int], "capacity")
        validate_arg(scheme, [str], "scheme")
        validate_arg(target, [str], "target")
        validate_arg(envelope, [str], "envelope")
        validate_arg(auth, [NoneAuth, NtlmAuth], "auth")

        self.vhost_id = vhost_id
        self.system_id = system_id
        self.capacity = capacity
        self.scheme = scheme
        self.target = target
        self.envelope = envelope
        self.auth = auth
        self.queues = Map()

    def add_queue(self, queue_name, path):
        validate_arg(queue_name, [str], "queue_name")
        validate_arg(path, [str], "path")

        self.queues.register(
            item_id=queue_name,
            unique=False,
            factory=lambda queue_name: {"path": path}
        )

    def serialize(self, peer, credentials):
        from rabbitmq.databus.library.configurator.models.base import Peer

        validate_arg(peer, [Peer], "peer")
        validate_arg(credentials, [Map], "credentials")

        return {
            "type": "rabbitmq-http",
            "capacity": self.capacity,
            "queues": self.queues,
            "retry.exchange": "ex_%s_retry" % self.system_id,
            "retry.queue": "q_%s_retry" % self.system_id,
            "envelope": self.envelope,
            "output": {
                **{
                    "scheme": self.scheme,
                    "target": self.target
                },
                **self.auth.serialize(credentials)
            },
            "input": {
                "auth": {
                    "password": credentials.find(peer.username, "password"),
                    "username": peer.username
                },
                "uri": peer.create_connection_uri(self.vhost_id)
            }
        }
