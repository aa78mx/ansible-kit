from grafana.library.panel.table.style import PanelTableColor
from grafana.library.panel.table.style import PanelTableColorMode

class RmqConstants:
    SMOOTHING_PERIOD = "90s"

    RMQ_MAIN_UID = "rmq-main"
    RMQ_MAIN_TITLE = "00.rmq-main"

    RMQ_ZONE_UID = "rmq-zone"
    RMQ_ZONE_TITLE = "01.rmq-zone"

    RMQ_ROUTER_UID = "rmq-router"
    RMQ_ROUTER_TITLE = "02.rmq-router"

    RMQ_VHOST_UID = "rmq-vhost"
    RMQ_VHOST_TITLE = "03.rmq-vhost"

    RMQ_MESSAGE_LOG_UID = "rmq-message-log"
    RMQ_MESSAGE_LOG_TITLE = "10.rmq-message-log"

    RMQ_MAIN_REPORT_UID = "rmq-main-report"
    RMQ_MAIN_REPORT_TITLE = "20.rmq-main-report"

    RMQ_STATE_ON_DATE_REPORT_UID = "rmq-state-on-date-report"
    RMQ_STATE_ON_DATE_REPORT_TITLE = "21.rmq-state-on-date-report"

    COLOR_UPTIME = PanelTableColor(PanelTableColorMode.CELL, 43200, 86400, True)
    COLOR_MESSAGE_READY = PanelTableColor(PanelTableColorMode.CELL, 1, 1000)
