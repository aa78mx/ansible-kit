from python.utils.validators import validate_arg

from grafana.library.constants import GrafanaConstants
from grafana.library.dashboard.dashboard import Dashboard
from grafana.library.dashboard.link import DashboardLink
from grafana.library.dashboard.variable.query import DashboardVariableQuery
from grafana.library.panel.graph.graph import PanelGraph
from grafana.library.panel.graph.graph import PanelGraphLegend
from grafana.library.panel.table.column.label import PanelTableColumnLabel
from grafana.library.panel.table.style import PanelTableLink
from grafana.library.panel.table.style import PanelTableStyleHidden
from grafana.library.panel.table.style import PanelTableStyleNumber
from grafana.library.prometheus.filter import PrometheusFilter
from grafana.library.prometheus.panel.graph.column.data import PrometheusPanelGraphColumnData
from grafana.library.prometheus.panel.table.table import PrometheusPanelTable
from grafana.library.prometheus.panel.table.table import PrometheusPanelTableColumnData

from rabbitmq.databus.library.dashboard.constants import RmqConstants

class RmqZoneDashboard:
    def __init__(self):
        super().__init__()

        self.y_virtual_host_table = 0
        self.h_virtual_host_table = 13
        self.y_message_ready_graph = self.y_virtual_host_table + self.h_virtual_host_table
        self.h_message_ready_graph = 7
        self.y_disk_free_graph = self.y_message_ready_graph + self.h_message_ready_graph
        self.h_disk_free_graph = 7

    def build_virtual_host_table(self, panel_count_in_row, panel_no, circuit_no, f):
        validate_arg(panel_count_in_row, [int], "panel_count_in_row")
        validate_arg(panel_no, [int], "panel_no")
        validate_arg(circuit_no, [str], "circuit_no")
        validate_arg(f, [PrometheusFilter], "f")

        by = " by (vhost)"
        link_factory = lambda tooltip: PanelTableLink(
            url="/grafana/d/%s/%s?orgId=1&refresh=30s&var-vhost=${__cell_1}&var-system=All" % (
                RmqConstants.RMQ_VHOST_UID, RmqConstants.RMQ_VHOST_TITLE
            ),
            tooltip=tooltip
        )
        f = f.add("circuit", "=", circuit_no)

        data_columns = []
        label_columns = []
        label_columns.append(PanelTableColumnLabel("Time", PanelTableStyleHidden()))

        _f = f.add("job", "=", "rmq-rabbitmq-server")
        _f = _f.add("queue", "=~", "^q_rmq_.*$")
        data_columns.append(PrometheusPanelTableColumnData(
            expr="sum(max_over_time(%s))%s" % (
                _f.metric("rabbitmq_queue_messages", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleNumber(
                alias="msg.rmq",
                link=link_factory("Кол-во сообщений для отправки на другой rabbitmq"),
                color=RmqConstants.COLOR_MESSAGE_READY
            )
        ))

        _f = f.add("job", "=", "rmq-rabbitmq-server")
        _f = _f.add("queue", "!~", "q_rmq_.*")
        data_columns.append(PrometheusPanelTableColumnData(
            expr="sum(max_over_time(%s))%s" % (
                _f.metric("rabbitmq_queue_messages", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleNumber(
                alias="msg.others",
                link=link_factory("Кол-во сообщений для обработки конечными системами"),
                color=RmqConstants.COLOR_MESSAGE_READY
            )
        ))

        return PrometheusPanelTable(
            x=GrafanaConstants.X[panel_count_in_row - 1][panel_no],
            y=self.y_virtual_host_table,
            w=GrafanaConstants.W[panel_count_in_row - 1],
            h=self.h_virtual_host_table,
            datasource="prometheus",
            title="virtual host (circuit%s)" % circuit_no,
            fontSize="65%",
            data_columns=data_columns,
            label_columns=label_columns
        )

    def build_message_ready_graph(self, panel_count_in_row, panel_no, circuit_no, f):
        validate_arg(panel_count_in_row, [int], "panel_count_in_row")
        validate_arg(panel_no, [int], "panel_no")
        validate_arg(circuit_no, [str], "circuit_no")
        validate_arg(f, [PrometheusFilter], "f")

        by = " by (vhost)"
        _f = f.add("circuit", "=", circuit_no)
        _f = _f.add("job", "=", "rmq-rabbitmq-server")

        return PanelGraph(
            x=GrafanaConstants.X[panel_count_in_row - 1][panel_no],
            y=self.y_message_ready_graph,
            w=GrafanaConstants.W[panel_count_in_row - 1],
            h=self.h_message_ready_graph,
            datasource="prometheus",
            title="message ready (circuit%s)" % circuit_no,
            data_columns=[
                PrometheusPanelGraphColumnData(
                    expr="sum(max_over_time(%s) > 0)%s" % (
                        _f.metric("rabbitmq_queue_messages", RmqConstants.SMOOTHING_PERIOD),
                        by
                    ),
                    legend="{{vhost}}"
                )
            ],
            legend=PanelGraphLegend(
                as_table=False,
                right_side=False,
                current=False
            ),
            y_min=0,
            y_max=0.5e6
        )

    def build_disk_free_graph(self, panel_count_in_row, panel_no, circuit_no, f):
        validate_arg(panel_count_in_row, [int], "panel_count_in_row")
        validate_arg(panel_no, [int], "panel_no")
        validate_arg(circuit_no, [str], "circuit_no")
        validate_arg(f, [PrometheusFilter], "f")

        _f = f.add("circuit", "=", circuit_no)
        _f = _f.add("job", "=", "rmq-rabbitmq-server")

        return PanelGraph(
            x=GrafanaConstants.X[panel_count_in_row - 1][panel_no],
            y=self.y_disk_free_graph,
            w=GrafanaConstants.W[panel_count_in_row - 1],
            h=self.h_disk_free_graph,
            datasource="prometheus",
            title="disk free (circuit%s)" % circuit_no,
            data_columns=[
                PrometheusPanelGraphColumnData(
                    expr="min(min_over_time(%s) - max_over_time(%s))" % (
                        _f.metric("rabbitmq_disk_space_available_bytes", RmqConstants.SMOOTHING_PERIOD),
                        _f.metric("rabbitmq_disk_space_available_limit_bytes", RmqConstants.SMOOTHING_PERIOD)
                    ),
                    legend="disk free"
                )
            ],
            y_format="bytes",
            y_min=0,
            y_max=GrafanaConstants.BYTES_IN_GiB * 100
        )

    def build(self, build_dir):
        validate_arg(build_dir, [str], "build_dir")

        f = PrometheusFilter().add("zone", "=", "${zone}")

        Dashboard(
            uid=RmqConstants.RMQ_ZONE_UID,
            title=RmqConstants.RMQ_ZONE_TITLE,
            panels=[
                self.build_virtual_host_table(2, 0, "01", f=f),
                self.build_virtual_host_table(2, 1, "02", f=f),
                self.build_message_ready_graph(2, 0, "01", f=f),
                self.build_message_ready_graph(2, 1, "02", f=f),
                self.build_disk_free_graph(2, 0, "01", f=f),
                self.build_disk_free_graph(2, 1, "02", f=f)
            ],
            variables=[
                DashboardVariableQuery(
                    name="zone",
                    datasource="prometheus",
                    query="label_values(%s, zone)" % PrometheusFilter().add("job", "=~", "^rmq-.*$").metric("up")
                )
            ],
            links=[
                DashboardLink(
                    url="/grafana/d/rmq-main/%s?orgId=1&refresh=30s" % RmqConstants.RMQ_MAIN_UID,
                    title=RmqConstants.RMQ_MAIN_TITLE
                )
            ]
        ).save(build_dir)
