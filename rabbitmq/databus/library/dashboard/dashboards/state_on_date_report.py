from python.utils.validators import validate_arg

from grafana.library.clickhouse.panel.field import ClickhousePanelField
from grafana.library.clickhouse.panel.table.table import ClickhousePanelTable
from grafana.library.constants import GrafanaConstants
from grafana.library.dashboard.dashboard import Dashboard
from grafana.library.dashboard.variable.custom_multi import DashboardVariableCustomMulti
from grafana.library.dashboard.variable.custom_multi import DashboardVariableCustomMultiValue
from grafana.library.dashboard.variable.option.include_all import DashboardVariableQueryOptionIncludeAll
from grafana.library.dashboard.variable.query import DashboardVariableQuery
from grafana.library.dashboard.variable.query import DashboardVariableQuerySort
from grafana.library.dashboard.variable.text import DashboardVariableText

from rabbitmq.databus.library.dashboard.constants import RmqConstants

class RmqStateOnDateReportDashboard:
    def __init__(self):
        super().__init__()

        self.y_report_table = 0
        self.h_report_table = 94

    def build_report_table(self):
        return ClickhousePanelTable(
            x=GrafanaConstants.X[0][0],
            y=self.y_report_table,
            w=GrafanaConstants.W[0],
            h=self.h_report_table,
            datasource="loghouse",
            query="\n".join([
                "select",
                "  ${var_fields:csv},",
                "  ${var_metrics:csv}",
                "from (",
                "  select",
                "    t.package_id,",
                "    t.package_source_version,",
                "    t.package_title,",
                "    t.package_exchange,",
                "    t.package_routing_key,",
                "    t.destination_system,",
                "    t.destination_system_name,",
                "    max(t.package_try_no) package_try_no,",
                "    toUnixTimestamp(max(t.timestamp)) * 1000 timestamp_last_event,",
                "    argMax(t.message, (t.package_try_no, t.timestamp)) message,",
                "    argMax(t.level, (t.package_try_no, t.timestamp)) level,",
                "    argMax(t.source_system, (t.package_try_no, t.timestamp)) source_system,",
                "    argMax(t.source_system_name, (t.package_try_no, t.timestamp)) source_system_name,",
                "    argMax(t.queue, (t.package_try_no, t.timestamp)) queue,",
                "    argMax(t.processing_duration, (t.package_try_no, t.timestamp)) processing_duration,",
                "    argMax(t.package_size, (t.package_try_no, t.timestamp)) package_size",
                "  from log.databuslog t",
                "  where (",
                "   $conditionalTest((t.package_source_version == ${var_source_version:sqlstring}) and, $var_source_version)",
                "   $conditionalTest((t.package_title in (${var_title:sqlstring})) and, $var_title)",
                "   $conditionalTest((t.destination_system in (${var_destination_system:sqlstring})) and, $var_destination_system)",
                "   $conditionalTest((t.package_exchange in (${var_exchange:sqlstring})) and, $var_exchange)",
                "   $conditionalTest((t.package_routing_key in (${var_routing_key:sqlstring})) and, $var_routing_key)",
                "   $conditionalTest((t.queue in (${var_queue:sqlstring})) and, $var_queue)",
                "    (t.logger == 'com.gitlab._1048576.databus.impl.MessageFactoryImpl') and",
                "    ($timeFilter)",
                "  )",
                "  group by",
                "    t.package_id,",
                "    t.package_source_version,",
                "    t.package_title,",
                "    t.package_exchange,",
                "    t.package_routing_key,",
                "    t.destination_system,",
                "    t.destination_system_name",
                ")",
                "where level in (${var_level:sqlstring})",
                "group by ${var_fields:csv}",
                "limit 100"
            ]),
            date_time_col_data_type="timestamp",
            fields={
                "timestamp_last_event": ClickhousePanelField(unit="dateTimeAsIso"),
                "processing_duration_min": ClickhousePanelField(unit="ms"),
                "processing_duration_max": ClickhousePanelField(unit="ms"),
                "processing_duration_median": ClickhousePanelField(unit="ms"),
                "package_size_min": ClickhousePanelField(unit="bytes"),
                "package_size_max": ClickhousePanelField(unit="bytes"),
                "package_size_median": ClickhousePanelField(unit="bytes")
            }
        )

    def build(self, build_dir):
        validate_arg(build_dir, [str], "build_dir")

        Dashboard(
            uid=RmqConstants.RMQ_STATE_ON_DATE_REPORT_UID,
            title=RmqConstants.RMQ_STATE_ON_DATE_REPORT_TITLE,
            panels=[
                self.build_report_table()
            ],
            variables=[
                DashboardVariableCustomMulti(
                    name="var_fields",
                    values=[
                        DashboardVariableCustomMultiValue("package_id"),
                        DashboardVariableCustomMultiValue("package_source_version"),
                        DashboardVariableCustomMultiValue("package_title"),
                        DashboardVariableCustomMultiValue("package_exchange"),
                        DashboardVariableCustomMultiValue("package_routing_key"),
                        DashboardVariableCustomMultiValue("package_try_no"),
                        DashboardVariableCustomMultiValue("timestamp_last_event"),
                        DashboardVariableCustomMultiValue("message"),
                        DashboardVariableCustomMultiValue("level"),
                        DashboardVariableCustomMultiValue("source_system"),
                        DashboardVariableCustomMultiValue("source_system_name"),
                        DashboardVariableCustomMultiValue("destination_system"),
                        DashboardVariableCustomMultiValue("destination_system_name"),
                        DashboardVariableCustomMultiValue("queue")
                    ],
                    label="fields"
                ),
                DashboardVariableCustomMulti(
                    name="var_metrics",
                    values=[
                        DashboardVariableCustomMultiValue(
                            value="count(distinct package_id) package_count",
                            text="package_count"
                        ),
                        DashboardVariableCustomMultiValue(
                            value="min(processing_duration) processing_duration_min",
                            text="processing_duration_min"
                        ),
                        DashboardVariableCustomMultiValue(
                            value="max(processing_duration) processing_duration_max",
                            text="processing_duration_max"
                        ),
                        DashboardVariableCustomMultiValue(
                            value="quantileTiming(processing_duration) processing_duration_median",
                            text="processing_duration_median"
                        ),
                        DashboardVariableCustomMultiValue(
                            value="min(package_size) package_size_min",
                            text="package_size_min"
                        ),
                        DashboardVariableCustomMultiValue(
                            value="max(package_size) package_size_max",
                            text="package_size_max"
                        ),
                        DashboardVariableCustomMultiValue(
                            value="quantile(package_size) package_size_median",
                            text="package_size_median"
                        )
                    ],
                    label="metrics"
                ),
                DashboardVariableCustomMulti(
                    name="var_level",
                    values=[
                        DashboardVariableCustomMultiValue("INFO"),
                        DashboardVariableCustomMultiValue("ERROR"),
                        DashboardVariableCustomMultiValue("WARNING")
                    ],
                    label="level"
                ),
                DashboardVariableText(
                    name="var_source_version",
                    label="source version"
                ),
                DashboardVariableQuery(
                    name="var_title",
                    query="\n".join([
                        "select distinct package_title",
                        "from log.databuslog",
                        "where package_title != ''",
                        "order by package_title"
                    ]),
                    datasource="loghouse",
                    label="title",
                    multi=True,
                    include_all=DashboardVariableQueryOptionIncludeAll()
                ),
                DashboardVariableQuery(
                    name="var_destination_system",
                    query="\n".join([
                        "select distinct destination_system",
                        "from log.databuslog",
                        "where destination_system != ''",
                        "order by destination_system"
                    ]),
                    datasource="loghouse",
                    label="destination_system",
                    multi=True,
                    include_all=DashboardVariableQueryOptionIncludeAll()
                ),
                DashboardVariableQuery(
                    name="var_exchange",
                    query="\n".join([
                        "select distinct package_exchange",
                        "from log.databuslog",
                        "where package_exchange != ''",
                        "order by package_exchange"
                    ]),
                    datasource="loghouse",
                    label="exchange",
                    multi=True,
                    include_all=DashboardVariableQueryOptionIncludeAll()
                ),
                DashboardVariableQuery(
                    name="var_routing_key",
                    query="\n".join([
                        "select distinct package_routing_key",
                        "from log.databuslog",
                        "where package_routing_key != ''",
                        "order by package_routing_key"
                    ]),
                    datasource="loghouse",
                    label="routing_key",
                    multi=True,
                    include_all=DashboardVariableQueryOptionIncludeAll()
                ),
                DashboardVariableQuery(
                    name="var_queue",
                    query="\n".join([
                        "select distinct queue",
                        "from log.databuslog",
                        "where queue != ''",
                        "order by queue"
                    ]),
                    datasource="loghouse",
                    label="queue",
                    multi=True,
                    include_all=DashboardVariableQueryOptionIncludeAll()
                )
            ]
        ).save(build_dir)
