from python.utils.validators import validate_arg

from grafana.library.clickhouse.panel.table.table import ClickhousePanelTable
from grafana.library.constants import GrafanaConstants
from grafana.library.dashboard.dashboard import Dashboard
from grafana.library.dashboard.variable.custom_multi import DashboardVariableCustomMulti
from grafana.library.dashboard.variable.custom_multi import DashboardVariableCustomMultiValue

from rabbitmq.databus.library.dashboard.constants import RmqConstants

class RmqMainReportDashboard:
    def __init__(self):
        super().__init__()

        self.y_report_table = 0
        self.h_report_table = 94

    def build_report_table(self):
        return ClickhousePanelTable(
            x=GrafanaConstants.X[0][0],
            y=self.y_report_table,
            w=GrafanaConstants.W[0],
            h=self.h_report_table,
            datasource="loghouse",
            query="\n".join([
                "select",
                "  ${var_fields:csv},",
                "  ${var_metrics:csv}",
                "from (",
                "  select *",
                "  from log.databuslog",
                "  where (",
                "    (logger == 'com.gitlab._1048576.databus.impl.MessageFactoryImpl') and",
                "    ($timeFilter)",
                "  )",
                ")",
                "group by ${var_fields:csv}",
                "limit 100"
            ]),
            date_time_col_data_type="timestamp"
        )

    def build(self, build_dir):
        validate_arg(build_dir, [str], "build_dir")

        Dashboard(
            uid=RmqConstants.RMQ_MAIN_REPORT_UID,
            title=RmqConstants.RMQ_MAIN_REPORT_TITLE,
            panels=[
                self.build_report_table()
            ],
            variables=[
                DashboardVariableCustomMulti(
                    name="var_fields",
                    values=[
                        DashboardVariableCustomMultiValue("level"),
                        DashboardVariableCustomMultiValue("package_title"),
                        DashboardVariableCustomMultiValue("package_exchange"),
                        DashboardVariableCustomMultiValue("message"),
                        DashboardVariableCustomMultiValue("source_system"),
                        DashboardVariableCustomMultiValue("source_system_name"),
                        DashboardVariableCustomMultiValue("destination_system"),
                        DashboardVariableCustomMultiValue("destination_system_name")
                    ],
                    label="fields"
                ),
                DashboardVariableCustomMulti(
                    name="var_metrics",
                    values=[
                        DashboardVariableCustomMultiValue(
                            value="count(*) as event_count",
                            text="event_count"
                        ),
                        DashboardVariableCustomMultiValue(
                            value="count(distinct package_id) as package_count",
                            text="package_count"
                        )
                    ],
                    label="metrics"
                )
            ]
        ).save(build_dir)
