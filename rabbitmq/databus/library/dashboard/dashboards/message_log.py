from python.utils.validators import validate_arg

from grafana.library.clickhouse.panel.field import ClickhousePanelField
from grafana.library.clickhouse.panel.table.table import ClickhousePanelTable
from grafana.library.constants import GrafanaConstants
from grafana.library.dashboard.dashboard import Dashboard
from grafana.library.dashboard.timepicker import DashboardTimepickerHidden
from grafana.library.dashboard.variable.custom_multi import DashboardVariableCustomMulti
from grafana.library.dashboard.variable.custom_multi import DashboardVariableCustomMultiValue
from grafana.library.dashboard.variable.option.include_all import DashboardVariableQueryOptionIncludeAll
from grafana.library.dashboard.variable.query import DashboardVariableQuery
from grafana.library.dashboard.variable.text import DashboardVariableText

from rabbitmq.databus.library.dashboard.constants import RmqConstants

class RmqMessageLogDashboard:
    def __init__(self):
        super().__init__()

        self.y_log_table = 0
        self.h_log_table = 94

    def build_log_table(self):
        return ClickhousePanelTable(
            x=GrafanaConstants.X[0][0],
            y=self.y_log_table,
            w=GrafanaConstants.W[0],
            h=self.h_log_table,
            datasource="loghouse",
            query="\n".join([
                "select",
                "  ${var_fields:csv}",
                "from (",
                "  select",
                "    toUnixTimestamp(t.timestamp) * 1000 timestamp,",
                "    t.*",
                "  from log.databuslog t",
                "  where (",
                "   $conditionalTest((package_title in (${var_title:sqlstring})) and, $var_title)",
                "   $conditionalTest((destination_system in (${var_destination_system:sqlstring})) and, $var_destination_system)",
                "   $conditionalTest((destination_system_name in (${var_destination_system_name:sqlstring})) and, $var_destination_system_name)",
                "   $conditionalTest((package_exchange in (${var_exchange:sqlstring})) and, $var_exchange)",
                "   $conditionalTest((package_routing_key in (${var_routing_key:sqlstring})) and, $var_routing_key)",
                "   $conditionalTest((t.queue in (${var_queue:sqlstring})) and, $var_queue)",
                "   $conditionalTest((package_source_version like '${var_source_version}%') and, $var_source_version)",
                "    (package_try_no >= ${var_try_no}) and",
                "    (logger == 'com.gitlab._1048576.databus.impl.MessageFactoryImpl')",
                "  )",
                ")",
                "order by timestamp desc",
                "limit 100"
            ]),
            fields={
                "timestamp": ClickhousePanelField(unit="dateTimeAsIso")
            }
        )

    def build(self, build_dir):
        validate_arg(build_dir, [str], "build_dir")

        Dashboard(
            uid=RmqConstants.RMQ_MESSAGE_LOG_UID,
            title=RmqConstants.RMQ_MESSAGE_LOG_TITLE,
            timepicker=DashboardTimepickerHidden(),
            panels=[
                self.build_log_table()
            ],
            variables=[
                DashboardVariableCustomMulti(
                    name="var_fields",
                    values=[
                        DashboardVariableCustomMultiValue("timestamp", selected=True),
                        DashboardVariableCustomMultiValue("package_source_version", selected=True),
                        DashboardVariableCustomMultiValue("package_title", selected=True),
                        DashboardVariableCustomMultiValue("message", selected=True),
                        DashboardVariableCustomMultiValue("package_try_no", selected=True),
                        DashboardVariableCustomMultiValue("source_system_name", selected=True),
                        DashboardVariableCustomMultiValue("destination_system_name", selected=True),
                        DashboardVariableCustomMultiValue("package_exchange", selected=True),
                        DashboardVariableCustomMultiValue("package_routing_key", selected=True),
                        DashboardVariableCustomMultiValue("package_id"),
                        DashboardVariableCustomMultiValue("level"),
                        DashboardVariableCustomMultiValue("source_system"),
                        DashboardVariableCustomMultiValue("destination_system"),
                        DashboardVariableCustomMultiValue("queue")
                    ],
                    label="fields"
                ),
                DashboardVariableText(
                    name="var_source_version",
                    label="source version"
                ),
                DashboardVariableText(
                    name="var_try_no",
                    label="try no >=",
                    value="0"
                ),
                DashboardVariableQuery(
                    name="var_title",
                    query="\n".join([
                        "select distinct package_title",
                        "from log.databuslog",
                        "where package_title != ''",
                        "order by package_title"
                    ]),
                    datasource="loghouse",
                    label="title",
                    multi=True,
                    include_all=DashboardVariableQueryOptionIncludeAll()
                ),
                DashboardVariableQuery(
                    name="var_destination_system",
                    query="\n".join([
                        "select distinct destination_system",
                        "from log.databuslog",
                        "where destination_system != ''",
                        "order by destination_system"
                    ]),
                    datasource="loghouse",
                    label="destination_system",
                    multi=True,
                    include_all=DashboardVariableQueryOptionIncludeAll()
                ),
                DashboardVariableQuery(
                    name="var_destination_system_name",
                    query="\n".join([
                        "select distinct destination_system_name",
                        "from log.databuslog",
                        "where destination_system_name != ''",
                        "order by destination_system_name"
                    ]),
                    datasource="loghouse",
                    label="destination_system_name",
                    multi=True,
                    include_all=DashboardVariableQueryOptionIncludeAll()
                ),
                DashboardVariableQuery(
                    name="var_exchange",
                    query="\n".join([
                        "select distinct package_exchange",
                        "from log.databuslog",
                        "where package_exchange != ''",
                        "order by package_exchange"
                    ]),
                    datasource="loghouse",
                    label="exchange",
                    multi=True,
                    include_all=DashboardVariableQueryOptionIncludeAll()
                ),
                DashboardVariableQuery(
                    name="var_routing_key",
                    query="\n".join([
                        "select distinct package_routing_key",
                        "from log.databuslog",
                        "where package_routing_key != ''",
                        "order by package_routing_key"
                    ]),
                    datasource="loghouse",
                    label="routing_key",
                    multi=True,
                    include_all=DashboardVariableQueryOptionIncludeAll()
                ),
                DashboardVariableQuery(
                    name="var_queue",
                    query="\n".join([
                        "select distinct queue",
                        "from log.databuslog",
                        "where queue != ''",
                        "order by queue"
                    ]),
                    datasource="loghouse",
                    label="queue",
                    multi=True,
                    include_all=DashboardVariableQueryOptionIncludeAll()
                )
            ]
        ).save(build_dir)
