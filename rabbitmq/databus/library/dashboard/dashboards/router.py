from python.utils.validators import validate_arg

from grafana.library.constants import GrafanaConstants
from grafana.library.dashboard.dashboard import Dashboard
from grafana.library.panel.table.column.label import PanelTableColumnLabel
from grafana.library.panel.table.style import PanelTableColor
from grafana.library.panel.table.style import PanelTableColorMode
from grafana.library.panel.table.style import PanelTableStyleHidden
from grafana.library.panel.table.style import PanelTableStyleNumber
from grafana.library.panel.table.style import PanelTableStyleString
from grafana.library.prometheus.filter import PrometheusFilter
from grafana.library.prometheus.panel.table.table import PrometheusPanelTable
from grafana.library.prometheus.panel.table.table import PrometheusPanelTableColumnData

from rabbitmq.databus.library.dashboard.constants import RmqConstants

class RmqRouterDashboard:
    def __init__(self):
        super().__init__()

        self.y_routes_table = 0
        self.h_routes_table = 13
        self.y_problem_routes_table = self.y_routes_table + self.h_routes_table
        self.h_problem_routes_table = 13

    def group_decorator(self, query):
        validate_arg(query, [str], "query")

        return "label_replace(%s, \"group\", \"$1->$2\", \"name\", \"^rmq:(.+):.+->rmq:(.+):.+$\")" % query

    def build_routes_table(self, panel_count_in_row, panel_no, circuit_no, f):
        validate_arg(panel_count_in_row, [int], "panel_count_in_row")
        validate_arg(panel_no, [int], "panel_no")
        validate_arg(circuit_no, [str], "circuit_no")
        validate_arg(f, [PrometheusFilter], "f")

        by = " by (group)"
        f = f.add("circuit", "=", circuit_no)
        f = f.add("job", "=", "rmq-databus-router")

        data_columns = []
        label_columns = []
        label_columns.append(PanelTableColumnLabel("Time", PanelTableStyleHidden()))

        data_columns.append(PrometheusPanelTableColumnData(
            expr="count(%s > 0)%s" % (
                self.group_decorator(
                    "min_over_time(%s)" % f.metric("databus_backend_up", RmqConstants.SMOOTHING_PERIOD)
                ),
                by
            ),
            style=PanelTableStyleString(
                alias="cnt.up"
            )
        ))

        data_columns.append(PrometheusPanelTableColumnData(
            expr="count(%s < 1)%s" % (
                self.group_decorator(
                    "min_over_time(%s)" % f.metric("databus_backend_up", RmqConstants.SMOOTHING_PERIOD)
                ),
                by
            ),
            style=PanelTableStyleString(
                alias="cnt.down",
                color=PanelTableColor(PanelTableColorMode.CELL, 1, 1)
            )
        ))

        data_columns.append(PrometheusPanelTableColumnData(
            expr="min(%s)%s" % (
                self.group_decorator(
                    "min_over_time(%s)" % f.metric("databus_backend_uptime", RmqConstants.SMOOTHING_PERIOD)
                ),
                by
            ),
            style=PanelTableStyleNumber(
                alias="uptime",
                unit="s",
                color=RmqConstants.COLOR_UPTIME
            )
        ))

        return PrometheusPanelTable(
            x=GrafanaConstants.X[panel_count_in_row - 1][panel_no],
            y=self.y_routes_table,
            w=GrafanaConstants.W[panel_count_in_row - 1],
            h=self.h_routes_table,
            datasource="prometheus",
            title="routes (circuit%s)" % circuit_no,
            fontSize="65%",
            data_columns=data_columns,
            label_columns=label_columns
        )

    def build_problem_routes_table(self, panel_count_in_row, panel_no, circuit_no, f):
        validate_arg(panel_count_in_row, [int], "panel_count_in_row")
        validate_arg(panel_no, [int], "panel_no")
        validate_arg(circuit_no, [str], "circuit_no")
        validate_arg(f, [PrometheusFilter], "f")

        by = " by (name)"
        f = f.add("circuit", "=", circuit_no)
        f = f.add("job", "=", "rmq-databus-router")

        data_columns = []
        label_columns = []
        label_columns.append(PanelTableColumnLabel("Time", PanelTableStyleHidden()))

        data_columns.append(PrometheusPanelTableColumnData(
            expr="min(min_over_time(%s))%s < 120" % (
                f.metric("databus_backend_uptime", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleNumber(
                alias="uptime",
                unit="s",
                color=RmqConstants.COLOR_UPTIME
            )
        ))

        return PrometheusPanelTable(
            x=GrafanaConstants.X[panel_count_in_row - 1][panel_no],
            y=self.y_problem_routes_table,
            w=GrafanaConstants.W[panel_count_in_row - 1],
            h=self.h_problem_routes_table,
            datasource="prometheus",
            title="problem routes (circuit%s)" % circuit_no,
            fontSize="65%",
            data_columns=data_columns,
            label_columns=label_columns
        )

    def build(self, build_dir):
        validate_arg(build_dir, [str], "build_dir")

        f = PrometheusFilter()

        Dashboard(
            uid=RmqConstants.RMQ_ROUTER_UID,
            title=RmqConstants.RMQ_ROUTER_TITLE,
            panels=[
                self.build_routes_table(2, 0, "01", f=f),
                self.build_routes_table(2, 1, "02", f=f),
                self.build_problem_routes_table(2, 0, "01", f=f),
                self.build_problem_routes_table(2, 1, "02", f=f)
            ],
            variables=[
            ]
        ).save(build_dir)
