from python.utils.validators import validate_arg

from grafana.library.constants import GrafanaConstants
from grafana.library.dashboard.dashboard import Dashboard
from grafana.library.dashboard.link import DashboardLink
from grafana.library.dashboard.variable.option.include_all import DashboardVariableQueryOptionIncludeAll
from grafana.library.dashboard.variable.query import DashboardVariableQuery
from grafana.library.panel.table.column.label import PanelTableColumnLabel
from grafana.library.panel.table.style import PanelTableLink
from grafana.library.panel.table.style import PanelTableStyleHidden
from grafana.library.panel.table.style import PanelTableStyleNumber
from grafana.library.prometheus.filter import PrometheusFilter
from grafana.library.prometheus.panel.table.table import PrometheusPanelTable
from grafana.library.prometheus.panel.table.table import PrometheusPanelTableColumnData

from rabbitmq.databus.library.dashboard.constants import RmqConstants

class RmqVhostDashboard:
    def __init__(self):
        super().__init__()

        self.y_system_table = 0
        self.h_system_table = 6
        self.y_queue_table = self.y_system_table + self.h_system_table
        self.h_queue_table = 13

    def system_decorator(self, query):
        validate_arg(query, [str], "query")

        return "label_replace(%s, \"system\", \"$1\", \"queue\", \"q_([a-z]+)_.+\")" % query

    def create_link_factory(self, vhost_cell, system_cell):
        return lambda tooltip: PanelTableLink(
            url="/grafana/d/%s/%s?orgId=1&refresh=30s&var-vhost=%s&var-system=%s" % (
                RmqConstants.RMQ_VHOST_UID, RmqConstants.RMQ_VHOST_UID, vhost_cell, system_cell
            ),
            tooltip=tooltip
        )

    def build_system_table(self, panel_count_in_row, panel_no, circuit_no, f):
        validate_arg(panel_count_in_row, [int], "panel_count_in_row")
        validate_arg(panel_no, [int], "panel_no")
        validate_arg(circuit_no, [str], "circuit_no")
        validate_arg(f, [PrometheusFilter], "f")

        by = " by (vhost, system)"
        link_factory = self.create_link_factory("${__cell_2}", "${__cell_1}")

        f = f.add("circuit", "=", circuit_no)
        f = f.add("queue", "=~", "^q_(${system})_.+$")

        data_columns = []
        label_columns = []
        label_columns.append(PanelTableColumnLabel("Time", PanelTableStyleHidden()))
        label_columns.append(PanelTableColumnLabel("vhost", PanelTableStyleHidden()))

        _f = f.add("job", "=", "rmq-rabbitmq-server")
        _f = _f.add("queue", "=~", "^.+_retry$")
        data_columns.append(PrometheusPanelTableColumnData(
            expr="sum(%s)%s" % (
                self.system_decorator(
                    "max_over_time(%s)" % _f.metric("rabbitmq_queue_messages", RmqConstants.SMOOTHING_PERIOD)
                ),
                by
            ),
            style=PanelTableStyleNumber(
                alias="msg.retry",
                link=link_factory("Кол-во сообщений в очереди ошибок"),
                color=RmqConstants.COLOR_MESSAGE_READY
            )
        ))

        _f = f.add("job", "=", "rmq-rabbitmq-server")
        _f = _f.add("queue", "!~", "^.+_retry$")
        data_columns.append(PrometheusPanelTableColumnData(
            expr="sum(%s)%s" % (
                self.system_decorator(
                    "max_over_time(%s)" % _f.metric("rabbitmq_queue_messages", RmqConstants.SMOOTHING_PERIOD)
                ),
                by
            ),
            style=PanelTableStyleNumber(
                alias="msg.ready",
                link=link_factory("Кол-во сообщений готовых к обработке"),
                color=RmqConstants.COLOR_MESSAGE_READY
            )
        ))

        return PrometheusPanelTable(
            x=GrafanaConstants.X[panel_count_in_row - 1][panel_no],
            y=self.y_system_table,
            w=GrafanaConstants.W[panel_count_in_row - 1],
            h=self.h_system_table,
            datasource="prometheus",
            title="system (circuit%s)" % circuit_no,
            fontSize="65%",
            data_columns=data_columns,
            label_columns=label_columns
        )

    def build_queue_table(self, panel_count_in_row, panel_no, circuit_no, f):
        validate_arg(panel_count_in_row, [int], "panel_count_in_row")
        validate_arg(panel_no, [int], "panel_no")
        validate_arg(circuit_no, [str], "circuit_no")
        validate_arg(f, [PrometheusFilter], "f")

        by = " by (vhost, system, queue)"
        link_factory = self.create_link_factory("${__cell_3}", "${__cell_2}")

        f = f.add("circuit", "=", circuit_no)
        f = f.add("queue", "=~", "^q_(${system})_.+$")

        data_columns = []
        label_columns = []
        label_columns.append(PanelTableColumnLabel("Time", PanelTableStyleHidden()))
        label_columns.append(PanelTableColumnLabel("vhost", PanelTableStyleHidden()))

        _f = f.add("job", "=", "rmq-rabbitmq-server")
        data_columns.append(PrometheusPanelTableColumnData(
            expr="sum(%s)%s" % (
                self.system_decorator(
                    "max_over_time(%s)" % _f.metric("rabbitmq_queue_messages", RmqConstants.SMOOTHING_PERIOD)
                ),
                by
            ),
            style=PanelTableStyleNumber(
                alias="msq.total",
                link=link_factory("Общее кол-во сообщений"),
                color=RmqConstants.COLOR_MESSAGE_READY
            )
        ))

        return PrometheusPanelTable(
            x=GrafanaConstants.X[panel_count_in_row - 1][panel_no],
            y=self.y_queue_table,
            w=GrafanaConstants.W[panel_count_in_row - 1],
            h=self.h_queue_table,
            datasource="prometheus",
            title="queue (circuit%s)" % circuit_no,
            fontSize="65%",
            data_columns=data_columns,
            label_columns=label_columns
        )

    def build(self, build_dir):
        validate_arg(build_dir, [str], "build_dir")

        f = PrometheusFilter().add("vhost", "=", "${vhost}")

        _f = PrometheusFilter().add("job", "=", "rmq-rabbitmq-server")
        Dashboard(
            uid=RmqConstants.RMQ_VHOST_UID,
            title=RmqConstants.RMQ_VHOST_TITLE,
            panels=[
                self.build_system_table(2, 0, "01", f=f),
                self.build_system_table(2, 1, "02", f=f),
                self.build_queue_table(2, 0, "01", f=f),
                self.build_queue_table(2, 1, "02", f=f)
            ],
            variables=[
                DashboardVariableQuery(
                    name="vhost",
                    datasource="prometheus",
                    query="label_values(%s, vhost)" % (
                        _f.metric("rabbitmq_queue_messages")
                    )
                ),
                DashboardVariableQuery(
                    name="system",
                    datasource="prometheus",
                    query="label_values(%s, queue)" % (
                        _f.add("vhost", "=", "${vhost}").metric("rabbitmq_queue_messages")
                    ),
                    regex="/^q_([a-z]+)_.+$/",
                    multi=True,
                    include_all=DashboardVariableQueryOptionIncludeAll()
                ),
                DashboardVariableQuery(
                    name="zone",
                    datasource="prometheus",
                    query="label_values(%s, zone)" % (
                        _f.add("vhost", "=", "${vhost}").metric("rabbitmq_queue_messages")
                    ),
                    hide=True
                )
            ],
            links=[
                DashboardLink(
                    url="/grafana/d/rmq-main/%s?orgId=1&refresh=30s" % RmqConstants.RMQ_MAIN_UID,
                    title=RmqConstants.RMQ_MAIN_TITLE
                ),
                DashboardLink(
                    url="/grafana/d/%s/%s?orgId=1&refresh=30s&var-zone=${zone}" % (
                        RmqConstants.RMQ_ZONE_UID, RmqConstants.RMQ_ZONE_TITLE
                    ),
                    title=RmqConstants.RMQ_ZONE_TITLE
                )
            ]
        ).save(build_dir)
