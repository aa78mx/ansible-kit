from python.utils.validators import validate_arg

from grafana.library.constants import GrafanaConstants
from grafana.library.dashboard.dashboard import Dashboard
from grafana.library.dashboard.timepicker import DashboardTimepickerHidden
from grafana.library.panel.row.row import PanelRow
from grafana.library.panel.table.column.label import PanelTableColumnLabel
from grafana.library.panel.table.style import PanelTableColor
from grafana.library.panel.table.style import PanelTableColorMode
from grafana.library.panel.table.style import PanelTableLink
from grafana.library.panel.table.style import PanelTableStyleHidden
from grafana.library.panel.table.style import PanelTableStyleNumber
from grafana.library.panel.table.style import PanelTableStyleString
from grafana.library.panel.table.style import PanelTableValueMaps
from grafana.library.prometheus.filter import PrometheusFilter
from grafana.library.prometheus.panel.table.table import PrometheusPanelTable
from grafana.library.prometheus.panel.table.table import PrometheusPanelTableColumnData

from rabbitmq.databus.library.dashboard.constants import RmqConstants

class RmqMainDashboard:
    def __init__(self, systems):
        super().__init__()

        validate_arg(systems, [dict], "systems")

        self.systems = systems

        self.y_router_table = 0
        self.h_router_table = 4
        self.y_databus_table = self.y_router_table + self.h_router_table
        self.h_databus_table = 16
        self.y_message_table = self.y_databus_table + self.h_databus_table
        self.h_message_table = 2 + round(1.8 * len(systems))

    def build_router_table(self, panel_count_in_row, panel_no, circuit_no, f):
        validate_arg(panel_count_in_row, [int], "panel_count_in_row")
        validate_arg(panel_no, [int], "panel_no")
        validate_arg(circuit_no, [str], "circuit_no")
        validate_arg(f, [PrometheusFilter], "f")

        by = " by (hostname)"
        link_factory = lambda tooltip: PanelTableLink(
            url="/grafana/d/%s/%s?orgId=1&refresh=30s" % (
                RmqConstants.RMQ_ROUTER_UID, RmqConstants.RMQ_ZONE_TITLE
            ),
            tooltip=tooltip
        )
        f = f.add("circuit", "=", circuit_no)

        data_columns = []
        label_columns = []
        label_columns.append(PanelTableColumnLabel("Time", PanelTableStyleHidden()))

        _f = f.add("job", "=", "rmq-databus-router")

        data_columns.append(PrometheusPanelTableColumnData(
            expr="sum(max_over_time(%s) > 0)%s" % (
                _f.metric("databus_unexpected_errors", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleNumber(
                alias="err",
                unit="short",
                link=link_factory("К-во критических ошибок в router"),
                color=PanelTableColor(PanelTableColorMode.CELL, 1, 1)
            )
        ))

        data_columns.append(PrometheusPanelTableColumnData(
            expr="sum(max_over_time(%s))%s" % (
                _f.metric("process_resident_memory_bytes", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleNumber(
                alias="rss",
                unit="bytes",
                link=link_factory("Объем RSS"),
                color=PanelTableColor(
                    mode=PanelTableColorMode.CELL,
                    a=GrafanaConstants.BYTES_IN_GiB * 0.5,
                    b=GrafanaConstants.BYTES_IN_GiB * 1
                )
            )
        ))

        data_columns.append(PrometheusPanelTableColumnData(
            expr="max(max_over_time(%s))%s" % (
                _f.metric("up", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleString(
                alias="state",
                link=link_factory("Состояние router"),
                color=PanelTableColor(PanelTableColorMode.CELL, 1, 1, True),
                maps=PanelTableValueMaps({"0": "down", "1": "up"})
            )
        ))

        data_columns.append(PrometheusPanelTableColumnData(
            expr="min(min_over_time(%s))%s" % (
                _f.metric("databus_uptime", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleNumber(
                alias="uptime",
                unit="s",
                link=link_factory("Время с последней перезагрузки"),
                color=RmqConstants.COLOR_UPTIME
            )
        ))

        data_columns.append(PrometheusPanelTableColumnData(
            expr="min(min_over_time(%s))%s" % (
                _f.metric("databus_backend_uptime", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleNumber(
                alias="route.uptime",
                unit="s",
                link=link_factory("Время с последней перезагрузки маршрута"),
                color=RmqConstants.COLOR_UPTIME
            )
        ))

        data_columns.append(PrometheusPanelTableColumnData(
            expr="count(min_over_time(%s) > 0)%s" % (
                _f.metric("databus_backend_up", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleNumber(
                alias="route.up",
                link=link_factory("К-во работающих маршрутов")
            )
        ))

        data_columns.append(PrometheusPanelTableColumnData(
            expr="count(min_over_time(%s) < 1)%s" % (
                _f.metric("databus_backend_up", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleNumber(
                alias="route.down",
                link=link_factory("К-во не работающих маршрутов"),
                color=PanelTableColor(PanelTableColorMode.CELL, 1, 1)
            )
        ))

        return PrometheusPanelTable(
            x=GrafanaConstants.X[panel_count_in_row - 1][panel_no],
            y=self.y_router_table,
            w=GrafanaConstants.W[panel_count_in_row - 1],
            h=self.h_router_table,
            datasource="prometheus",
            title="router (circuit%s)" % circuit_no,
            fontSize="65%",
            data_columns=data_columns,
            label_columns=label_columns
        )

    def build_databus_table(self, panel_count_in_row, panel_no, circuit_no, f):
        validate_arg(panel_count_in_row, [int], "panel_count_in_row")
        validate_arg(panel_no, [int], "panel_no")
        validate_arg(circuit_no, [str], "circuit_no")
        validate_arg(f, [PrometheusFilter], "f")

        by = " by (zone, hostname)"
        link_factory = lambda tooltip: PanelTableLink(
            url="/grafana/d/%s/%s?orgId=1&refresh=30s&var-zone=${__cell_2}" % (
                RmqConstants.RMQ_ZONE_UID, RmqConstants.RMQ_ZONE_TITLE
            ),
            tooltip=tooltip
        )
        f = f.add("circuit", "=", circuit_no)

        data_columns = []
        label_columns = []
        label_columns.append(PanelTableColumnLabel("Time", PanelTableStyleHidden()))
        label_columns.append(PanelTableColumnLabel("zone", PanelTableStyleHidden()))

        _f = f.add("job", "=~", "^rmq-databus-(backend|frontend)$")
        data_columns.append(PrometheusPanelTableColumnData(
            expr="sum(max_over_time(%s) > 0)%s" % (
                _f.metric("databus_unexpected_errors", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleNumber(
                alias="err",
                unit="short",
                link=link_factory("К-во критических ошибок в databus"),
                color=PanelTableColor(PanelTableColorMode.CELL, 1, 1)
            )
        ))

        _f = f.add("job", "=", "rmq-fluentd")
        _f = _f.add("tag", "=~", "^(fluent.error|fluent.warn)$")
        data_columns.append(PrometheusPanelTableColumnData(
            expr="sum(max_over_time(%s) > 0)%s" % (
                _f.metric("fluentd_input_messages_total", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleNumber(
                alias="fd.err",
                unit="short",
                link=link_factory("К-во ошибок в fluentd"),
                color=PanelTableColor(PanelTableColorMode.CELL, 1, 1)
            )
        ))

        _f = f.add("job", "=", "rmq-rabbitmq-server")
        data_columns.append(PrometheusPanelTableColumnData(
            expr="min(min_over_time(%s) - max_over_time(%s))%s" % (
                _f.metric("rabbitmq_disk_space_available_bytes", RmqConstants.SMOOTHING_PERIOD),
                _f.metric("rabbitmq_disk_space_available_limit_bytes", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleNumber(
                alias="df",
                unit="bytes",
                link=link_factory("Объем свободного места на диске"),
                color=PanelTableColor(
                    mode=PanelTableColorMode.CELL,
                    a=GrafanaConstants.BYTES_IN_GiB * 10,
                    b=GrafanaConstants.BYTES_IN_GiB * 20,
                    invert=True
                )
            )
        ))

        _f = f.add("job", "=", "rmq-rabbitmq-server")
        data_columns.append(PrometheusPanelTableColumnData(
            expr="max(max_over_time(%s) / min_over_time(%s))%s" % (
                _f.metric("rabbitmq_process_resident_memory_bytes", RmqConstants.SMOOTHING_PERIOD),
                _f.metric("rabbitmq_resident_memory_limit_bytes", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleNumber(
                alias="rmq.rss",
                unit="percentunit",
                link=link_factory("% использованного RSS процессом rabbitmq-server"),
                color=PanelTableColor(PanelTableColorMode.CELL, 0.5, 0.7)
            )
        ))

        _f = f.add("job", "=", "rmq-rabbitmq-server")
        data_columns.append(PrometheusPanelTableColumnData(
            expr="max(max_over_time(%s))%s" % (
                _f.metric("rabbitmq_connections", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleNumber(
                alias="rmq.conn",
                link=link_factory("Количество соединений с rabbitmq-server"),
                color=PanelTableColor(PanelTableColorMode.CELL, 300, 1000)
            )
        ))

        _f = f.add("job", "=~", "^rmq-databus-(backend|frontend)$")
        data_columns.append(PrometheusPanelTableColumnData(
            expr="sum(max_over_time(%s))%s" % (
                _f.metric("process_resident_memory_bytes", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleNumber(
                alias="fe+be.rss",
                unit="bytes",
                link=link_factory("Объем RSS, который используют frontend и backend"),
                color=PanelTableColor(
                    mode=PanelTableColorMode.CELL,
                    a=GrafanaConstants.BYTES_IN_GiB,
                    b=GrafanaConstants.BYTES_IN_GiB * 1.5
                )
            )
        ))

        _f = f.add("job", "=", "rmq-rabbitmq-server")
        data_columns.append(PrometheusPanelTableColumnData(
            expr="max(max_over_time(%s))%s" % (
                _f.metric("up", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleString(
                alias="rmq.state",
                link=link_factory("Состояние rabbitmq-server"),
                color=PanelTableColor(PanelTableColorMode.CELL, 1, 1, True),
                maps=PanelTableValueMaps({"0": "down", "1": "up"})
            )
        ))

        _f = f.add("job", "=", "rmq-databus-frontend")
        data_columns.append(PrometheusPanelTableColumnData(
            expr="max(max_over_time(%s))%s" % (
                _f.metric("up", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleString(
                alias="fe.state",
                link=link_factory("Состояние frontend"),
                color=PanelTableColor(PanelTableColorMode.CELL, 1, 1, True),
                maps=PanelTableValueMaps({"0": "down", "1": "up"})
            )
        ))

        _f = f.add("job", "=", "rmq-databus-backend")
        data_columns.append(PrometheusPanelTableColumnData(
            expr="max(max_over_time(%s))%s" % (
                _f.metric("up", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleString(
                alias="be.state",
                link=link_factory("Состояние backend"),
                color=PanelTableColor(PanelTableColorMode.CELL, 1, 1, True),
                maps=PanelTableValueMaps({"0": "down", "1": "up"})
            )
        ))

        _f = f.add("job", "=", "rmq-fluentd")
        data_columns.append(PrometheusPanelTableColumnData(
            expr="max(max_over_time(%s))%s" % (
                _f.metric("up", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleString(
                alias="fd.state",
                link=link_factory("Состояние fluentd"),
                color=PanelTableColor(PanelTableColorMode.CELL, 1, 1, True),
                maps=PanelTableValueMaps({"0": "down", "1": "up"})
            )
        ))

        _f = f.add("job", "=~", "^rmq-(rabbitmq-server|databus-backend|databus-frontend)$")
        _f = _f.add("__name__", "=~", "^rabbitmq_erlang_uptime_seconds|databus_uptime$")
        data_columns.append(PrometheusPanelTableColumnData(
            expr="min(min_over_time(%s))%s" % (
                _f.metric("", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleNumber(
                alias="uptime",
                unit="s",
                link=link_factory("Время с последней перезагрузки"),
                color=RmqConstants.COLOR_UPTIME
            )
        ))

        _f = f.add("job", "=", "rmq-rabbitmq-server")
        _f = _f.add("queue", "=~", "^q_rmq_.*$")
        data_columns.append(PrometheusPanelTableColumnData(
            expr="sum(max_over_time(%s))%s" % (
                _f.metric("rabbitmq_queue_messages", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleNumber(
                alias="msg.rmq",
                link=link_factory("Кол-во сообщений для отправки на другой rabbitmq"),
                color=RmqConstants.COLOR_MESSAGE_READY
            )
        ))

        _f = f.add("job", "=", "rmq-rabbitmq-server")
        _f = _f.add("queue", "!~", "q_rmq_.*")
        data_columns.append(PrometheusPanelTableColumnData(
            expr="sum(max_over_time(%s))%s" % (
                _f.metric("rabbitmq_queue_messages", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleNumber(
                alias="msg.others",
                link=link_factory("Кол-во сообщений для обработки конечными системами"),
                color=RmqConstants.COLOR_MESSAGE_READY
            )
        ))

        return PrometheusPanelTable(
            x=GrafanaConstants.X[panel_count_in_row - 1][panel_no],
            y=self.y_databus_table,
            w=GrafanaConstants.W[panel_count_in_row - 1],
            h=self.h_databus_table,
            datasource="prometheus",
            title="databus (circuit%s)" % circuit_no,
            fontSize="65%",
            data_columns=data_columns,
            label_columns=label_columns
        )

    def build_message_table(self, f):
        validate_arg(f, [PrometheusFilter], "f")

        by = " by (zone, vhost)"
        link_factory = lambda tooltip, system: PanelTableLink(
            url="/grafana/d/%s/%s?orgId=1&refresh=30s&var-vhost=${__cell_1}&var-system=%s" % (
                RmqConstants.RMQ_VHOST_UID, RmqConstants.RMQ_VHOST_TITLE, system
            ),
            tooltip=tooltip
        )
        f = f.add("job", "=", "rmq-rabbitmq-server")

        data_columns = []
        label_columns = []
        label_columns.append(PanelTableColumnLabel("Time", PanelTableStyleHidden()))

        for system_id, system in sorted(self.systems.items()):
            _f = f.add("queue", "=~", "^q_%s_.*$" % system_id)
            data_columns.append(PrometheusPanelTableColumnData(
                expr="sum(max_over_time(%s))%s" % (
                    _f.metric("rabbitmq_queue_messages", RmqConstants.SMOOTHING_PERIOD),
                    by
                ),
                style=PanelTableStyleNumber(
                    alias=system_id,
                    link=link_factory(
                        tooltip="Кол-во сообщений для обработки системой \"%s\"" % system.get("title"),
                        system=system_id
                    ),
                    color=RmqConstants.COLOR_MESSAGE_READY
                )
            ))

        _f = f.add("queue", "!~", "^q_rmq_.*$")
        data_columns.append(PrometheusPanelTableColumnData(
            expr="sum(max_over_time(%s))%s" % (
                _f.metric("rabbitmq_queue_messages", RmqConstants.SMOOTHING_PERIOD),
                by
            ),
            style=PanelTableStyleNumber(
                alias="total",
                link=link_factory("Общее кол-во сообщений", "All"),
                color=RmqConstants.COLOR_MESSAGE_READY
            )
        ))

        return PrometheusPanelTable(
            x=GrafanaConstants.X[0][0],
            y=self.y_message_table,
            w=GrafanaConstants.W[0],
            h=self.h_message_table,
            datasource="prometheus",
            title="messages",
            fontSize="65%",
            data_columns=data_columns,
            label_columns=label_columns
        )

    def build(self, build_dir):
        validate_arg(build_dir, [str], "build_dir")

        f = PrometheusFilter()

        Dashboard(
            uid=RmqConstants.RMQ_MAIN_UID,
            title=RmqConstants.RMQ_MAIN_TITLE,
            timepicker=DashboardTimepickerHidden(),
            panels=[
                PanelRow(
                    y=self.y_router_table,
                    title="router and databus"
                ),
                self.build_router_table(2, 0, "01", f=f),
                self.build_router_table(2, 1, "02", f=f),
                self.build_databus_table(2, 0, "01", f=f),
                self.build_databus_table(2, 1, "02", f=f),
                PanelRow(
                    y=self.y_message_table,
                    title="message"
                ),
                self.build_message_table(f=f)
            ],
            variables=[
            ]
        ).save(build_dir)
