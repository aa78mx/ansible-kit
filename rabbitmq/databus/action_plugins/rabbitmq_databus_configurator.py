import os
import re
import sys
import yaml

sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), "../../../")))

from ansible.playbook.task import Task
from ansible.plugins.action import ActionBase
from ansible.template import Templar
from ansible.utils.display import Display
from ansible.utils.listify import listify_lookup_plugin_terms

from python.error import Error
from python.utils.args import Args

from rabbitmq.databus.library.configurator.configurator import Configurator

display = Display()

class ActionModule(ActionBase):
    def include(self, include_task_args, configurator, task_vars):
        include_task_args.validate({
            "state": {"type": str},
            "path": {"type": str}
        })

        path = include_task_args.get___("path").as_text()

        if os.path.isdir(path):
            for root, dirs, files in os.walk(path):
                for file_name in files:
                    file_path = os.path.join(root, file_name)
                    display.display("  - include %s" % file_path, newline=False, color="dark gray")
                    self.include(
                        include_task_args=Args({
                            "state": "include",
                            "path": file_path
                        }),
                        configurator=configurator,
                        task_vars=task_vars
                    )
                    self._display.display(" - ok", color="dark gray")

            return

        with open(path, "r") as stream:
            tasks = yaml.safe_load(stream)
            for task_args in tasks:
                task_args = Args(task_args)
                task_args.validate({
                    "rabbitmq_databus_configurator": {"type": dict},
                    "with_cartesian": {"type": list, "optional": True},
                    "with_items": {"type": list, "optional": True},
                    "when": {"type": [list, str], "optional": True},
                    "vars": {"type": dict, "optional": True}
                })

                action = task_args.get___("rabbitmq_databus_configurator").as_dict()

                task = Task().load_data(
                    ds=task_args.origin,
                    variable_manager=self._task.get_variable_manager(),
                    loader=self._task.get_loader()
                )

                templar = Templar(
                    loader=self._loader,
                    shared_loader_obj=self._shared_loader_obj,
                    variables=task_vars
                )

                if (task.loop_with is not None):
                    loopTerms = listify_lookup_plugin_terms(
                        terms=task.loop,
                        templar=templar,
                        loader=self._loader,
                        fail_on_undefined=True,
                        convert_bare=False
                    )

                    lookup = self._shared_loader_obj.lookup_loader.get(
                        task.loop_with,
                        loader=self._loader,
                        templar=templar
                    )

                    items = lookup.run(terms=loopTerms, wantlist=True)
                    for item in items:
                        self.execute(configurator, task, templar, action, {
                            "item": item
                        })
                else:
                    self.execute(configurator, task, templar, action, {})

    def execute(self, configurator, task, templar, action, variables):
        if not task.evaluate_conditional(templar, variables):
            return

        templar.available_variables = variables
        action_args = Args(templar.template(action))

        state = action_args.get___("state").as_text()
        if (not isinstance(state, str)):
            raise Error("[state] is required")

        if (state == "entity"):
            configurator.add_entity(action_args)
        elif (state == "route"):
            configurator.add_route(action_args)
        else:
            raise Error("unsupported state [%s]" % state)

    def run(self, tmp=None, task_vars=None):
        super(ActionModule, self).run(tmp, task_vars)

        state = self._task.args.get("state")
        if (not isinstance(state, str)):
            raise Error("[state] is required")

        configurator = Configurator(
            task_vars.get("rabbitmq_databus_configuration") if (state != "init") else None
        )

        try:
            if (state == "init"):
                # TODO: Обращение к private члену класса
                groups_all = self._task.get_variable_manager()._inventory.groups
                groups_filtered = {}
                for group_name, group in groups_all.items():
                    if (group_name not in ["all", "ungrouped"] and ("rabbitmq_databus_configurator" in group.vars)):
                        groups_filtered[group_name] = Args(group.vars["rabbitmq_databus_configurator"])

                configurator.init(Args(self._task.args), groups_filtered)
            elif (state == "entity"):
                configurator.add_entity(Args(self._task.args))
            elif (state == "route"):
                configurator.add_route(Args(self._task.args))
            elif (state == "include"):
                self.include(Args(self._task.args), configurator, task_vars)
            elif (state == "include_dir"):
                self.include_dir(Args(self._task.args), configurator, task_vars)
            elif (state == "create"):
                configurator.create(Args(self._task.args), "target/")
            else:
                raise Error("unsupported state [%s]" % state)
        except Error as e:
            return {
                "failed": True,
                "msg": re.sub("->\n +", "-> ", str(e))
            }
        except Exception as e:
            return {
                "failed": True,
                "msg": str(e)
            }

        return {
            "changed": False,
            "ansible_facts": {
                "rabbitmq_databus_configuration": configurator.meta
            }
        }
