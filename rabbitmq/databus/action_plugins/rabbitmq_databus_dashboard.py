import os
import sys

sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), "../../../")))

from ansible.plugins.action import ActionBase

from python.utils.args import Args

from rabbitmq.databus.library.dashboard.dashboards.main import RmqMainDashboard
from rabbitmq.databus.library.dashboard.dashboards.main_report import RmqMainReportDashboard
from rabbitmq.databus.library.dashboard.dashboards.message_log import RmqMessageLogDashboard
from rabbitmq.databus.library.dashboard.dashboards.router import RmqRouterDashboard
from rabbitmq.databus.library.dashboard.dashboards.state_on_date_report import RmqStateOnDateReportDashboard
from rabbitmq.databus.library.dashboard.dashboards.vhost import RmqVhostDashboard
from rabbitmq.databus.library.dashboard.dashboards.zone import RmqZoneDashboard

class ActionModule(ActionBase):
    def run(self, tmp=None, task_vars=None):
        super(ActionModule, self).run(tmp, task_vars)

        build_dir = "target/dashboard/"
        os.makedirs(build_dir, exist_ok=True)

        args = Args(self._task.args)

        args.validate({
            "systems": {"type": dict}
        })

        RmqMainDashboard(args.get("systems")).build(build_dir)
        RmqRouterDashboard().build(build_dir)
        RmqZoneDashboard().build(build_dir)
        RmqVhostDashboard().build(build_dir)
        RmqMessageLogDashboard().build(build_dir)
        RmqMainReportDashboard().build(build_dir)
        RmqStateOnDateReportDashboard().build(build_dir)

        return {
            "changed": False
        }
