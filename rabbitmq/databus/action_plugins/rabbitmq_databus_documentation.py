import os
import sys

sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), "../../../")))

from jinja2 import Template

from ansible.plugins.action import ActionBase

from python.utils.args import Args
from python.utils.path import normalize_dir_path
from python.utils.validators import validate_arg

from rabbitmq.databus.library.configurator.models.base import Meta
from rabbitmq.databus.library.configurator.models.base import NodeProfile
from rabbitmq.databus.library.configurator.models.base import SystemProfile

class Ref:
    def __init__(self, meta):
        super().__init__()

        validate_arg(meta, [Meta], "meta")

        self.meta = meta

    def system(self, item):
        if isinstance(item, str):
            system_id = self.meta.get_system_profile(item).system_id

            return "[%s](systems/%s)" % (system_id, system_id)
        else:
            validate_arg(item, [SystemProfile], "item")

            return "[%s (%s)](systems/%s)" % (item.title, item.system_id, item.system_id)

    def node(self, item):
        if isinstance(item, str):
            node_id = self.meta.get_node_profile(item).node_id

            return "[%s](nodes/%s)" % (node_id, node_id)
        else:
            validate_arg(item, [NodeProfile], "item")

            return "[%s (%s)](nodes/%s)" % (item.title, item.node_id, item.node_id)

    def zone(self, item):
        return "[%s](zones/%s)" % (item.zone_id, item.zone_id)

    def entity(self, item):
        if (item.title is None):
            return "[%s](entities/%s)" % (item.entity_id, item.entity_id)
        else:
            return "[%s - %s](entities/%s)" % (item.entity_id, item.title, item.entity_id)

class ActionModule(ActionBase):
    TEMPLATE_DIR = os.path.normpath(os.path.join(os.path.dirname(__file__), "../templates"))

    def run(self, tmp=None, task_vars=None):
        super(ActionModule, self).run(tmp, task_vars)

        args = Args(self._task.args)
        args.validate({
            "target_dir": {"type": str}
        })

        target_dir = normalize_dir_path(args.get("target_dir"))

        meta = task_vars.get("rabbitmq_databus_configuration")
        ref = Ref(meta)

        with open("%s/home.md.j2" % self.TEMPLATE_DIR, "r") as f:
            home_template = Template(f.read())

        with open("%s/node.md.j2" % self.TEMPLATE_DIR, "r") as f:
            node_template = Template(f.read())

        with open("%s/system.md.j2" % self.TEMPLATE_DIR, "r") as f:
            system_template = Template(f.read())

        with open("%s/zone.md.j2" % self.TEMPLATE_DIR, "r") as f:
            zone_template = Template(f.read())

        with open("%s/entity.md.j2" % self.TEMPLATE_DIR, "r") as f:
            entity_template = Template(f.read())

        os.makedirs(target_dir, exist_ok=True)
        with open("%s/home.md" % target_dir, "w") as f:
            f.write(home_template.render(meta=meta, ref=ref))

        os.makedirs("%s/nodes/" % target_dir, exist_ok=True)
        for node in meta.node_profiles.values():
            with open("%s/nodes/%s.md" % (target_dir, node.node_id), "w") as f:
                f.write(node_template.render(node=node))

        os.makedirs("%s/systems/" % target_dir, exist_ok=True)
        for system in meta.system_profiles.values():
            with open("%s/systems/%s.md" % (target_dir, system.system_id), "w") as f:
                f.write(system_template.render(system=system))

        os.makedirs("%s/entities/" % target_dir, exist_ok=True)
        for entity in meta.entities.values():
            with open("%s/entities/%s.md" % (target_dir, entity.entity_id), "w") as f:
                f.write(entity_template.render(entity=entity, meta=meta, ref=ref))

        os.makedirs("%s/zones/" % target_dir, exist_ok=True)
        for zone in meta.zone_profiles.values():
            with open("%s/zones/%s.md" % (target_dir, zone.zone_id), "w") as f:
                f.write(zone_template.render(zone=zone, meta=meta, ref=ref))

        return {
            "changed": False
        }
