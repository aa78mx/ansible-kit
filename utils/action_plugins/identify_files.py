import os
import sys
import re

sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), "../../")))

from ansible.plugins.action import ActionBase

from python.utils.args import Args
from python.utils.path import validate_dir_path

class Base:
    def __init__(self, args):
        super().__init__()

        args.validate({
            "path": {"type": str},
            "patterns": {"type": list}
        })

        self.patterns = args.get("patterns")
        self.path = validate_dir_path(args.get("path"))

    def run(self, execute_module):
        find_result = execute_module(self.find_options())

        if (find_result.get("failed")):
            return find_result

        unknown_files = []

        for f in find_result.get("files"):
            path = f.get("path")
            is_detected = False
            for pattern in self.patterns:
                if ((path == pattern) or re.match("^%s$" % pattern, path)):
                    is_detected = True
                    break
            if (not is_detected):
                unknown_files.append(path)

        if (len(unknown_files) > 0):
            return {
                "failed": True,
                "msg": "Unknown files: " + ", ".join(unknown_files)
            }
        else:
            return {
                "changed": False
            }

class Windows(Base):
    pass

class Linux(Base):
    def find_options(self):
        return {
            "name": "find",
            "args": {
                "path": self.path,
                "file_type": "any"
            }
        }

class ActionModule(ActionBase):
    def execute_module(self, options):
        return self._execute_module(
            module_name=options.get("name"),
            module_args=options.get("args"),
            task_vars=self.task_vars,
            tmp=self.tmp
        )

    def run(self, tmp=None, task_vars=None):
        super(ActionModule, self).run(tmp, task_vars)

        self.tmp = tmp
        self.task_vars = task_vars

        args = Args(self._task.args)

        if (self._connection.transport == "winrm"):
            return Windows(args).run(self.execute_module)
        else:
            return Linux(args).run(self.execute_module)
