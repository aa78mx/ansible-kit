import os
import re
import sys

sys.path.append(os.path.normpath(os.path.join(os.path.dirname(__file__), "../../")))

from jinja2.runtime import Undefined

from python.error import Error
from python.utils.path import normalize_dir_path
from python.utils.path import validate_dir_path
from python.utils.path import validate_file_path

class FilterModule(object):
    @staticmethod
    def not_empty_string(value):
        if (value == ""):
            raise Error("empty string instead of none")
        return value

    @staticmethod
    def do_equal(lhs, rhs):
        if (lhs is None):
            lhs = None
        elif isinstance(lhs, Undefined):
            lhs = None
        elif (lhs == ""):
            lhs = None

        if (rhs is None):
            rhs = None
        elif isinstance(rhs, Undefined):
            rhs = None
        elif (rhs == ""):
            rhs = None

        return lhs == rhs

    @staticmethod
    def nvl(value, default):
        if FilterModule.do_equal(value, None):
            return FilterModule.not_empty_string(default)
        else:
            return value

    @staticmethod
    def validate_dict(value):
        if (value is None):
            raise Error("none instead of dict")
        if (not isinstance(value, dict)):
            raise Error("[%s] instead of dict" % type(value))

        return value

    @staticmethod
    def validate_enum(value, *items):
        for item in items:
            if FilterModule.do_equal(value, FilterModule.not_empty_string(item)):
                return value

        raise Error("unexpected value")

    @staticmethod
    def validate_hash_sha256(value):
        if (not re.match(r"^sha256:[0-9a-z]{64}$", FilterModule.validate_text(value))):
            raise Error("[%s] instead of sha256 hash" % value)

        return value

    @staticmethod
    def validate_list(value):
        if (value is None):
            raise Error("none instead of list")
        if (not isinstance(value, list)):
            raise Error("[%s] instead of list" % type(value))

        return value

    @staticmethod
    def validate_text(value):
        if (value is None):
            raise Error("none instead of text")
        if (not isinstance(value, str)):
            raise Error("%s instead of text" % type(value))
        if ((len(value) <= 0)):
            raise Error("empty string instead of text")

        return value

    @staticmethod
    def decode(value, *items):
        size = len(items) // 2

        for i in range(size):
            k = FilterModule.not_empty_string(items[2 * i])
            v = FilterModule.not_empty_string(items[2 * i + 1])

            if FilterModule.do_equal(value, k):
                return v

        if (len(items) != size):
            return FilterModule.not_empty_string(items[2 * size])

        raise Error("match not found")

    @staticmethod
    def equal(lhs, rhs):
        return FilterModule.do_equal(lhs, FilterModule.not_empty_string(rhs))

    @staticmethod
    def is_distinct_from(lhs, rhs):
        return not FilterModule.do_equal(lhs, FilterModule.not_empty_string(rhs))

    @staticmethod
    def replace_backslash(text, replacement):
        return text.replace("\\", replacement)

    def filters(self):
        return {
            "equal": FilterModule.equal,
            "is_distinct_from": FilterModule.is_distinct_from,
            "replace_backslash": FilterModule.replace_backslash,

            "decode": FilterModule.decode,
            "nvl": FilterModule.nvl,

            "normalize_dir_path": normalize_dir_path,
            "validate_dir_path": validate_dir_path,
            "validate_file_path": validate_file_path,

            "validate_dict": FilterModule.validate_dict,
            "validate_enum": FilterModule.validate_enum,
            "validate_hash_sha256": FilterModule.validate_hash_sha256,
            "validate_list": FilterModule.validate_list,
            "validate_text": FilterModule.validate_text
        }
