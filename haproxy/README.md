# haproxy

```yml
tasks:
  - name: install | haproxy
    include_role:
      name: haproxy
    vars:
      haproxy_service_conf:
        "./":
          copy_files:
            - files-vault/haproxy/haproxy.pem
            - roles/haproxy/files/dhparam.pem
          template_files:
            - templates/haproxy/haproxy.cfg.j2
            - roles/haproxy/templates/global.cfg.j2
      haproxy_container_ports:
        - 80:80
        - 443:443
```
