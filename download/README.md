# download

```yml
tasks:
  - include_role:
      name: download
    vars:
      download_dest_file_location: /baz/
      download_dest_file_name: quux
      download_file_checksum: sha256:0000000000000000000000000000000000000000000000000000000000000000
      download_src_file_url: https://example.com/foo.bar
```
