# host

## 1. host/preinstall

```yml
- hosts: localhost
  tasks:
    - name: install | preinstall host
      include_role:
        name: host/preinstall
  vars:
    ansible_connection: local
```

## 2. host/install

```yml
tasks:
  - name: install | install host
    include_role:
      name: host/install
    vars:
      host_sysctl:
        net.ipv4.tcp_keepalive_time: 60
        net.ipv4.tcp_keepalive_intvl: 10
        net.ipv4.tcp_keepalive_probes: 6
```
