# keepalived

```yml
tasks:
  - name: install | keepalived
    include_role:
      name: keepalived
    vars:
      keepalived_service_conf:
        "./":
          template_files:
            - roles/keepalived/templates/keepalived.conf.j2
    tags:
      - keepalived
```
