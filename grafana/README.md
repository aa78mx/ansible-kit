# grafana

```yml
tasks:
  - name: install | grafana
    include_role:
      name: grafana
    vars:
      grafana_service_name: dashboard-grafana
      grafana_service_conf:
        "./":
          copy_files:
            - files/grafana/grafana.ini
        "./provisioning/dashboards/":
          copy_files:
            - files/grafana/dashboards/dashboards.yml
        "./provisioning/dashboards/bar/":
          copy_files:
            - files/grafana/dashboards/bar/bar-overview.json
        "./provisioning/datasources/":
          copy_files:
            - files/grafana/datasources/prometheus.yaml
      grafana_container_networks:
        dashboard:
```
