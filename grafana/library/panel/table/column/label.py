from python.utils.validators import validate_arg

from grafana.library.panel.table.style import PanelTableStyleHidden
from grafana.library.panel.table.style import PanelTableStyleString

class PanelTableColumnLabel():
    def __init__(self, pattern, style):
        super().__init__()

        validate_arg(pattern, [str], "pattern")
        validate_arg(style, [PanelTableStyleHidden, PanelTableStyleString], "style")

        self.pattern = pattern
        self.style = style

    def serialize(self):
        return self.style.serialize(self.pattern)
