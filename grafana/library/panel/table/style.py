from enum import Enum

from python.utils.validators import validate_arg

from grafana.library.not_used import NotUsed

class PanelTableLink():
    def __init__(self, url: str, tooltip=""):
        super().__init__()

        validate_arg(url, [str], "url")
        validate_arg(tooltip, [str], "tooltip")

        self.url = url
        self.tooltip = tooltip

    def withTooltip(self, tooltip):
        validate_arg(tooltip, [str], "tooltip")

        return PanelTableLink(self.url, tooltip)

    def serialize(self):
        return {
            "link": True,
            "linkUrl": self.url,
            "linkTooltip": self.tooltip
        }

class PanelTableColorMode(Enum):
    CELL = "cell"
    ROW = "row"

class PanelTableColor:
    COLORS = [
        "rgba(50, 172, 45, 0.97)",
        "rgba(237, 129, 40, 0.89)",
        "rgba(245, 54, 54, 0.9)"
    ]

    def __init__(self, mode: PanelTableColorMode, a, b, invert=False):
        super().__init__()

        validate_arg(mode, [PanelTableColorMode], "mode")
        validate_arg(a, [int, float], "a")
        validate_arg(b, [int, float], "b")
        validate_arg(invert, [bool], "invert")

        self.mode = mode
        self.a = a
        self.b = b
        self.colors = list(reversed(PanelTableColor.COLORS)) if invert else PanelTableColor.COLORS

    def serialize(self):
        return {
            "colorMode": self.mode.value,
            "thresholds": [str(self.a), str(self.b)],
            "colors": self.colors
        }

class PanelTableValueMaps:
    def __init__(self, maps: dict):
        super().__init__()

        validate_arg(maps, [dict], "maps")

        self.maps = maps

    def serialize(self):
        return {
            "valueMaps": list(map(lambda pair: {"value": pair[0], "text": pair[1]}, self.maps.items())),
            "mappingType": 1
        }

class PanelTableStyle():
    def __init__(self, type: str, alias: str, link, color):
        super().__init__()

        validate_arg(type, [str], "type")
        validate_arg(alias, [str], "alias")
        validate_arg(link, [PanelTableLink, NotUsed], "link")
        validate_arg(color, [PanelTableColor, NotUsed], "color")

        self.type = type
        self.alias=alias
        self.link = link
        self.color=color

    def serialize(self, pattern: str):
        return {
            **self.link.serialize(),
            **self.color.serialize(),
            **{
                "pattern": pattern,
                "type": self.type,
                "alias": self.alias
            }
        }

class PanelTableStyleHidden():
    def __init__(self):
        super().__init__()

    def serialize(self, pattern: str):
        return {
            "pattern": pattern,
            "type": "hidden"
        }

class PanelTableStyleNumber(PanelTableStyle):
    def __init__(self, alias="", unit="short", link=NotUsed(), color=NotUsed()):
        super().__init__(
            type="number",
            alias=alias,
            link=link,
            color=color
        )

        validate_arg(unit, [str], "unit")

        self.unit = unit

    def serialize(self, pattern: str):
        return {
            **super().serialize(pattern),
            **{
                "unit": self.unit
            }
        }

class PanelTableStyleString(PanelTableStyle):
    def __init__(self, alias="", link=NotUsed(), color=NotUsed(), maps=NotUsed()):
        super().__init__(
            type="string",
            alias=alias,
            link=link,
            color=color
        )

        validate_arg(maps, [PanelTableValueMaps, NotUsed], "maps")

        self.maps = maps

    def serialize(self, pattern: str):
        return {
            **super().serialize(pattern),
            **self.maps.serialize()
        }
