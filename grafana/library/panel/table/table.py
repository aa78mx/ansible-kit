from python.utils.validators import validate_arg

from grafana.library.panel.panel import Panel

class PanelTable:
    def __init__(
        self,
        x: int, y: int, w: int, h: int,
        datasource: str,
        title: str
    ):
        super().__init__()

        validate_arg(x, [int], "x")
        validate_arg(y, [int], "y")
        validate_arg(w, [int], "w")
        validate_arg(h, [int], "h")
        validate_arg(datasource, [str], "datasource")
        validate_arg(title, [str], "title")

        self.base = Panel(
            type="table",
            x=x,
            y=y,
            w=w,
            h=h,
            datasource=datasource,
            title=title
        )

    def serialize(self):
        return self.base.serialize()
