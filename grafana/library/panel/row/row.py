from python.utils.validators import validate_arg

from grafana.library.constants import GrafanaConstants

class PanelRow:
    def __init__(self, y: int, title: str):
        super().__init__()

        validate_arg(y, [int], "y")
        validate_arg(title, [str], "title")

        self.y = y
        self.title = title

    def serialize(self):
        return {
            "type": "row",
            "gridPos": {
                "x": GrafanaConstants.X[0][0],
                "y": self.y,
                "w": GrafanaConstants.W[0],
                "h": 1
            },
            "title": self.title
        }
