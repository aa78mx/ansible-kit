from enum import Enum

from python.utils.validators import validate_arg

class PanelTargetFormat(Enum):
    TABLE = "table"
    SERIES = "series"

class PanelTarget:
    def __init__(self, format: PanelTargetFormat):
        super().__init__()

        validate_arg(format, [PanelTargetFormat], "format")

        self.format = format

    def serialize(self, id: int):
        validate_arg(id, [int], "id")

        return {
            "format": self.format.value,
            "refId": hex(id)[2:]
        }
