from python.utils.validators import validate_arg
from python.utils.validators import validate_list_arg

class Panel:
    def __init__(self, type: str, x: int, y: int, w: int, h: int, datasource: str, title: str, targets: list = []):
        from grafana.library.clickhouse.panel.target import ClickhousePanelTarget
        from grafana.library.prometheus.panel.target import PrometheusPanelTarget

        super().__init__()

        validate_arg(type, [str], "type")
        validate_arg(x, [int], "x")
        validate_arg(y, [int], "y")
        validate_arg(w, [int], "w")
        validate_arg(h, [int], "h")
        validate_arg(datasource, [str], "datasource")
        validate_arg(title, [str], "title")
        validate_list_arg(targets, [ClickhousePanelTarget, PrometheusPanelTarget], "targets")

        self.type = type
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.datasource = datasource
        self.title = title
        self.targets = targets

    def serialize(self):
        targets = list(map(
            lambda pair: pair[1].serialize(pair[0]),
            enumerate(self.targets)
        ))

        return {
            "type": self.type,
            "gridPos": {
                "x": self.x,
                "y": self.y,
                "w": self.w,
                "h": self.h
            },
            "datasource": self.datasource,
            "title": self.title,
            "targets": targets
        }
