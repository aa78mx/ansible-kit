from python.utils.validators import validate_arg

from grafana.library.not_used import NotUsed
from grafana.library.panel.panel import Panel

class PanelGraphLegendHidden:
    def serialize(self):
        return {
            "show": False
        }

class PanelGraphLegend:
    def __init__(self, as_table=False, right_side=False, current=False):
        super().__init__()

        validate_arg(as_table, [bool], "as_table")
        validate_arg(right_side, [bool], "right_side")
        validate_arg(current, [bool], "current")

        self.as_table = as_table
        self.right_side = right_side
        self.current = current

    def serialize(self):
        return {
            "alignAsTable": self.as_table,
            "rightSide": self.right_side,
            "current": self.current,
            "show": True,
            "values": True,
            "hideEmpty": True,
            "hideZero": True
        }

class PanelGraph(Panel):
    def __init__(
        self,
        x: int, y: int, w: int, h: int,
        datasource,
        title="",
        data_columns=[],
        legend=PanelGraphLegendHidden(),
        stack=True,
        y_format="short",
        y_min=None,
        y_max=None
    ):
        validate_arg(x, [int], "x")
        validate_arg(y, [int], "y")
        validate_arg(w, [int], "w")
        validate_arg(h, [int], "h")
        validate_arg(datasource, [str], "datasource")
        validate_arg(title, [str], "title")
        validate_arg(data_columns, [list], "data_columns")
        validate_arg(legend, [PanelGraphLegendHidden, PanelGraphLegend], "legend")
        validate_arg(stack, [bool], "stack")
        validate_arg(y_format, [str], "y_format")
        validate_arg(y_min, [None, int, float], "y_min")
        validate_arg(y_max, [None, int, float], "y_max")

        super().__init__(
            type="graph",
            x=x,
            y=y,
            w=w,
            h=h,
            datasource=datasource,
            title=title,
            targets=list(map(lambda column: column.target, data_columns))
        )

        self.data_columns = data_columns
        self.legend = legend
        self.stack = stack
        self.y_format = y_format
        self.y_min = y_min
        self.y_max = y_max

    def serialize(self):
        return {
            **super().serialize(),
            **{
                "fill": 2,
                "legend": self.legend.serialize(),
                "stack": self.stack,
                "yaxes": [
                    {
                        "format": self.y_format,
                        "label": None,
                        "logBase": 1,
                        "max": self.y_max,
                        "min": self.y_min,
                        "show": True
                    },
                    {
                        "format": self.y_format,
                        "label": None,
                        "logBase": 1,
                        "max": self.y_max,
                        "min": self.y_min,
                        "show": True
                    }
                ]
            }
        }
