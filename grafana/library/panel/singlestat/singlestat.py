from python.utils.validators import validate_arg

from grafana.library.not_used import NotUsed
from grafana.library.panel.panel import Panel

class PanelSinglestatColor:
    COLORS = [
        "rgba(50, 172, 45, 0.97)",
        "rgba(237, 129, 40, 0.89)",
        "rgba(245, 54, 54, 0.9)"
    ]

    def __init__(self, a, b, invert=False, background=True):
        super().__init__()

        validate_arg(a, [int, float], "a")
        validate_arg(b, [int, float], "b")
        validate_arg(invert, [bool], "invert")
        validate_arg(background, [bool], "background")

        self.a = a
        self.b = b
        self.colors = list(reversed(PanelSinglestatColor.COLORS)) if invert else PanelSinglestatColor.COLORS
        self.background = background

    def serialize(self):
        return {
            "thresholds": "%d, %d" % (self.a, self.b),
            "colors": self.colors,
            "colorBackground": self.background
        }

class PanelSinglestat(Panel):
    def __init__(
        self, x: int, y: int, w: int, h: int, datasource: str, target,
        font_size="50%", color=NotUsed(), transparent=True, prefix=""
    ):
        from grafana.library.prometheus.panel.target import PrometheusPanelTarget

        validate_arg(x, [int], "x")
        validate_arg(y, [int], "y")
        validate_arg(w, [int], "w")
        validate_arg(h, [int], "h")
        validate_arg(datasource, [str], "datasource")
        validate_arg(target, [PrometheusPanelTarget], "taget")
        validate_arg(font_size, [str], "font_size")
        validate_arg(color, [PanelSinglestatColor, NotUsed], "color")
        validate_arg(transparent, [bool], "transparent")
        validate_arg(prefix, [str], "prefix")

        super().__init__(
            type="singlestat",
            x=x,
            y=y,
            w=w,
            h=h,
            datasource=datasource,
            title="",
            targets=[target]
        )

        self.font_size = font_size
        self.color = color
        self.transparent = transparent
        self.prefix = prefix

    def serialize(self):
        return {
            **super().serialize(),
            **self.color.serialize(),
            **{
                "valueFontSize": self.font_size,
                "transparent": self.transparent,
                "prefix": self.prefix,
                "prefixFontSize": self.font_size
            }
        }
