import posixpath
import json

from python.utils.validators import validate_arg
from python.utils.validators import validate_list_arg

from grafana.library.clickhouse.panel.table.table import ClickhousePanelTable
from grafana.library.dashboard.link import DashboardLink
from grafana.library.dashboard.timepicker import DashboardTimepicker
from grafana.library.dashboard.timepicker import DashboardTimepickerHidden
from grafana.library.dashboard.variable.custom_multi import DashboardVariableCustomMulti
from grafana.library.dashboard.variable.query import DashboardVariableQuery
from grafana.library.dashboard.variable.text import DashboardVariableText
from grafana.library.panel.graph.graph import PanelGraph
from grafana.library.panel.row.row import PanelRow
from grafana.library.panel.singlestat.singlestat import PanelSinglestat
from grafana.library.prometheus.panel.table.table import PrometheusPanelTable

class Dashboard:
    def __init__(
        self,
        uid: str,
        title: str,
        timepicker=DashboardTimepicker(),
        panels=[],
        variables=[],
        tags=[],
        links=[]
    ):
        super().__init__()

        validate_arg(uid, [str], "uid")
        validate_arg(title, [str], "title")
        validate_arg(timepicker, [DashboardTimepicker, DashboardTimepickerHidden], "timepicker")
        validate_list_arg(
            arg=panels,
            types=[
                ClickhousePanelTable,
                PanelGraph,
                PanelRow,
                PanelSinglestat,
                PrometheusPanelTable
            ],
            name="panels"
        )
        validate_list_arg(
            arg=variables,
            types=[
                DashboardVariableCustomMulti,
                DashboardVariableQuery,
                DashboardVariableText
            ],
            name="variables"
        )
        validate_list_arg(tags, [str], "tags")
        validate_list_arg(links, [DashboardLink], "links")

        self.uid = uid
        self.title = title
        self.timepicker = timepicker
        self.panels = panels
        self.variables = variables
        self.tags = tags
        self.links = links

    def save(self, path):
        full_path = posixpath.join(path, self.title + ".json")

        with open(full_path, "w") as f:
            json.dump(self.serialize(), f, sort_keys=True, indent=2)
            f.write("\n")

    def serialize(self):
        return {
            **self.timepicker.serialize(),
            **{
                "uid": self.uid,
                "title": self.title,
                "panels": list(map(lambda panel: panel.serialize(), self.panels)),
                "templating": {
                    "list": list(map(lambda variable: variable.serialize(), self.variables))
                },
                "tags": self.tags,
                "links": list(map(lambda link: link.serialize(), self.links)),
                "refresh": "30s"
            }
        }
