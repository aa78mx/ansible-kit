from python.utils.validators import validate_arg

class DashboardLink:
    def __init__(self, url, title):
        super().__init__()

        validate_arg(url, [str], "url")
        validate_arg(title, [str], "title")

        self.url = url
        self.title = title

    def serialize(self):
        return {
            "type": "link",
            "icon": "dashboard",
            "url": self.url,
            "title": self.title
        }
