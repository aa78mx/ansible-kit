from python.utils.validators import validate_arg
from python.utils.validators import validate_list_arg

class DashboardVariableCustomMultiValue:
    def __init__(self, value: str, text=None, selected=False):
        super().__init__()

        validate_arg(value, [str], "value")
        validate_arg(text, [None, str], "text")
        validate_arg(selected, [bool], "selected")

        self.value = value
        self.text = text if (text is not None) else self.value
        self.selected = selected

class DashboardVariableCustomMulti:
    def __init__(
        self,
        name: str,
        values=[],
        label=""
    ):
        super().__init__()

        validate_arg(name, [str], "name")
        validate_list_arg(values, [DashboardVariableCustomMultiValue], "values")
        validate_arg(label, [str], "label")

        self.name = name
        self.values = values
        self.label = label

    def serialize(self):
        selected = list(filter(lambda v: v.selected, self.values))
        all_value = DashboardVariableCustomMultiValue(
            value="$__all",
            text="All",
            selected=len(selected) < 1
        )

        values = [all_value] + self.values

        if all_value.selected:
            selected.insert(0, all_value)

        return {
            "name": self.name,
            "label": self.label,
            "options": list(map(lambda v: {"selected": v.selected, "text": v.text, "value": v.value}, values)),
            "multi": True,
            "current": {
               "text": list(map(lambda v: v.text, selected)),
               "value": list(map(lambda v: v.value, selected))
            },
            "type": "custom"
        }
