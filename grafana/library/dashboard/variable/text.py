from python.utils.validators import validate_arg

class DashboardVariableText:
    def __init__(self, name: str, value="", label=""):
        super().__init__()

        validate_arg(name, [str], "name")
        validate_arg(value, [str], "value")
        validate_arg(label, [str], "label")

        self.name = name
        self.value = value
        self.label = label

    def serialize(self):
        return {
            "type": "textbox",
            "name": self.name,
            "query": self.value,
            "label": self.label,
            "current": {
                "value": self.value
            }
        }
