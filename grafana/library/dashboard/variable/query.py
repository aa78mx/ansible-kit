from enum import Enum

from python.utils.validators import validate_arg

from grafana.library.dashboard.variable.option.include_all import DashboardVariableQueryOptionIncludeAll
from grafana.library.not_used import NotUsed

class DashboardVariableQuerySort(Enum):
    DISABLED = 0
    ALPHABETICAL_ASC = 1
    ALPHABETICAL_DESC = 2

class DashboardVariableQuery:
    def __init__(
        self,
        name: str,
        datasource: str,
        query: str,
        label="",
        regex="",
        multi=False,
        include_all=NotUsed(),
        hide=False,
        sort=DashboardVariableQuerySort.DISABLED
    ):
        super().__init__()

        validate_arg(name, [str], "name")
        validate_arg(datasource, [str], "datasource")
        validate_arg(query, [str], "query")
        validate_arg(label, [str], "label")
        validate_arg(regex, [str], "regex")
        validate_arg(multi, [bool], "multi")
        validate_arg(include_all, [NotUsed, DashboardVariableQueryOptionIncludeAll], "include_all")
        validate_arg(hide, [bool], "hide")
        validate_arg(sort, [DashboardVariableQuerySort], "sort")

        self.name = name
        self.datasource = datasource
        self.query = query
        self.label = label
        self.regex = regex
        self.multi = multi
        self.include_all = include_all
        self.hide = hide
        self.sort = sort

    def serialize(self):
        return {
            **self.include_all.serialize(),
            **{
                "name": self.name,
                "query": self.query,
                "datasource": self.datasource,
                "label": self.label,
                "regex": self.regex,
                "multi": self.multi,
                "hide": 2 if self.hide else 0,
                "type": "query",
                "refresh": 1,
                "sort": self.sort.value
            }
        }
