from python.utils.validators import validate_arg

class DashboardVariableQueryOptionIncludeAll:
    def __init__(self, all_value=None):
        super().__init__()

        validate_arg(all_value, [str, None], "all_value")

        self.all_value = all_value

    def serialize(self):
        return {
            "includeAll": True,
            "allValue": self.all_value
        }
