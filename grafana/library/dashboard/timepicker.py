class DashboardTimepickerHidden:
    def serialize(self):
        return {
            "timepicker": {
                "hidden": True
            }
        }

class DashboardTimepicker:
    def serialize(self):
        return {
            "timepicker": {
                "refresh_intervals": ["30s", "1m"]
            },
            "time": {
                "from": "now-1d",
                "to": "now"
            },
            "refresh": "30s"
        }
