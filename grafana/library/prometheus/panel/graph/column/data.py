from python.utils.validators import validate_arg

from grafana.library.panel.target import PanelTargetFormat
from grafana.library.prometheus.panel.target import PrometheusPanelTarget

class PrometheusPanelGraphColumnData:
    def __init__(self, expr: str, legend: str):

        super().__init__()

        validate_arg(expr, [str], "expr")
        validate_arg(legend, [str], "legend")

        self.target = PrometheusPanelTarget(
            format=PanelTargetFormat.SERIES,
            expr=expr,
            instant=False,
            legend=legend
        )
