from grafana.library.panel.singlestat.singlestat import PanelSinglestat
from grafana.library.panel.target import PanelTargetFormat
from grafana.library.prometheus.panel.target import PrometheusPanelTarget

class PrometheusPanelSinglestat(PanelSinglestat):
    def __init__(
        self, x: int, y: int, w: int, h: int, expr: str,
        font_size="50%", color=NotUsed(), transparent=True, prefix=""
    ):
        super().__init__(
            x=x,
            y=y,
            w=w,
            h=h,
            title="",
            target=PrometheusPanelTarget(
                format=PanelTargetFormat.TABLE,
                expr=expr,
                instant=True,
                legend=""
            ),
            font_size=font_size,
            color=color,
            transparent=transparent,
            prefix=prefix
        )
