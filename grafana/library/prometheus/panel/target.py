from python.utils.validators import validate_arg

from grafana.library.panel.target import PanelTarget
from grafana.library.panel.target import PanelTargetFormat

class PrometheusPanelTarget(PanelTarget):
    def __init__(self, format: PanelTargetFormat, expr: str, instant: bool, legend: str):
        validate_arg(format, [PanelTargetFormat], "format")
        validate_arg(expr, [str], "expr")
        validate_arg(instant, [bool], "instant")
        validate_arg(legend, [str], "legend")

        super().__init__(
            format=format
        )

        self.expr = expr
        self.instant = instant
        self.legend = legend

    def serialize(self, id: int):
        validate_arg(id, [int], "id")

        return {
            **super().serialize(id),
            **{
                "expr": self.expr,
                "instant": self.instant,
                "legendFormat": self.legend
            }
        }
