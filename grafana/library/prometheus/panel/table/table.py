from python.utils.validators import validate_arg
from python.utils.validators import validate_list_arg

from grafana.library.panel.panel import Panel
from grafana.library.panel.table.column.label import PanelTableColumnLabel
from grafana.library.prometheus.panel.table.column.data import PrometheusPanelTableColumnData

class PrometheusPanelTable(Panel):
    def __init__(
        self,
        x: int, y: int, w: int, h: int,
        datasource,
        title="",
        fontSize="100%",
        data_columns=[],
        label_columns=[]
    ):
        validate_arg(x, [int], "x")
        validate_arg(y, [int], "y")
        validate_arg(w, [int], "w")
        validate_arg(h, [int], "h")
        validate_arg(datasource, [str], "datasource")
        validate_arg(fontSize, [str], "fontSize")
        validate_list_arg(data_columns, [PrometheusPanelTableColumnData], "data_columns")
        validate_list_arg(label_columns, [PanelTableColumnLabel], "label_columns")

        super().__init__(
            type="table",
            x=x,
            y=y,
            w=w,
            h=h,
            datasource=datasource,
            title=title,
            targets=list(map(lambda column: column.target, data_columns))
        )

        self.font_size = fontSize
        self.data_columns = data_columns
        self.label_columns = label_columns

    def serialize(self):
        if (len(self.data_columns) == 1):
            data_column_styles = [self.data_columns[0].style.serialize("Value")]
        else:
            data_column_styles = list(map(
                lambda pair: pair[1].style.serialize("Value #%s" % hex(pair[0])[2:]),
                enumerate(self.data_columns)
            ))

        label_column_styles = list(map(lambda style: style.serialize(), self.label_columns))

        return {
            **super().serialize(),
            **{
                "transform": "table",
                "fontSize": self.font_size,
                "styles": label_column_styles + data_column_styles,
                "sort": {
                    "col": 1,
                    "desc": False
                }
            }
        }
