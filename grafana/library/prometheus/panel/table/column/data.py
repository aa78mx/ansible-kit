from python.utils.validators import validate_arg

from grafana.library.panel.table.style import PanelTableStyleNumber
from grafana.library.panel.table.style import PanelTableStyleString
from grafana.library.panel.target import PanelTargetFormat
from grafana.library.prometheus.panel.target import PrometheusPanelTarget

class PrometheusPanelTableColumnData:
    def __init__(self, expr: str, style=PanelTableStyleNumber()):
        super().__init__()

        validate_arg(expr, [str], "expr")
        validate_arg(style, [PanelTableStyleNumber, PanelTableStyleString], "style")

        self.target = PrometheusPanelTarget(
            format=PanelTargetFormat.TABLE,
            expr=expr,
            instant=True,
            legend=""
        )
        self.style = style
