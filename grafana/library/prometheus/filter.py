import json

from python.error import Error
from python.utils.validators import validate_arg
from python.utils.validators import validate_list_arg

class PrometheusFilter():
    def __init__(self, filters=[]):
        super().__init__()

        validate_list_arg(filters, [str], "filters")

        self.filters = filters

    def add(self, label: str, operator: str, value: str):
        validate_arg(label, [str], "label")
        validate_arg(operator, [str], "operator")
        validate_arg(value, [str], "value")

        if (operator not in ["=~", "!~", "!=", "="]):
            raise Error("invalid operator [%s]" % operator)

        return PrometheusFilter(self.filters + ["%s%s%s" % (label, operator, json.dumps(value))])

    def __str__(self):
        return "{" + ", ".join(self.filters) + "}"

    def metric(self, name, time_range=None):
        validate_arg(name, [str], "name")
        validate_arg(time_range, [None, str], "time_range")

        if (time_range is None):
            return name + self.__str__()
        else:
            return name + self.__str__() + "[" + time_range + "]"
