from python.utils.validators import validate_arg
from python.utils.validators import validate_dict_arg

from grafana.library.clickhouse.panel.field import ClickhousePanelField
from grafana.library.clickhouse.panel.target import ClickhousePanelTarget

class ClickhousePanelExt:
    def __init__(
        self,
        query: str,
        date_time_col_data_type: str = "",
        fields: dict = {}
    ):
        validate_arg(query, [str], "query")
        validate_arg(date_time_col_data_type, [str], "date_time_col_data_type")
        validate_dict_arg(fields, [ClickhousePanelField], "fields")

        self.target = ClickhousePanelTarget(
            query=query,
            date_time_col_data_type=date_time_col_data_type
        )
        self.fields = fields

    def serialize(self):
        return {
            "targets": [self.target.serialize(0)],
            "fieldConfig": {
                "overrides": list(map(lambda pair: pair[1].serialize(pair[0]), self.fields.items()))
            }
        }
