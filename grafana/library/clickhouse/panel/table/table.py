from python.utils.validators import validate_arg
from python.utils.validators import validate_dict_arg

from grafana.library.clickhouse.panel.field import ClickhousePanelField
from grafana.library.clickhouse.panel.panel_ext import ClickhousePanelExt
from grafana.library.panel.table.table import PanelTable

class ClickhousePanelTable:
    def __init__(
        self,
        x: int, y: int, w: int, h: int,
        datasource: str,
        query: str,
        date_time_col_data_type: str = "",
        title: str = "",
        fields: dict = {}
    ):
        validate_arg(x, [int], "x")
        validate_arg(y, [int], "y")
        validate_arg(w, [int], "w")
        validate_arg(h, [int], "h")
        validate_arg(datasource, [str], "datasource")
        validate_arg(query, [str], "query")
        validate_arg(date_time_col_data_type, [str], "date_time_col_data_type")
        validate_arg(title, [str], "title")
        validate_dict_arg(fields, [ClickhousePanelField], "fields")

        self.base = PanelTable(
            x=x,
            y=y,
            w=w,
            h=h,
            datasource=datasource,
            title=title
        )
        self.ext = ClickhousePanelExt(
            query=query,
            date_time_col_data_type=date_time_col_data_type,
            fields=fields
        )

    def serialize(self):
        return {
            **self.base.serialize(),
            **self.ext.serialize()
        }
