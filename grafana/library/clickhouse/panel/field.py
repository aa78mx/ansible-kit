from python.utils.validators import validate_arg

class ClickhousePanelField:
    def __init__(self, unit: str = None):
        validate_arg(unit, [str, None], "unit")

        self.unit = unit

    def serialize(self, id):
        validate_arg(id, [str], "id")

        properties = []

        if (self.unit is not None):
            properties.append({
                "id": "unit",
                "value": self.unit
            })

        return {
            "matcher": {
                "id": "byName",
                "options": id
            },
            "properties": properties
        }
