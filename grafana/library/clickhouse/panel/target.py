from grafana.library.panel.target import PanelTarget
from grafana.library.panel.target import PanelTargetFormat

from python.utils.validators import validate_arg

class ClickhousePanelTarget(PanelTarget):
    def __init__(self, query: str, date_time_col_data_type: str):
        validate_arg(query, [str], "query")
        validate_arg(date_time_col_data_type, [str], "date_time_col_data_type")

        super().__init__(
            format=PanelTargetFormat.TABLE
        )

        self.query = query
        self.date_time_col_data_type = date_time_col_data_type

    def serialize(self, id: int):
        validate_arg(id, [int], "id")

        return {
            **super().serialize(id),
            **{
                "query": self.query,
                "dateTimeColDataType": self.date_time_col_data_type,
                "dateTimeType": "TIMESTAMP"
            }
        }
