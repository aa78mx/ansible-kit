class GrafanaConstants:
    BYTES_IN_GiB = 1073741824

    X = [
        [0],
        [0, 12],
        [0, 8, 16]
    ]

    W = [
        24,
        12,
        8
    ]
